package com.qiniu.api.rs;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONException;
import net.sf.json.JSONObject;
import net.sf.json.util.JSONTokener;

import com.qiniu.api.net.CallRet;

public class BatchCallRet extends CallRet {

  public List<CallRet> results = new ArrayList<CallRet>();

  public BatchCallRet(CallRet ret) {
    super(ret);
    if (ret.ok() && ret.getResponse() != null) {
      try {
        unmarshal(ret.getResponse());
      } catch (Exception e) {
        e.printStackTrace();
        this.exception = e;
      }
    }
  }

  @Override
  public boolean ok() {
    // partial ok
    if (this.statusCode == 298) {
      return false;
    }
    return super.ok();
  }

  private void unmarshal(String response) throws JSONException {
    JSONTokener tokens = new JSONTokener(response);
    JSONArray arr = JSONArray.fromObject(tokens);

    for (int i = 0; i < arr.size(); i++) {
      CallRet ret = new CallRet();
      JSONObject jsonObj = arr.getJSONObject(i);
      if (jsonObj.has("code")) {
        int code = jsonObj.getInt("code");
        ret.statusCode = code;
      }
      if (jsonObj.has("data")) {
        JSONObject body = jsonObj.getJSONObject("data");
        ret.response = body.toString();
      }
      results.add(ret);
    }
  }

}

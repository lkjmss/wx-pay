/*!
 * IwonnetUI upload (http://www.iwonnet.com)
 * Copyright 2013 Iwonnet, Inc.
 * Licensed under http://www.apache.org/licenses/LICENSE-2.0
 */
!function($) {
	var wUpload = function(element, options) {
		this._id = (((1 + Math.random()) * 0x10000) | 0).toString(16);
		this.opts = options;
		this._initLayout($(element));
		this.init();
	};
	wUpload.prototype = {
		init : function() {
			var that = this;
			var defaults = {
				// 处理上传文件的服务器端页面的url地址，可以是绝对地址，也可以是相对地址，当为相对地址时相对的是当前代码所在的文档地址
				upload_url : "/ezdocs/upload/3308011000000240",
				// swfupload.swf文件的绝对或相对地址，相对地址是指相对于当前的页面地址。实例化swfupload后，就不能再改变该属性的值了。
				flash_url : "/swfupload/swfupload.swf",
				// 相当于用普通的文件域上传文件时的name属性，服务器端接收页面通过该名称来获取上传的文件
				file_post_name : "Filedata",
				// 一个对象直接量，里面的键/值对会随着每一个文件一起上传，文件上传要附加一些信息时很有用
				post_params : {},
				// 为false时,post_params属性定义的参数会以post方式上传；为true时，则会以get方式上传（即参数会以查询字符串的形式附加到url后面）
				use_query_string : false,
				requeue_on_error : false,
				http_success : [ 201, 202 ],
				assume_success_timeout : 0,
				// 该属性指定了允许上传的文件类型，当有多个类型时使用分号隔开，比如：*.jpg;*.png ,允许所有类型时请使用
				// *.*
				file_types : "*.jpg;*.gif;*.png;*.zip;*.doc;*.docx;*.xls;*.xlsx;*.ppt;*.pptx;*.txt;",
				// 指定在文件选取窗口中显示的文件类型描述，起一个提示和说明的作用吧
				file_types_description : "Web Image Files",
				// 指定要上传的文件的最大体积，可以带单位，合法的单位有:B、KB、MB、GB，如果省略了单位，则默认为KB。该属性为0时，表示不限制文件的大小。
				file_size_limit : "100MB",
				// 指定最多能上传多少个文件，当上传成功的文件数量达到了这个最大值后，就不能再上传文件了，也不能往上传队列里添加文件了。把该属性设为0时表示不限制文件的上传数量。
				file_upload_limit : 0,
				// 指定文件上传队列里最多能同时存放多少个文件。当超过了这个数目后只有当队列里有文件上传成功、上传出错或被取消上传后，等同数量的其他文件才可以被添加进来。当file_upload_limit的数值或者剩余的能上传的文件数量小于file_queue_limit时，则取那个更小的值
				file_queue_limit : 0,
				debug : false,
				// 为true时会加一个随机数在swfupload.swf地址的后面，以阻止flash影片被缓存，这是为了防止某些版本的IE浏览器在读取缓存的falsh影片时出现的bug
				prevent_swf_caching : false,
				// 如果为false则SWFUpload会把swfupload.swf用到的相对地址转换为绝对地址，以达到更好的兼容性
				preserve_relative_urls : false,
				// 指定一个dom元素的id,该dom元素在swfupload实例化后会被Flash按钮代替，这个dom元素相当于一个占位符
				button_placeholder_id : "spanButtonPlaceHolder",
				// 指定Flash按钮的背景图片，相对地址或绝对地址都可以。该地址会受到preserve_relative_urls属性的影响，遵从与upload_url一样的规则。
				// 该背景图片必须是一个sprite图片,从上到下包含了Flash按钮的正常、鼠标悬停、按下、禁用这四种状态。因此该图片的高度应该是Flash按钮高度的四倍
				button_image_url : "/swfupload/XPButtonNoText_61x22.png",
				// 指定Flash按钮的宽度
				button_width : 61,
				// 指定Flash按钮的高度，应该为button_image_url所指定的按钮背景图片高度的1/4
				button_height : 22,
				// 指定Flash按钮上的文字，也可以是html代码
				button_text : "<span class=\"center\">浏览</span>",
				// Flash按钮上的文字的样式，使用方法见示例
				button_text_style : ".center { text-align:center;width:56px}",
				// 指定Flash按钮顶部的内边距，可使用负值
				button_text_top_padding : 3,
				// 指定Flash按钮左边的内边距，可使用负值
				button_text_left_padding : 1,
				button_action : SWFUpload.BUTTON_ACTION.SELECT_FILES,
				// 为true时Flash按钮将变为禁用状态，点击也不会触发任何行为
				button_disabled : false,
				// 指定鼠标悬停在Flash按钮上时的光标样式，可用值为SWFUpload.CURSOR里定义的常量
				button_cursor : SWFUpload.CURSOR.HAND,
				// 指定Flash按钮的WMODE属性，可用值为SWFUpload.WINDOW_MODE里定义的常量
				button_window_mode : SWFUpload.WINDOW_MODE.WINDOW,
				file_queued_handler : that._fileQueued,
				upload_start_handler : that._uploadStart,
				file_dialog_complete_handler : that._fileComplete,
				upload_progress_handler : that._uploadProgress,
				upload_success_handler : that._uploadSuccess,
				upload_complete_handler : that._uploadComplete
			};
			var options = $.extend(true, {}, defaults, this.opts);
			this.swfu = new SWFUpload(options);
		},
		// 初始化布局
		_initLayout : function($el) {
			var opts = this.opts;
			var $elParent = $el.parent();
			var elIndex = $el.index();
			$el.remove();

			var upload = [
					'<div class="wUI_upload_warp">',
					'<style></style>',
					'<div class="wUI_upload_hd">',
					'<a class="btn btn-default" id="spanButtonPlaceHolder">浏览</a>',
					'<label style="height: 34px;line-height: 34px;margin-left: 5px;font-weight: bolder;position: absolute;">',
					'本次上传数量：<span id="tot_num">0</span>，成功数量：<span id="success">0</span>，失败数量：<span id="danger">0</span></label>',
					'</div>', '<div class="wUI_upload_bd">',
					'<ul class="upload-body"></ul>',
					'</div></div>' ];
			var $upload = $(upload.join(''));
			this.$upload = $upload;
			this.$ul = $upload.find('.upload-body');
			this.$style = $upload.find('style');
			this.$help = $upload.find('help');

			// 放回原位置
			if (elIndex === 0 || $elParent.children().length == 0) {
				$elParent.prepend(this.$upload);
			} else {
				$elParent.children().eq(elIndex - 1).after(this.$upload);
			}
		},
		_fileQueued : function() {
			var $el = $(this.movieElement).parents('.wUI_upload_warp');
			var file = arguments[0];
			var li = [
					'<li id="' + file.id + '">',
					'<span class="glyphicon glyphicon-remove"></span>',
					'<label class="text-overflow">' + file.name + '</label>',
					'<div class="progress">',
					'<div class="progress-bar" id="' + file.id
							+ '_progress">0%</div>', '</div>',
					'<label class="text-overflow upload_state">等待上传</label>',
					'</li>' ];
			var $ul = $el.find('.upload-body');
			$ul.append($(li.join('')));
			$('#tot_num').text(parseInt($('#tot_num').text()) + 1);
		},
		_uploadStart : function(file) {
			// alert(file.id);
			var _id = file.id, $file = $('#' + _id);
			$file.find('.upload_state').text('数据上传中...');
		},
		_fileComplete : function() {
			if (this.getStats().files_queued > 0) {
				this.startUpload();
			}
		},
		_uploadProgress : function(file, btyes, total) {
			var $progress = $('#' + file.id + '_progress');
			var uploaded = parseFloat(btyes / total).toFixed(4) * 100 + '%';
			$progress.text(uploaded).css('width', uploaded);
		},
		_uploadSuccess : function(file, data, response) {
			data = eval('(' + data + ')');
			var _id = file.id, $file = $('#' + _id);
			if (data.flag) {
				$file.addClass('danger');
				$('#danger').text(parseInt($('#danger').text()) + 1);
				$file.find('.upload_state').html(data.message + '<a onclick="ErrorMessage(\'文件上传失败\',\''+data.error+'\')">点击查看详细错误信息</a>');
			} else {
				$file.addClass('success');
				$('#success').text(parseInt($('#success').text()) + 1);
				$file.find('.upload_state').text(data.message);
				$file.slideUp('slow', function() {
					$(this).remove();
				});
			}
		},
		_uploadComplete : function(file) {
			if (this.getStats().files_queued > 0) {
				this.startUpload();
			}
		}
	};

	$.fn.upload = function() {
		if (arguments.length === 0 || typeof arguments[0] === 'object') {
			var option = arguments[0], data = this.data('wUpload'), options = $
					.extend(true, {}, $.fn.upload.defaults, option);
			if (!data) {
				data = new wUpload(this, options);
				this.data('wUpload', data);
			}
		}
		if (typeof arguments[0] === 'string') {
		}
	};

	$.fn.upload.defaults = {

	};
}(window.jQuery);
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
  String path = request.getContextPath();
  String basePath = request.getScheme() + "://" + request.getServerName() + ":"
      + request.getServerPort() + path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>My JSP 'view.jsp' starting page</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<script type="text/javascript"
	src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script>
<link rel="stylesheet" type="text/css" href="/wx/css/view_base.css">

</head>

<body>
	<div class="rich_media">
		<div id="js_top_ad_area" class="top_banner"></div>
		<div class="rich_media_inner">
			<div id="page-content">
				<div class="rich_media_area_primary">
					<h2 class="rich_media_title">123</h2>
					<div class="rich_media_meta_list">
						<em id="post-date" class="rich_media_meta rich_media_meta_text">2014-05-27</em>
						<a
							class="rich_media_meta rich_media_meta_link rich_media_meta_nickname"
							href="javascript:void(0);" id="post-user">浙江网能</a>
					</div>
					<div class="rich_media_thumb_wrp">
						<img class="rich_media_thumb" id="js_cover"
							onerror="this.parentNode.removeChild(this)"
							src="http://mmbiz.qpic.cn/mmbiz/uWNXnHhFjCiaabW3SySibnVLXsm5H57PfM6E8ia6TicImMpW5h6MjePZCkziasMoAtgYs62nMiaLJpxuEeawk1KGuJGA/0?tp=webp">
					</div>
					<div class="rich_media_content"></div>
					<link rel="stylesheet" type="text/css" href="/wx/css/view.css">
				</div>
				<div class="rich_media_area_extra">
					<div class="mpda_bottom_container" id="js_bottom_ad_area"></div>
					<div id="js_iframetest" style="display: none;"></div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>

package com.jfinal.ext.plugin.qiniu;

import com.jfinal.plugin.IPlugin;

public class QiNiuPlugin implements IPlugin {

  private String propFile = "qiniu.properties";

  public QiNiuPlugin() {
  }

  public QiNiuPlugin(String propFile) {
    this.propFile = propFile;
  }

  @Override
  public boolean start() {
    QiNiuKit.init(new QiNiuConfig(propFile));
    // System.out.println(QiNiuKit.list("wechatmedia", 10));
    return true;
  }

  @Override
  public boolean stop() {
    return true;
  }

}

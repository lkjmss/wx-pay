package com.jfinal.ext.plugin.qiniu;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.qiniu.api.io.IoApi;
import com.qiniu.api.io.PutExtra;
import com.qiniu.api.io.PutRet;
import com.qiniu.api.rs.RSClient;
import com.qiniu.api.rsf.ListItem;
import com.qiniu.api.rsf.ListPrefixRet;
import com.qiniu.api.rsf.RSFClient;

public class QiNiuKit {

  public static QiNiuConfig config;

  public static void init(QiNiuConfig qiniuConfig) {
    config = qiniuConfig;
  }

  public static PutRet put(String bucketName, String filePath, InputStream is) {
    String token = config.getToken(bucketName);
    return IoApi.Put(token, filePath, is, new PutExtra());
  }

  public static PutRet put(String bucketName, String filePath, File file) {
    String token = config.getToken(bucketName);
    return IoApi.putFile(token, filePath, file, new PutExtra());
  }

  public static List<ListItem> list(String bucketName) {
    return list(bucketName, 0);
  }

  public static List<ListItem> list(String bucketName, int count) {
    return list(bucketName, "", count);
  }

  public static List<ListItem> list(String bucketName, String directoryPath) {
    return list(bucketName, directoryPath, 0);
  }

  public static List<ListItem> list(String bucketName, String directoryPath, int count) {
    if (directoryPath == null) {
      directoryPath = "";
    }
    if (count == 0) {
      count = Integer.MAX_VALUE;
    }
    RSFClient client = config.getRSFClient();

    ListPrefixRet list = client.listPrifix(bucketName, directoryPath, "", count);
    if (list == null)
      return null;

    List<ListItem> items = new ArrayList<ListItem>();
    items.addAll(list.results);

    return items;
  }

  public static void remove(String bucketName, String filePath) {
    RSClient client = config.getRSClient();
    client.delete(bucketName, filePath);
  }

  public static InputStream download(String key) throws IOException {
    String qiniuHost = config.getHost();
    URL url = new URL(qiniuHost + key);
    return url.openStream();
  }

  public static boolean download(String key, String saveDirectory) throws IOException {
    InputStream is = null;
    OutputStream os = null;
    try {
      is = download(key);
      File fileDir = new File(saveDirectory);
      if (fileDir.exists() && fileDir.isDirectory()) {
        File outFile = new File(saveDirectory + fileDir.separator + key);
        os = new FileOutputStream(outFile);
        byte[] buff = new byte[1024];
        while (true) {
          int readed = is.read(buff);
          if (readed == -1) {
            break;
          }
          byte[] temp = new byte[readed];
          System.arraycopy(buff, 0, temp, 0, readed);
          os.write(temp);
        }
        return true;
      } else {
        throw new FileNotFoundException("未找到目录：" + saveDirectory);
      }
    } catch (IOException e) {
      e.printStackTrace();
      return false;
    } finally {
      if (is != null)
        is.close();
      if (os != null)
        os.close();
    }
  }

  public static void setQiNiuLoadHost(String hostUrl) {
    config.setHost(hostUrl);
  }
  
  public static String getQiNiuLoadHost(){
    return config.getHost();
  }
}

package com.jfinal.ext.plugin.qiniu;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONException;

import org.apache.commons.codec.EncoderException;

import com.iwonnet.wx.model.XtXtpz;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Logger;
import com.qiniu.api.auth.AuthException;
import com.qiniu.api.auth.digest.Mac;
import com.qiniu.api.fop.ImageExif;
import com.qiniu.api.fop.ImageInfo;
import com.qiniu.api.rs.GetPolicy;
import com.qiniu.api.rs.PutPolicy;
import com.qiniu.api.rs.RSClient;
import com.qiniu.api.rs.URLUtils;
import com.qiniu.api.rsf.ListItem;
import com.qiniu.api.rsf.ListPrefixRet;
import com.qiniu.api.rsf.RSFClient;

/**
 * 七牛文件存储配置类
 * 
 * @author yanglh
 * 
 */
public final class QiNiuConfig {

  private static String load;
  private Logger logger = Logger.getLogger(getClass());
  private String ak;
  private String sk;

  /**
   * @param propertiesFile
   *         配置文件路径
   */
  public QiNiuConfig(String propertiesFile) {
    Prop qiniuProp = PropKit.use(propertiesFile);
    this.ak = qiniuProp.get("ak");
    this.sk = qiniuProp.get("sk");
    XtXtpz xtpz = XtXtpz.dao.findFirst("select * from xt_xtpz where pzdm = ?", "picturePrefix");
    if (StrKit.notNull(xtpz)) {
      System.out.println("=====");
      this.load = xtpz.getStr("PZZ");
    } else {
      this.load = qiniuProp.get("load");
    }
  }

  /**
   * @param ak
   *         appkey
   * @param sk
   *         sercrtkey
   */
  public QiNiuConfig(String ak, String sk) {
    this.ak = ak;
    this.sk = sk;
  }

  /**
   * @return appkey
   */
  public String getAk() {
    return ak;
  }

  /**
   * @return sercrtkey
   */
  public String getSk() {
    return sk;
  }

  public void setHost(String url) {
    this.load = url;
  }

  /**
   * 获取Host路径
   * 
   * @return Host路径
   */
  public String getHost() {
    String qiniuHost = load;
    if (!qiniuHost.startsWith("http"))
      qiniuHost = "http://" + qiniuHost;
    if (!qiniuHost.endsWith("/"))
      qiniuHost += "/";
    return qiniuHost;
  }

  /**
   * 获取Token
   * 
   * @param bucketName
   *         库名
   * @return Token
   */
  public String getToken(String bucketName) {
    Mac mac = new Mac(ak, sk);
    PutPolicy putPolicy = new PutPolicy(bucketName);
    try {
      return putPolicy.token(mac);
    } catch (JSONException e) {
      logger.error("qiniu config get token", e);
    } catch (AuthException e) {
      logger.error("qiniu config get token", e);
    }
    return null;
  }

  public RSFClient getRSFClient() {
    Mac mac = new Mac(ak, sk);
    RSFClient client = new RSFClient(mac);
    return client;
  }

  public RSClient getRSClient() {
    Mac mac = new Mac(ak, sk);
    RSClient client = new RSClient(mac);
    return client;
  }

  public static void main(String[] args) throws EncoderException, AuthException {
    String ak = "spE6Duuemfi5i-0se2oIyT5t6iilY8u9Oe5WwBpb";
    String sk = "64o9U-4qMnDFd8tIPpEea4PJs0pTRoTEnpLbmsIA";
    String bucketName = "wechatmedia";
    QiNiuConfig config = new QiNiuConfig(ak, sk);
    RSFClient client = config.getRSFClient();
    List<ListItem> all = new ArrayList<ListItem>();
    ListPrefixRet ret = null;
    String marker = "";
    while (true) {
      ret = client.listPrifix(bucketName, "", marker, 0);
      marker = ret.marker;
      all.addAll(ret.results);
      if (!ret.ok()) {
        break;
      }
    }
    System.out.println(all);
    System.out.println(config.getRSClient().stat(bucketName, "20100617143034?029.jpg"));
    Mac mac = new Mac(ak, sk);
    String baseUrl = URLUtils.makeBaseUrl("wechatmedia.qiniudn.com", "20100617143034?029.jpg");
    GetPolicy getPolicy = new GetPolicy();
    String downloadUrl = getPolicy.makeRequest(baseUrl, mac);
    System.out.println(downloadUrl);
    System.out.println(ImageInfo.call(baseUrl, mac));
    System.out.println(ImageExif.call(baseUrl, mac));
  }
}

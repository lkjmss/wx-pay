package com.jfinal.plugin.tablebind;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Pattern;

import javax.sql.DataSource;

import com.iwonnet.wx.model.BaseModel;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.IDataSourceProvider;
import com.jfinal.plugin.activerecord.Model;

public class AutoTableBindPlugin extends ActiveRecordPlugin {

  private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

  public static enum TableNameStyle {
    UP, LOWER, UP_UNDERLINE, LOWER_UNDERLINE
  }

  private TableNameStyle tableNameStyle;

  public AutoTableBindPlugin(DataSource dataSource) {
    super(dataSource);
  }

  public AutoTableBindPlugin(DataSource dataSource, TableNameStyle tableNameStyle) {
    super(dataSource);
    this.tableNameStyle = tableNameStyle;
  }

  public AutoTableBindPlugin(IDataSourceProvider dataSourceProvider) {
    super(dataSourceProvider);
  }

  public AutoTableBindPlugin(IDataSourceProvider dataSourceProvider, TableNameStyle tableNameStyle) {
    super(dataSourceProvider);
    this.tableNameStyle = tableNameStyle;
  }

  @SuppressWarnings({ "rawtypes", "unchecked" })
  public boolean start() {
    StringBuffer autoPrint = new StringBuffer();
    autoPrint.append("Auto bind table model ------- ").append(sdf.format(new Date()))
        .append(" ------------------------------").append("\n");
    List<Class> modelList = ClassSearcher.findClasses(BaseModel.class);
    autoPrint.append("Model size  : ").append(modelList.size()).append("\n");
    for (Class<? extends Model<?>> modelClass : modelList) {
      Table tb = modelClass.getAnnotation(Table.class);
      if (tb != null) {
        if (StrKit.notBlank(tb.pkName())) {
          this.addMapping(tb.tableName(), tb.pkName(), modelClass);
          autoPrint.append("* TableName : ").append(tb.tableName()).append("[PkName:")
              .append(tb.pkName()).append(",").append("ModelClass: ")
              .append(modelClass.getSimpleName()).append(".(").append(modelClass.getSimpleName())
              .append(".java:1)]").append("\n");
        } else {
          this.addMapping(tb.tableName(), modelClass);
          autoPrint.append("* TableName : ").append(tb.tableName()).append("[PkName:").append("ID")
              .append(",").append("ModelClass: ").append(modelClass.getSimpleName()).append(".(")
              .append(modelClass.getSimpleName()).append(".java:1)]").append("\n");
        }
      } else {
        String tableName = modelClass.getSimpleName();
        if (tableNameStyle == TableNameStyle.UP) {
          tableName = tableName.toUpperCase();
        } else if (tableNameStyle == TableNameStyle.LOWER) {
          tableName = tableName.toLowerCase();
        } else {
          String regex = "[A-Z]";
          Pattern pattern = Pattern.compile(regex);
          String[] str = pattern.split(tableName);
          StringBuffer sb = new StringBuffer();
          String tmp = null;
          for (int i = 1; i < str.length; i++) {
            if (Pattern.compile("^[A-Z]" + str[i] + "$").matcher(tableName).find()) {
              sb.append(tableName);
            } else {
              tmp = tableName.substring(0, tableName.indexOf(str[i]) + 1);
              tableName = tableName.replace(tmp, "");
              sb.append(tmp).append("_");
              if (Pattern.compile("^[A-Z]" + str[i + 1] + "$").matcher(tableName).find()) {
                sb.append(tableName);
                break;
              }
            }
          }
          if (tableNameStyle == TableNameStyle.LOWER_UNDERLINE)
            tableName = sb.toString().toLowerCase();
          else if (tableNameStyle == TableNameStyle.UP_UNDERLINE)
            tableName = sb.toString().toUpperCase();
          else
            tableName = sb.toString();
        }
        this.addMapping(tableName, modelClass);
        autoPrint.append("* TableName : ").append(tableName).append("[PkName:").append("ID")
            .append(",").append("ModelClass: ").append(modelClass.getSimpleName()).append(".(")
            .append(modelClass.getSimpleName()).append(".java:1)]").append("\n");
      }
    }
    autoPrint
        .append("--------------------------------------------------------------------------------\n");
    System.out.println(autoPrint.toString());
    return super.start();
  }

  public static void main(String[] args) {
    List<Class> modelList = ClassSearcher.findClasses(Model.class);
    for (Class<? extends Model<?>> modelClass : modelList) {
      Table tb = modelClass.getAnnotation(Table.class);
      if (tb != null) {
        if (StrKit.notBlank(tb.pkName())) {
          // this.addMapping(tb.tableName(), tb.pkName(), modelClass);
        } else {
          // this.addMapping(tb.tableName(), modelClass);
        }
      } else {
        String tableName = modelClass.getSimpleName();
        System.out.println(tableName);
        String regex = "[A-Z]";
        Pattern pattern = Pattern.compile(regex);
        String[] str = pattern.split(tableName);
        StringBuffer sb = new StringBuffer();
        String tmp = null;
        for (int i = 1; i < str.length; i++) {
          if (Pattern.compile("^[A-Z]" + str[i] + "$").matcher(tableName).find()) {
            sb.append(tableName);
          } else {
            tmp = tableName.substring(0, tableName.indexOf(str[i]) + 1);
            tableName = tableName.replace(tmp, "");
            sb.append(tmp).append("_");
            if (Pattern.compile("^[A-Z]" + str[i + 1] + "$").matcher(tableName).find()) {
              sb.append(tableName);
              break;
            }
          }
        }
        System.out.println(sb.toString());
        System.out.println("-----------------------------------");
      }
    }
  }
}

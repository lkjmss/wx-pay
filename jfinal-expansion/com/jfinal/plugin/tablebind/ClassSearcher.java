package com.jfinal.plugin.tablebind;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import com.jfinal.kit.ValidateKit;

public class ClassSearcher {

  private static List<File> findFiles(String baseDirName, String targetFileName) {
    List<File> classFiles = new ArrayList<File>();
    String tempName = null;
    File baseDir = new File(baseDirName);
    if (!baseDir.exists() || !baseDir.isDirectory()) {
      System.out.println("文件查找失败：" + baseDirName + "不是一个目录！");
    } else {
      String[] fileList = baseDir.list();
      for (String fileName : fileList) {
        fileName = baseDirName + File.separator + fileName;
        File readFile = new File(fileName);
        if (readFile.isDirectory()) {
          classFiles.addAll(findFiles(fileName, targetFileName));
        } else {
          tempName = readFile.getName();
          // System.out.println(fileName + " --> " +
          // ValidateKit.match(targetFileName, tempName));
          if (ValidateKit.match(targetFileName, tempName))
            classFiles.add(readFile.getAbsoluteFile());
        }
      }
    }
    return classFiles;
  }

  @SuppressWarnings("rawtypes")
  public static List<Class> findClasses(Class clazz) {
    List<Class> classList = new ArrayList<Class>();
    URL classPathUrl = ClassSearcher.class.getResource("/");
    List<File> classFileList = findFiles(classPathUrl.getFile(), ".*\\.class");
    for (File file : classFileList) {
      String className = className(file, "/classes");
      try {
        Class<?> classFromFile = Class.forName(className);
        if (classFromFile.getSuperclass() == clazz)
          classList.add(classFromFile);
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
      }
    }
    return classList;
  }

  private static String className(File classFile, String pre) {
    String objStr = classFile.toString().replaceAll("\\\\", "/");
    String className;
    className = objStr.substring(objStr.indexOf(pre) + pre.length(), objStr.indexOf(".class"));
    if (className.startsWith("/")) {
      className = className.substring(className.indexOf("/") + 1);
    }
    return className.replaceAll("/", ".");
  }
}

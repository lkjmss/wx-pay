/**
 * Copyright (c) 2011-2015, James Zhan 詹波 (jfinal@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.jfinal.plugin.activerecord;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.jfinal.log.Logger;

/**
 * SqlReporter.
 */
public class SqlReporter implements InvocationHandler {

  private Connection conn;
  private static boolean loggerOn = false;
  private static final Logger log = Logger.getLogger(SqlReporter.class);
  private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

  SqlReporter(Connection conn) {
    this.conn = conn;
  }

  public static void setLogger(boolean on) {
    SqlReporter.loggerOn = on;
  }

  @SuppressWarnings("rawtypes")
  Connection getConnection() {
    Class clazz = conn.getClass();
    return (Connection) Proxy.newProxyInstance(clazz.getClassLoader(),
        new Class[] { Connection.class }, this);
  }

  public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
    try {
      if (method.getName().equals("prepareStatement")) {
        StringBuffer sb = new StringBuffer();
        sb.append("SQL Print ------------------- ").append(sdf.format(new Date()))
            .append(" ------------------------------\n");
        sb.append("Sql         : ").append(args[0]);
        if (loggerOn)
          log.info(sb.toString());
        else
          System.out.println(sb.toString());
      }
      return method.invoke(conn, args);
    } catch (InvocationTargetException e) {
      throw e.getTargetException();
    }
  }
}

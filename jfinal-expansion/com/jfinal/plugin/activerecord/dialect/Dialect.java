package com.jfinal.plugin.activerecord.dialect;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.jfinal.plugin.activerecord.DbKit;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.plugin.activerecord.Table;

/**
 * Dialect.
 */
public abstract class Dialect {

	public abstract String forTableBuilderDoBuild(String tableName);

	public abstract void forModelSave(Table table, Map<String, Object> attrs, StringBuilder sql, List<Object> paras);

	public abstract String forModelDeleteById(Table table);

	public abstract void forModelUpdate(Table table, Map<String, Object> attrs, Set<String> modifyFlag, String pKey, Object id, StringBuilder sql, List<Object> paras);

	public abstract String forModelFindById(Table table, String columns);

	public abstract String forDbFindById(String tableName, String primaryKey, String columns);

	public abstract String forDbDeleteById(String tableName, String primaryKey);

	public abstract void forDbSave(StringBuilder sql, List<Object> paras, String tableName, Record record);

	public abstract void forDbUpdate(String tableName, String primaryKey, Object id, Record record, StringBuilder sql, List<Object> paras);

	public abstract void forPaginate(StringBuilder sql, int pageNumber, int pageSize, String select, String sqlExceptSelect);

	public String getQueryStringAll(Table table) {
		StringBuilder sb = new StringBuilder("select ");
		for (String key : table.getColumnTypeMap().keySet()) {
			sb.append("`").append(key).append("`").append(",");
		}
		sb.deleteCharAt(sb.length() - 1).append(" from `").append(table.getName()).append("`");
		return sb.toString();
	}

	public boolean isOracle() {
		return false;
	}

	public boolean isTakeOverDbPaginate() {
		return false;
	}

	public Page<Record> takeOverDbPaginate(Connection conn, int pageNumber, int pageSize, String select, String sqlExceptSelect, Object... paras) throws SQLException {
		throw new RuntimeException("You should implements this method in " + getClass().getName());
	}

	public boolean isTakeOverModelPaginate() {
		return false;
	}

	@SuppressWarnings("rawtypes")
	public Page takeOverModelPaginate(Connection conn, Class<? extends Model> modelClass, int pageNumber, int pageSize, String select, String sqlExceptSelect, Object... paras) throws Exception {
		throw new RuntimeException("You should implements this method in " + getClass().getName());
	}

	public void fillStatement(PreparedStatement pst, List<Object> paras) throws SQLException {
		boolean isShowSql = DbKit.getConfig().isShowSql();
		StringBuffer sb = new StringBuffer();
		sb.append("Parameter   : ");
		for (int i = 0, size = paras.size(); i < size; i++) {
			pst.setObject(i + 1, paras.get(i));
			if (isShowSql)
				sb.append((i == 0) ? paras.get(i) : "," + paras.get(i));
		}
		sb.append("\n--------------------------------------------------------------------------------\n");
		if (isShowSql)
			System.out.print(sb.toString());
	}

	public void fillStatement(PreparedStatement pst, Object... paras) throws SQLException {
		boolean isShowSql = DbKit.getConfig().isShowSql();
		StringBuffer sb = new StringBuffer();
		sb.append("Parameter   : ");
		for (int i = 0; i < paras.length; i++) {
			pst.setObject(i + 1, paras[i]);
			if (isShowSql)
				sb.append((i == 0) ? paras[i] : "," + paras[i]);
		}
		sb.append("\n--------------------------------------------------------------------------------\n");
		if (isShowSql)
			System.out.print(sb.toString());
	}

	public String getDefaultPrimaryKey() {
		return "id";
	}
}

package com.iwonnet.wx.controller;

import com.iwonnet.wx.service.IWXService;
import com.jfinal.aop.Before;
import com.jfinal.core.Controller;
import com.jfinal.ext.interceptor.NotAction;
import com.jfinal.kit.HttpKit;
import com.jfinal.log.Logger;
import com.jfinal.plugin.spring.Inject;
import com.jfinal.weixin.sdk.api.ApiConfig;
import com.jfinal.weixin.sdk.jfinal.WeixinInterceptor;
import com.jfinal.weixin.sdk.kit.MsgEncryptKit;
import com.jfinal.weixin.sdk.msg.InMsgParaser;
import com.jfinal.weixin.sdk.msg.OutMsgXmlBuilder;
import com.jfinal.weixin.sdk.msg.in.InImageMsg;
import com.jfinal.weixin.sdk.msg.in.InLinkMsg;
import com.jfinal.weixin.sdk.msg.in.InLocationMsg;
import com.jfinal.weixin.sdk.msg.in.InMsg;
import com.jfinal.weixin.sdk.msg.in.InTextMsg;
import com.jfinal.weixin.sdk.msg.in.InVideoMsg;
import com.jfinal.weixin.sdk.msg.in.InVoiceMsg;
import com.jfinal.weixin.sdk.msg.in.event.InFollowEvent;
import com.jfinal.weixin.sdk.msg.in.event.InLocationEvent;
import com.jfinal.weixin.sdk.msg.in.event.InMenuEvent;
import com.jfinal.weixin.sdk.msg.in.event.InQrCodeEvent;
import com.jfinal.weixin.sdk.msg.in.event.InTemplateEvnet;
import com.jfinal.weixin.sdk.msg.in.speech_recognition.InSpeechRecognitionResults;
import com.jfinal.weixin.sdk.msg.out.OutMsg;
import com.jfinal.weixin.sdk.msg.out.OutTextMsg;

/**
 * 接收微信服务器消息，自动解析成 InMsg 并分发到相应的处理方法
 */
public class WeixinController extends Controller {

  @Inject.BY_NAME
  private IWXService wxService;
  private static final Logger log = Logger.getLogger(WeixinController.class);
  private String inMsgXml = null; // 本次请求 xml数据
  private InMsg inMsg = null; // 本次请求 xml 解析后的 InMsg 对象

  /**
   * weixin 公众号服务器调用唯一入口，即在开发者中心输入的 URL 必须要指向此 action
   */
  @Before(WeixinInterceptor.class)
  public void index() {
    // 开发模式输出微信服务发送过来的 xml 消息
    if (ApiConfig.isDevMode()) {
      System.out.println("接收消息:");
      System.out.println(getInMsgXml());
    }
    // 解析消息并根据消息类型分发到相应的处理方法
    String appid = getPara();
    InMsg msg = getInMsg();
    OutMsg outMsg = null;
    if (msg instanceof InTextMsg)
      outMsg = wxService.processInTextMsg(appid, (InTextMsg) msg);
    else if (msg instanceof InImageMsg)
      outMsg = wxService.processInImageMsg(appid, (InImageMsg) msg);
    else if (msg instanceof InVoiceMsg)
      outMsg = wxService.processInVoiceMsg(appid, (InVoiceMsg) msg);
    else if (msg instanceof InVideoMsg)
      outMsg = wxService.processInVideoMsg(appid, (InVideoMsg) msg);
    else if (msg instanceof InLocationMsg)
      outMsg = wxService.processInLocationMsg(appid, (InLocationMsg) msg);
    else if (msg instanceof InLinkMsg)
      outMsg = wxService.processInLinkMsg(appid, (InLinkMsg) msg);
    else if (msg instanceof InFollowEvent)
      outMsg = wxService.processInFollowEvent(appid, (InFollowEvent) msg);
    else if (msg instanceof InQrCodeEvent)
      outMsg = wxService.processInQrCodeEvent(appid, (InQrCodeEvent) msg);
    else if (msg instanceof InLocationEvent)
      outMsg = wxService.processInLocationEvent(appid, (InLocationEvent) msg);
    else if (msg instanceof InMenuEvent)
      outMsg = wxService.processInMenuEvent(appid, (InMenuEvent) msg);
    else if (msg instanceof InTemplateEvnet)
      outMsg = wxService.processInTemplateEvent(appid, (InTemplateEvnet) msg);
    else if (msg instanceof InSpeechRecognitionResults)
      outMsg = wxService.processInSpeechRecognitionResults(appid, (InSpeechRecognitionResults) msg);
    else
      log.error("未能识别的消息类型。 消息 xml 内容为：\n" + getInMsgXml());
    if (outMsg != null) {
      render(outMsg);
    } else
      renderNull();
  }

  /**
   * 在接收到微信服务器的 InMsg 消息后后响应 OutMsg 消息
   */
  public void render(OutMsg outMsg) {
    String outMsgXml = OutMsgXmlBuilder.build(outMsg);
    // 开发模式向控制台输出即将发送的 OutMsg 消息的 xml 内容
    if (ApiConfig.isDevMode()) {
      System.out.println("发送消息:");
      System.out.println(outMsgXml);
      System.out.println("--------------------------------------------------------------------------------\n");
    }

    // 是否需要加密消息
    if (ApiConfig.isEncryptMessage()) {
      outMsgXml = MsgEncryptKit.encrypt(outMsgXml, getPara("timestamp"), getPara("nonce"));
    }

    renderText(outMsgXml, "text/xml");
  }

  public void renderOutTextMsg(String content) {
    OutTextMsg outMsg = new OutTextMsg(getInMsg());
    outMsg.setContent(content);
    render(outMsg);
  }

  @Before(NotAction.class)
  public String getInMsgXml() {
    if (inMsgXml == null) {
      inMsgXml = HttpKit.readIncommingRequestData(getRequest());
      // 是否需要解密消息
      if (ApiConfig.isEncryptMessage()) {
        inMsgXml = MsgEncryptKit.decrypt(inMsgXml, getPara("timestamp"), getPara("nonce"),
            getPara("msg_signature"));
      }
    }
    return inMsgXml;
  }

  @Before(NotAction.class)
  public InMsg getInMsg() {
    if (inMsg == null)
      inMsg = InMsgParaser.parse(getInMsgXml());
    return inMsg;
  }
}

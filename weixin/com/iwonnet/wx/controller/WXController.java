package com.iwonnet.wx.controller;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.jfinal.log.Logger;
import com.jfinal.weixin.sdk.api.ApiResult;
import com.jfinal.weixin.sdk.api.CustomServiceAPI;
import com.jfinal.weixin.sdk.msg.in.InImageMsg;
import com.jfinal.weixin.sdk.msg.in.InLinkMsg;
import com.jfinal.weixin.sdk.msg.in.InLocationMsg;
import com.jfinal.weixin.sdk.msg.in.InTextMsg;
import com.jfinal.weixin.sdk.msg.in.InVideoMsg;
import com.jfinal.weixin.sdk.msg.in.InVoiceMsg;
import com.jfinal.weixin.sdk.msg.in.event.InFollowEvent;
import com.jfinal.weixin.sdk.msg.in.event.InLocationEvent;
import com.jfinal.weixin.sdk.msg.in.event.InMenuEvent;
import com.jfinal.weixin.sdk.msg.in.event.InQrCodeEvent;
import com.jfinal.weixin.sdk.msg.in.event.InTemplateEvnet;
import com.jfinal.weixin.sdk.msg.in.speech_recognition.InSpeechRecognitionResults;
import com.jfinal.weixin.sdk.msg.out.OutImageMsg;
import com.jfinal.weixin.sdk.msg.out.OutNewsMsg;
import com.jfinal.weixin.sdk.msg.out.OutServiceMsg;
import com.jfinal.weixin.sdk.msg.out.OutTextMsg;
import com.jfinal.weixin.sdk.msg.out.OutVoiceMsg;

@Deprecated
public class WXController extends WeixinController {

  private static final String helpStr = "\t您好，目前公众号处于测试阶段，您目前可以点击菜单中的操作手册获取每一个功能操作说明，您也可以联系客服直接联系在线客服提问；长三角地区（浙江，江苏）用户可致电：4000-018-028，西南地区（重庆）用户可致电：400-023-1171进行电话指导。公众号持续更新中，想要更多惊喜欢迎每天关注 ^_^";
  private static final Logger log = Logger.getLogger(WXController.class);

  /**
   * 实现父类抽方法，处理文本消息 本例子中根据消息中的不同文本内容分别做出了不同的响应，同时也是为了测试 jfinal weixin sdk的基本功能：
   * 本方法仅测试了 OutTextMsg、OutNewsMsg、OutMusicMsg 三种类型的OutMsg， 其它类型的消息会在随后的方法中进行测试
   */
  protected void processInTextMsg(String appid, InTextMsg inTextMsg) {
    /**
     * 将Help放入数据库，通过appid进行动态获取
     */
    System.out.println(appid);
    String msgContent = inTextMsg.getContent().trim();
    // 帮助提示
    if ("help".equalsIgnoreCase(msgContent)) {
      OutTextMsg outMsg = new OutTextMsg(inTextMsg);
      outMsg.setContent(helpStr);
      render(outMsg);
    } else {
      OutTextMsg outMsg = new OutTextMsg(inTextMsg);
      outMsg.setContent("\t文本消息已成功接收，内容为： " + inTextMsg.getContent() + "\n\n" + helpStr);
      render(outMsg);
    }
  }

  /**
   * 实现父类抽方法，处理图片消息
   */
  protected void processInImageMsg(String appid, InImageMsg inImageMsg) {
    OutImageMsg outMsg = new OutImageMsg(inImageMsg);
    // 将刚发过来的图片再发回去
    outMsg.setMediaId(inImageMsg.getMediaId());
    render(outMsg);
  }

  /**
   * 实现父类抽方法，处理语音消息
   */
  protected void processInVoiceMsg(String appid, InVoiceMsg inVoiceMsg) {
    OutVoiceMsg outMsg = new OutVoiceMsg(inVoiceMsg);
    // 将刚发过来的语音再发回去
    outMsg.setMediaId(inVoiceMsg.getMediaId());
    render(outMsg);
  }

  /**
   * 实现父类抽方法，处理视频消息
   */
  protected void processInVideoMsg(String appid, InVideoMsg inVideoMsg) {
    /*
     * 腾讯 api 有 bug，无法回复视频消息，暂时回复文本消息代码测试 OutVideoMsg outMsg = new
     * OutVideoMsg(inVideoMsg); outMsg.setTitle("OutVideoMsg 发送");
     * outMsg.setDescription("刚刚发来的视频再发回去"); // 将刚发过来的视频再发回去，经测试证明是腾讯官方的 api 有
     * bug，待 api bug 却除后再试 outMsg.setMediaId(inVideoMsg.getMediaId());
     * render(outMsg);
     */
    OutTextMsg outMsg = new OutTextMsg(inVideoMsg);
    outMsg.setContent("\t视频消息已成功接收，该视频的 mediaId 为: " + inVideoMsg.getMediaId());
    render(outMsg);
  }

  /**
   * 实现父类抽方法，处理地址位置消息
   */
  protected void processInLocationMsg(String appid, InLocationMsg inLocationMsg) {
    OutTextMsg outMsg = new OutTextMsg(inLocationMsg);
    outMsg.setContent("已收到地理位置消息:" + "\nlocation_X = " + inLocationMsg.getLocation_X()
        + "\nlocation_Y = " + inLocationMsg.getLocation_Y() + "\nscale = "
        + inLocationMsg.getScale() + "\nlabel = " + inLocationMsg.getLabel());
    render(outMsg);
  }

  /**
   * 实现父类抽方法，处理链接消息 特别注意：测试时需要发送我的收藏中的曾经收藏过的图文消息，直接发送链接地址会当做文本消息来发送
   */
  protected void processInLinkMsg(String appid, InLinkMsg inLinkMsg) {
    OutNewsMsg outMsg = new OutNewsMsg(inLinkMsg);
    outMsg
        .addNews(
            "链接消息已成功接收",
            "链接使用图文消息的方式发回给你，还可以使用文本方式发回。点击图文消息可跳转到链接地址页面，是不是很好玩 :)",
            "http://mmbiz.qpic.cn/mmbiz/zz3Q6WSrzq1ibBkhSA1BibMuMxLuHIvUfiaGsK7CC4kIzeh178IYSHbYQ5eg9tVxgEcbegAu22Qhwgl5IhZFWWXUw/0",
            inLinkMsg.getUrl());
    render(outMsg);
  }

  /**
   * 实现父类抽方法，处理关注/取消关注消息
   */
  protected void processInFollowEvent(InFollowEvent inFollowEvent) {
    OutTextMsg outMsg = new OutTextMsg(inFollowEvent);
    outMsg.setContent("感谢关注EasyHIS产品微信公众平台\n\n浙江网能信息技术有限公司");
    // 如果为取消关注事件，将无法接收到传回的信息
    render(outMsg);
  }

  /**
   * 实现父类抽方法，处理扫描带参数二维码事件
   */
  protected void processInQrCodeEvent(InQrCodeEvent inQrCodeEvent) {
    OutTextMsg outMsg = new OutTextMsg(inQrCodeEvent);
    outMsg.setContent("processInQrCodeEvent() 方法测试成功");
    render(outMsg);
  }

  /**
   * 实现父类抽方法，处理上报地理位置事件
   */
  protected void processInLocationEvent(InLocationEvent inLocationEvent, String appid) {
    // OutTextMsg outMsg = new OutTextMsg(inLocationEvent);
    // outMsg.setContent("processInLocationEvent() 方法测试成功");
    // render(outMsg);
    renderNull();
  }

  /**
   * 实现父类抽方法，处理自定义菜单事件
   */
  protected void processInMenuEvent(String appid, InMenuEvent inMenuEvent) {
    // renderOutTextMsg("你点击了自定义菜单事件，返回值：" + inMenuEvent.getEventKey());
    String eventKey = inMenuEvent.getEventKey();
    if ("V1002_CUSTOMER_SERVICE".equals(eventKey)) {
      ApiResult online = CustomServiceAPI.getOnlinekflist();
      JSONObject json = online.getJson();
      if (json != null) {
        JSONArray kfArray = json.getJSONArray("kf_online_list");
        if (kfArray != null && kfArray.size() > 0) {
          render(new OutServiceMsg(inMenuEvent));
        } else {
          renderOutTextMsg("当前为下班时间，请联系:\n4000-018-028(长三角地区)\n400-023-1171(重庆地区)");
        }
      } else {
        log.error("客服系统出现异常");
        renderOutTextMsg("客服系统异常，请联系:\n4000-018-028(长三角地区)\n400-023-1171(重庆地区)");
      }
    } else if ("V1001_CPFW_CPJX".equals(eventKey)) {
      // 产品介绍
      OutNewsMsg outMsg = new OutNewsMsg(inMenuEvent);
      outMsg
          .addNews(
              "产品介绍",
              "EasyHIS系统是医疗卫生应用集成平台（ESIP）的重要组成部分，是针对中小型医院药店的实际业务需求研发的一体化在线HIS系统，采用SAAS（Software as a Service：软件即服务）的运维模式，面向中小型医疗机构提供专业技术服务，为医院、药店提供内部诊疗管理和销售管理，实现与城镇居民医保、新农合等各种医保结算系统的实时交易结算。系统能有效地降低区县级中小型医疗机构、药店的信息化成本，提升信息化水平和管理能力，提高信息化投资回报率。",
              "http://www.iwonnet.com/CPJS.jpg", "http://www.iwonnet.com/wx/cpjs/cpjs.html");
      render(outMsg);
    } else if ("V2001_CZSC_GYJS".equals(eventKey)) {
      // 药店指南
      OutNewsMsg outMsg = new OutNewsMsg(inMenuEvent);
      outMsg.addNews("药店指南", "关于药店以及医院门诊部的医保（自费）购药结算的操作说明", "http://www.iwonnet.com/GYJS.jpg",
          "http://www.iwonnet.com/wx/Pharmacy/yaodian.html");
      render(outMsg);
    } else if ("V2002_CZSC_ZYJS".equals(eventKey)) {
      // 医院指南
      OutNewsMsg outMsg = new OutNewsMsg(inMenuEvent);
      outMsg.addNews("医院指南", "关于医院住院部的医保（自费）住院结算，病房病床管理，每日医嘱设定、复核、执行、每日清单打印等操作说明",
          "http://www.iwonnet.com/ZYJS.jpg", "http://www.iwonnet.com/wx/hospital/yiyuan.html");
      render(outMsg);
    } else if ("V2003_CZSC_YBDZ".equals(eventKey)) {
      // Del
      OutNewsMsg outMsg = new OutNewsMsg(inMenuEvent);
      outMsg.addNews("医保对账", "关于医疗机构医保刷卡的日对账、月对账的操作说明", "http://www.iwonnet.com/YBDZ.jpg",
          "http://www.iwonnet.com/wx/index.html");
      render(outMsg);
    } else if ("V2004_CZSC_KFCZ".equals(eventKey)) {
      // 库房指南
      OutNewsMsg outMsg = new OutNewsMsg(inMenuEvent);
      outMsg.addNews("库房指南", "关于库房的日常管理和出入库管理", "http://www.iwonnet.com/KFCZ.jpg",
          "http://www.iwonnet.com/wx/inventory/kufang.html");
      render(outMsg);
    } else if ("V2005_CZSC_BBDY".equals(eventKey)) {
      // Del
      OutNewsMsg outMsg = new OutNewsMsg(inMenuEvent);
      outMsg.addNews("报表打印", "EasyHIS中各种报表说明以及出处", "http://www.iwonnet.com/BBDY.jpg",
          "http://www.iwonnet.com/wx/index.html");
      render(outMsg);
    } else {
      renderOutTextMsg("本微信目前处于测试阶段，此功能暂未实现，将于近期完成上线...");
    }
  }

  /**
   * 实现父类抽方法，处理接收语音识别结果
   */
  protected void processInSpeechRecognitionResults(String appid,
      InSpeechRecognitionResults inSpeechRecognitionResults) {
    renderOutTextMsg("语音识别结果： " + inSpeechRecognitionResults.getRecognition());
  }

  protected void processInTemplateEvent(String appid, InTemplateEvnet inTemplateEvnet) {
    renderNull();
  }
}

package com.iwonnet.wx.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.iwonnet.wx.openapi.AuthInterceptor;
import com.iwonnet.wx.openapi.token.AuthToken;
import com.iwonnet.wx.payapi.PayUtil;
import com.iwonnet.wx.service.IPayService;
import com.jfinal.aop.Before;
import com.jfinal.aop.ClearInterceptor;
import com.jfinal.core.ActionKey;
import com.jfinal.core.Controller;
import com.jfinal.core.JFinal;
import com.jfinal.ext.interceptor.NoUrlPara;
import com.jfinal.ext.interceptor.POST;
import com.jfinal.kit.HttpKit;
import com.jfinal.plugin.spring.Inject;

@Before({ NoUrlPara.class, POST.class, AuthInterceptor.class })
public class WXPayController extends Controller {

	private static final Log LOGGER = LogFactory.getLog(WXPayController.class);

	private static final boolean DEV_MODE = JFinal.me().getConstants().getDevMode();

	@Inject.BY_NAME
	private IPayService payService;

	private AuthToken token;

	private void setErrorMessage(String errmsg, Throwable t, JSONObject json) {
		LOGGER.error(t.getMessage(), t);
		json.put("errcode", "-1");
		json.put("errmsg", errmsg);
		json.put("errdetail", t.getMessage());
	}

	/**
	 * OAuth2.0跳转商城
	 */
	@ClearInterceptor
	@Before(NoUrlPara.class)
	public void index() {
		String openid = null;
		String code = getPara("code");
		String state = getPara("state");
		if (DEV_MODE) {
			System.out.println("OAuth2.0: code=" + code + ", wxid=" + state);
		}
		try {
			openid = payService.getOpenid(code, state);
			if (DEV_MODE) {
				System.out.println("openid=" + openid);
			}
			redirect("http://www.iwonnet.com:5678/Business/mobile_toZfcs.ac?wxOpenID=" + openid + "&wxid=" + state);
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			renderText("跳转失败，请联系客服！");
		}
	}

	@ClearInterceptor
	public void sendPicture() {
		String result = "";
		try {
			String nc = new String(getPara("nc").getBytes("ISO-8859-1"), "UTF-8");
			result = payService.sendPicture(nc, getPara("url"));
		} catch (Exception e) {
			e.printStackTrace();
			StringWriter sw = null;
			PrintWriter pw = null;
			try {
				sw = new StringWriter();
				pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				result = sw.toString();
			} finally {
				pw.close();
				try {
					sw.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
		renderText(result);
	}

	/**
	 * 支付结果通知
	 */
	@ClearInterceptor
	@Before(POST.class)
	@ActionKey(value = "paynotify")
	public void payNotify() {
		Map<String, String> result = new HashMap<String, String>();
		String attach = getPara();
		String notifyXml = HttpKit.readIncommingRequestData(getRequest());
		try {
			payService.responseNotify(attach, notifyXml);
			result.put("return_code", "SUCCESS");
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			result.put("return_code", "FAIL");
			result.put("return_msg", e.getMessage());
		}
		try {
			renderText(PayUtil.mapToXml(result));
		} catch (Exception e) {
			LOGGER.error("响应支付结果通知出错！", e);
		}
	}

	/**
	 * 退款结果通知
	 */
	@ClearInterceptor
	@Before({ NoUrlPara.class })
	@ActionKey(value = "refundnotify")
	public void refundNotify() {
		Map<String, String> result = new HashMap<String, String>();
		String notifyXml = HttpKit.readIncommingRequestData(getRequest());
		try {
			payService.refundNotify(notifyXml);
			result.put("return_code", "SUCCESS");
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			result.put("return_code", "FAIL");
			result.put("return_msg", e.getMessage());
		}
		try {
			renderText(PayUtil.mapToXml(result));
		} catch (Exception e) {
			LOGGER.error("响应退款结果通知出错！", e);
		}
	}

	/**
	 * 统一下单
	 */
	public void unifiedOrder() {
		JSONObject json = new JSONObject();
		try {
			Map<String, String> result = payService.unifiedOrder(token.getWxid(), getPara("orderInfo"));
			json.put("errcode", "1");
			json.put("data", result);
		} catch (Exception e) {
			setErrorMessage("请求失败！", e, json);
		}
		renderJson(json.toString());
	}

	/**
	 * 查询订单
	 */
	public void orderQuery() {
		JSONObject json = new JSONObject();
		try {
			Map<String, String> result = payService.orderQuery(token.getWxid(), getPara("orderNo"));
			json.put("errcode", "1");
			json.put("data", result);
		} catch (Exception e) {
			setErrorMessage("请求失败！", e, json);
		}
		renderJson(json.toString());
	}

	/**
	 * 关闭订单
	 */
	public void closeOrder() {
		JSONObject json = new JSONObject();
		try {
			Map<String, String> result = payService.closeOrder(token.getWxid(), getPara("orderNo"));
			json.put("errcode", "1");
			json.put("data", result);
		} catch (Exception e) {
			setErrorMessage("请求失败！", e, json);
		}
		renderJson(json.toString());
	}

	/**
	 * 申请退款
	 */
	public void refund() {
		JSONObject json = new JSONObject();
		try {
			Map<String, String> result = payService.refund(token.getWxid(), getPara("refundInfo"));
			json.put("errcode", "1");
			json.put("data", result);
		} catch (Exception e) {
			setErrorMessage("请求失败！", e, json);
		}
		renderJson(json.toString());
	}

	/**
	 * 退款查询
	 */
	public void refundQuery() {
		JSONObject json = new JSONObject();
		try {
			Map<String, String> result = payService.refundQuery(token.getWxid(), getPara("refundNo"));
			json.put("errcode", "1");
			json.put("data", result);
		} catch (Exception e) {
			setErrorMessage("请求失败！", e, json);
		}
		renderJson(json.toString());
	}
}

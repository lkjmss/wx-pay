package com.iwonnet.wx.controller;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

import com.jfinal.core.Controller;

public class IndexController extends Controller {

	public void upload() {
		render("/upload.html");
	}

	public void s() {
		// renderJsp("/wx/view.jsp");
		HttpServletRequest request= getRequest();
		StringBuilder sb = new StringBuilder();
		Enumeration<?> enumeration = getRequest().getHeaderNames();
		while (enumeration.hasMoreElements()) {
			String o = (String) enumeration.nextElement();
			sb.append(o).append(": ").append(request.getHeader(o)).append("\n");
		}
		System.out.println(sb);
		renderText(sb.toString());
	}
}

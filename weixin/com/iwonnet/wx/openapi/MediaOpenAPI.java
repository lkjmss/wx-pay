package com.iwonnet.wx.openapi;

import java.io.IOException;
import java.util.List;

import com.iwonnet.wx.model.BcDmt;
import com.iwonnet.wx.openapi.token.AuthToken;
import com.iwonnet.wx.service.IWebService;
import com.iwonnet.wx.utils.EnumUtil;
import com.iwonnet.wx.utils.ModelUtil;
import com.iwonnet.wx.utils.StringUtil;
import com.jfinal.aop.ClearInterceptor;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.spring.Inject;
import com.jfinal.upload.UploadFile;
import com.jfinal.weixin.sdk.api.ReturnCode;

public class MediaOpenAPI extends WeixinOpenAPI {

  @Inject.BY_NAME
  private IWebService webService;

  @ClearInterceptor
  public void upload() throws IOException {
    UploadFile uploadFile = getFile();
    String yhbh = getPara("yhbh");
    String item = getPara("item");
    if (StrKit.isBlank(yhbh)) {
      renderJson(ReturnCode.bulidError(40002));// 不合法的凭证类型
      return;
    }
    String retVal = webService.fileUpload(yhbh, item, uploadFile, getPara("description"),
        getPara("sfgx"));
    renderJson(retVal);
  }

  public void get() {

  }

  /**
   * 获取影像列表
   */
  @ClearInterceptor
  public void list() {
    String retVal = "{}";
    int pageSize = 10;
    String fzbh = getPara("item");
    String pagePara = getPara("pageSize");
    if (StrKit.notBlank(pagePara)) {
      pageSize = new Integer(pagePara);
    }
    String fz_sql = " and fzbh";
    if (StrKit.isBlank(fzbh) || fzbh.equals("1") || fzbh.equals("null")) {
      fz_sql += " is null ";
    } else {
      fz_sql += (" = '" + fzbh + "' ");
    }
    Integer pageNumber = getParaToInt("pageNumber");
    String yhbh = getPara("yhbh");
    String ispic = getPara("ispic");
    String sql = "from bc_dmt where yhbh = ? and zxbz = '0' " + fz_sql + " order by czsj desc";
    Object[] para = new Object[] { yhbh };
    if ("1".equals(ispic) || "true".equalsIgnoreCase(ispic)) {
      sql = "from bc_dmt where yhbh = ? and yxlx = ? and zxbz = '0' " + fz_sql
          + " order by czsj desc";
      para = new Object[] { yhbh, EnumUtil.UploadFileType.image.getCode() };
    }
    if (StrKit.notNull(pageNumber)) {
      Page<BcDmt> page = BcDmt.Dao.paginate(pageNumber, pageSize, "select * ", sql, para);
      retVal = ModelUtil.conver(page).toString();
    } else {
      List<BcDmt> list = BcDmt.Dao.find("select * " + sql, para);
      retVal = ModelUtil.conver(list).toString();
    }
    renderJson(retVal);
  }

  /**
   * 上传多媒体材料到微信端<br>
   * 需要传入参数：yxbh
   * 
   * @date 2015-3-20
   * @author zhongjf
   */
  public void uploadnews() {
    AuthToken token = getToken();
    String access_token = token.getAccessToken();
    Long wxid = token.getWxid();
    Long yxbh = StringUtil.objectToLong(getPara("yxbh"));
    String result = webService.uploadnews(access_token, yxbh, wxid);
    renderJson(result);
  }
  
  /**
   * 上传多媒体文件(图文信息)
   * @author xz 2015-3-25
   *
   *
   * @return void
   */
  public void messageUploadNews() {
    AuthToken token = getToken();
    String access_token = token.getAccessToken();
    Long wxid = token.getWxid();
    Long dlbh = StringUtil.objectToLong(getPara("dlbh"));
    String result = webService.messageUploadNews(access_token, wxid, dlbh);
    renderJson(result);
  }
}

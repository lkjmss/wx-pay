package com.iwonnet.wx.openapi;

import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.iwonnet.wx.model.WxCd;
import com.iwonnet.wx.openapi.cache.CacheManager;
import com.iwonnet.wx.openapi.token.AuthToken;
import com.iwonnet.wx.openapi.token.CacheTokenKit;
import com.iwonnet.wx.service.IWebService;
import com.iwonnet.wx.utils.BeanUtil;
import com.iwonnet.wx.utils.EnumUtil;
import com.iwonnet.wx.utils.StringUtil;
import com.jfinal.plugin.spring.Inject;
import com.jfinal.weixin.sdk.api.MenuApi;
import com.jfinal.weixin.sdk.kit.ParaMap;

public class MenuOpenAPI extends WeixinOpenAPI {

  @Inject.BY_NAME
  private IWebService webService;

  public void create() {
    AuthToken authToken = super.getToken();
    String _first_menu_sql = "select * from wx_cd where wxid = ? and cdjb = '1' order by pxm";
    String chrild_menu_sql = "select * from wx_cd where wxid = ? and cdjb = '2' and sjbh = ? order by pxm";
    JSONArray menu_array = new JSONArray();
    List<WxCd> _first_cd = WxCd.dao.find(_first_menu_sql, authToken.getWxid());
    for (WxCd wxCd : _first_cd) {
      JSONObject cd_Json = new JSONObject();
      cd_Json.put("name", wxCd.getStr("MC"));
      String type = "clcik", key;
      List<WxCd> chrild_cd = WxCd.dao.find(chrild_menu_sql, new Object[] { authToken.getWxid(),
          wxCd.get("CDBH") });
      if (chrild_cd == null || chrild_cd.size() == 0) {
        key = StringUtil.objectToString(wxCd.get("URL"));
        type = EnumUtil.XYLX.VIEW.getValue();
        if(BeanUtil.isEmpty(key)){
        	key =  StringUtil.objectToString(wxCd.get("WBNR"));
        	type = EnumUtil.XYLX.CLCIK.getValue();
        }
//        if (wxCd.getStr("XYLX").equals(EnumUtil.XYLX.VIEW.getCode())) {
//          type = EnumUtil.XYLX.VIEW.getValue();
//          key = wxCd.getStr("URL");
//        } else {
//          type = EnumUtil.XYLX.CLCIK.getValue();
//          key = wxCd.getStr("WBNR");
//        }
        cd_Json.put("type", type);
        cd_Json.put(type.equals(EnumUtil.XYLX.VIEW.getValue()) ? "url" : "key", key);
      } else {
        JSONArray sub_button = new JSONArray();
        for (WxCd sub_wxcd : chrild_cd) {
          ParaMap p = ParaMap.create("name", sub_wxcd.getStr("MC"));
          String pType = EnumUtil.XYLX.VIEW.getValue();
          String pKey = StringUtil.objectToString(sub_wxcd.get("URL"));
          if(BeanUtil.isEmpty(sub_wxcd.get("URL"))){
          	pType = EnumUtil.XYLX.CLCIK.getValue();
          	pKey = StringUtil.objectToString(sub_wxcd.get("WBNR"));
          }
          p.put("type",pType);
          p.put(pType.equals(EnumUtil.XYLX.VIEW.getValue()) ? "url" : "key", pKey);
//          if (EnumUtil.XYLX.VIEW.getCode().equals(sub_wxcd.getStr("XYLX"))) {
//            p.put("type", EnumUtil.XYLX.VIEW.getValue()).put("url", sub_wxcd.getStr("URL"));
//          } else {
//            p.put("type", EnumUtil.XYLX.CLCIK.getValue()).put("key", sub_wxcd.getStr("WBNR"));
//          }
          sub_button.add(p.getData());
        }
        cd_Json.put("sub_button", sub_button);
      }
      menu_array.add(cd_Json);
    }
    JSONObject create_menu = new JSONObject();
    create_menu.put("button", menu_array);
    System.out.println(create_menu);
    String retVal = MenuApi.createMenu(authToken.getAccessToken(), create_menu.toString())
        .getJsonStr();
    System.out.println(retVal);
    renderJson(retVal);
  }

  public void get() {
    String retVal = "{}";
    AuthToken authToken = CacheTokenKit.getCacheToken(getPara("access_token"), "access_token");
    JSONObject retJSON = MenuApi.getMenu(CacheManager.getAccessToken(authToken.getToken()))
        .getJson();
    renderJson(retVal);
  }
}

package com.iwonnet.wx.openapi;

import java.io.UnsupportedEncodingException;

import com.iwonnet.wx.openapi.token.AuthToken;
import com.iwonnet.wx.service.IWebService;
import com.iwonnet.wx.utils.StringUtil;
import com.jfinal.plugin.spring.Inject;
import com.jfinal.weixin.sdk.api.ReturnCode;

public class UserOpenApi extends WeixinOpenAPI {

  @Inject.BY_NAME
  private IWebService webService;

  // /api/group/create
  /**
   * 修改分组
   */
  public void rename() {
    AuthToken token = getToken();
    String access_token = token.getAccessToken();
    String openid = getPara("openid");
    String name, jsonStr;
    try {
      name = new String(getPara("name").getBytes("ISO-8859-1"), "UTF-8");
      jsonStr = webService.updateremark(access_token, openid, name);
    } catch (UnsupportedEncodingException e) {
      jsonStr = ReturnCode.bulidError(-1, e.getMessage());
    }
    renderJson(jsonStr);
  }

  public void getInfo() {
    AuthToken token = getToken();
    String access_token = token.getAccessToken();
    String openid = getPara("openid");
    Long wxid = StringUtil.objectToLong(getPara("wxid"));
    String jsonStr = webService.getUserInfo(access_token, openid, wxid);
    renderJson(jsonStr);
  }

  public void get() {
    AuthToken token = getToken();
    String access_token = token.getAccessToken();
    String yhbh = getPara("yhbh");
    String wxid = getPara("wxid");
    String jsonStr = null;
    try {
      jsonStr = webService.getUserList(access_token, yhbh, wxid);
    } catch (Exception e) {
      jsonStr = ReturnCode.bulidError(-1, e.getMessage());
    }
    renderJson(jsonStr);
  }

}

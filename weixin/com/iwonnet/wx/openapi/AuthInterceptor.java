package com.iwonnet.wx.openapi;

import java.lang.reflect.Field;

import com.iwonnet.wx.openapi.token.AuthToken;
import com.iwonnet.wx.openapi.token.CacheTokenKit;
import com.jfinal.aop.Interceptor;
import com.jfinal.core.ActionInvocation;
import com.jfinal.core.Controller;
import com.jfinal.weixin.sdk.api.ReturnCode;

/**
 * 登录验证拦截器
 * 
 * @author yanglh
 * 
 */
public class AuthInterceptor implements Interceptor {

	public void intercept(ActionInvocation ai) {
		Controller controller = ai.getController();
		// 签名检测
		if (checkSignature(controller)) {
			ai.invoke();
		} else {
			// 40014 不合法的access_token
			controller.renderJson(ReturnCode.bulidError(40014));
		}
	}

	private boolean checkSignature(Controller controller) {
		AuthToken token = CacheTokenKit.getCacheToken(controller.getPara("access_token"), "access_token");
		if (token != null) {
			Field[] fields = controller.getClass().getDeclaredFields();
			for (Field field : fields) {
				if ("token".equals(field.getName())) {
					field.setAccessible(true);
					try {
						field.set(controller, token);
						break;
					} catch (Exception e) {
						throw new RuntimeException(e);
					}
				}
			}
			return true;
		}
		return false;
	}
}

package com.iwonnet.wx.openapi;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import org.apache.log4j.Logger;

import com.iwonnet.wx.openapi.token.AuthToken;
import com.iwonnet.wx.service.IWebService;
import com.jfinal.plugin.spring.Inject;
import com.jfinal.weixin.sdk.api.ApiResult;
import com.jfinal.weixin.sdk.api.MessageSendApi;
import com.jfinal.weixin.sdk.api.ReturnCode;

public class MessageOpenAPI extends WeixinOpenAPI {

	private final static Logger LOGGER = Logger.getLogger(MessageOpenAPI.class);

	@Inject.BY_NAME
	private IWebService webService;

	/**
	 * 消息群发送接口
	 */
	public void messageCustomSend() {
		AuthToken token = getToken();
		String access_token = token.getAccessToken();
		String jsonParameter = "";
		try {
			jsonParameter = URLDecoder.decode(getPara("jsonParameter"), "UTF-8");
			System.out.println("群发参数：" + jsonParameter);
		} catch (UnsupportedEncodingException e) {
		}
		ApiResult apiResult = null;
		String sendType = getPara("sendType");
		if ("3".equals(sendType)) {
			apiResult = MessageSendApi.sendAll(access_token, jsonParameter);
		} else if ("2".equals(sendType)) {
			apiResult = MessageSendApi.send(access_token, jsonParameter);
		} else if ("1".equals(sendType)) {
			apiResult = MessageSendApi.sendPreview(access_token, jsonParameter);
		} else {
			apiResult = new ApiResult(ReturnCode.bulidError(-1, "sendType不合法！"));
		}
		renderJson(apiResult.getJson());
	}

	public void sendTemplateMessage() {
		AuthToken token = getToken();
		String access_token = token.getAccessToken();
		String jsonParameter = "";
		try {
			jsonParameter = URLDecoder.decode(getPara("jsonParameter"), "UTF-8");
			System.out.println("模板参数：" + jsonParameter);
		} catch (UnsupportedEncodingException e) {
		}
		ApiResult apiResult = MessageSendApi.sendTemplate(access_token, jsonParameter);
		renderJson(apiResult.getJson());
	}
}

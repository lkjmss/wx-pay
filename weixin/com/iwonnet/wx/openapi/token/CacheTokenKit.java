package com.iwonnet.wx.openapi.token;

import com.jfinal.log.Logger;
import com.jfinal.plugin.ehcache.CacheKit;

public class CacheTokenKit {

	private static final Logger log = Logger.getLogger(CacheTokenKit.class);

	public static void saveCacheToken(AuthToken accessToken, String cacheName) {
		if (accessToken != null) {
			// String appid = accessToken.getAppid().toUpperCase();
			String token = accessToken.getToken().toUpperCase();
			// CacheKit.put(cacheName, appid, accessToken);
			CacheKit.put(cacheName, token, accessToken);
		}
	}

	public static AuthToken getCacheToken(String token, String cacheName) {
		return CacheKit.get(cacheName, token == null ? null : token.toUpperCase(), AuthToken.class);
	}
}

package com.iwonnet.wx.openapi.token;

import java.io.Serializable;
import java.util.Map;

import com.jfinal.kit.HttpKit;
import com.jfinal.plugin.ehcache.CacheKit;
import com.jfinal.plugin.ehcache.IDataLoader;
import com.jfinal.weixin.sdk.api.AccessToken;
import com.jfinal.weixin.sdk.kit.ParaMap;

public class AuthToken implements IDataLoader, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2042848584974130858L;
	private Long wxid;
	private String appid;
	private String token;
	private String secret;
	private String timestamp;
	private String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential";

	public AuthToken() {
	}

	public AuthToken(Long wxid, String appid, String token, String secret, String timestamp) {
		this.wxid = wxid;
		this.appid = appid;
		this.token = token;
		this.secret = secret;
		this.timestamp = timestamp;
	}

	public Long getWxid() {
		return wxid;
	}

	public void setWxid(Long wxid) {
		this.wxid = wxid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getAppid() {
		return appid;
	}

	public String getToken() {
		return token;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public Object load() {
		return null;
	}

	public synchronized String getAccessToken() {
		AccessToken accessToken = CacheKit.get("wx_access_token", appid);
		if (accessToken != null && accessToken.isAvailable()) {
			return accessToken.getAccessToken();
		}
		for (int i = 0; i < 3; i++) {
			// String appId = ApiConfig.getAppId();
			// String appSecret = ApiConfig.getAppSecret();
			Map<String, String> queryParas = ParaMap.create("appid", appid).put("secret", secret).getData();
			//Map<String, String> queryParas = ParaMap.create("appid", "wxfb1f422963f18931").put("secret", "a2ec9a3a35366ea1038cf84177893410").getData();
			String json = HttpKit.get(url, queryParas);
			accessToken = new AccessToken(json);
			if (accessToken.isAvailable())
				break;
		}
		CacheKit.put("wx_access_token", appid, accessToken);
		return accessToken.getAccessToken();
	}

	public String getSecret() {
		return secret;
	}

	public void setSecret(String secret) {
		this.secret = secret;
	}
}

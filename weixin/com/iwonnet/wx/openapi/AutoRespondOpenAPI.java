package com.iwonnet.wx.openapi;

import net.sf.json.JSONObject;

import com.iwonnet.wx.model.WxZdhf;
import com.iwonnet.wx.openapi.token.AuthToken;
import com.jfinal.weixin.sdk.api.ReturnCode;

public class AutoRespondOpenAPI extends WeixinOpenAPI {

  public void autoRespond() {
    String ret = "";
    AuthToken token = super.getToken();
    Long wxid = token.getWxid();
    JSONObject json = JSONObject.fromObject(getPara("query"));
    String key = json.getString("key");
    String load_help = "select * from wx_zdhf where wxid = ? and hflx = '9'";
    WxZdhf help = WxZdhf.Dao.findFirst(load_help, wxid);
    if (help != null) {
      if (help.getStr("PPNR").equalsIgnoreCase(key)) {
        ret = bulid(help).toString();
        renderJson(ret);
        return;
      }
    }
    String search_sql = "select * from wx_zdhf where wxid = ? and hflx = '1' and ppnr = ?";
    WxZdhf hf = WxZdhf.Dao.findFirst(search_sql, new Object[] { wxid, key });
    if (hf != null) {
      renderJson(bulid(hf));
      return;
    } else if (help != null) {
      JSONObject _json = new JSONObject();
      _json.put("errcode", 0);
      _json.put("returnmsg", "\t文本消息已成功接收，内容为：" + key + "\n\n" + help.getStr("WBNR"));
      renderJson(_json);
      return;
    }
    ret = ReturnCode.bulidError(0);
    renderJson(ret);
  }

  private JSONObject bulid(WxZdhf h) {
    JSONObject json = new JSONObject();
    json.put("errcode", 0);
    json.put("returnmsg", h.getStr("WBNR"));
    return json;
  }
}

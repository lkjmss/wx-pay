package com.iwonnet.wx.openapi.cache;

import java.util.HashMap;
import java.util.Map;

import com.iwonnet.wx.openapi.token.AuthToken;
import com.iwonnet.wx.openapi.token.CacheTokenKit;
import com.jfinal.kit.HttpKit;
import com.jfinal.weixin.sdk.api.AccessToken;
import com.jfinal.weixin.sdk.kit.ParaMap;

public class CacheManager {

  private static Map<String, AccessToken> access_tokens;
  private static Map<String, AuthToken> auth_tokens;
  private static String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential";
  static {
    if (access_tokens == null) {
      access_tokens = new HashMap<String, AccessToken>();
    }
    if (auth_tokens == null) {
      auth_tokens = new HashMap<String, AuthToken>();
    }
  }

  public static synchronized String getAccessToken(String auth_token) {
    AuthToken auth_t = CacheTokenKit.getCacheToken(auth_token, "access_token");
    AccessToken access_t = access_tokens.get(auth_t.getAppid());
    if (access_t != null && access_t.isAvailable()) {
      return access_t.getAccessToken();
    } else {
      Map<String, String> queryParas = ParaMap.create("appid", auth_t.getAppid())
          .put("secret", auth_t.getSecret()).getData();
      String json = HttpKit.get(url, queryParas);
      access_t = new AccessToken(json);
      access_tokens.put(auth_t.getAppid(), access_t);
      return access_t.getAccessToken();
    }
  }
}

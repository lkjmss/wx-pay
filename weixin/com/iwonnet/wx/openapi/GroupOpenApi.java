package com.iwonnet.wx.openapi;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.iwonnet.wx.openapi.token.AuthToken;
import com.iwonnet.wx.service.IWebService;
import com.jfinal.kit.EncryptionKit;
import com.jfinal.kit.HttpKit;
import com.jfinal.plugin.spring.Inject;
import com.jfinal.weixin.sdk.api.ReturnCode;
import com.jfinal.weixin.sdk.kit.ParaMap;

public class GroupOpenApi extends WeixinOpenAPI {

  @Inject.BY_NAME
  private IWebService webService;

  // /api/group/create
  /**
   * 创建分组
   */
  public void create() {
    AuthToken token = getToken();
    String access_token = token.getAccessToken();
    String fzbh = getPara("fzbh");
    String jsonStr = webService.groupCreate(access_token, fzbh);
    renderJson(jsonStr);
  }

  /**
   * 同步分组
   */
  public void sync() {
    AuthToken token = getToken();
    String access_token = token.getAccessToken();
    Long wxid = token.getWxid();
    String result = webService.syncGroupList(wxid, access_token);
    renderJson(result);
  }

  /**
   * 修改分组名称
   */
  public void rename() {
    AuthToken token = getToken();
    String access_token = token.getAccessToken();
    String fzbh = getPara("fzbh");
    String fzmc, result;
    try {
      fzmc = new String(getPara("fzmc").getBytes("ISO-8859-1"), "UTF-8");
      result = webService.rename(access_token, fzbh, fzmc);
    } catch (UnsupportedEncodingException e) {
      result = ReturnCode.bulidError(-1, e.getMessage());
    }
    renderJson(result);
  }

  /**
   * 查询用户所在分组
   */
  public void getid() {
    AuthToken token = getToken();
    String access_token = token.getAccessToken();
    String openid = getPara("openid");
    String result = webService.getid(token.getWxid(), access_token, openid);
    renderJson(result);
  }

  /**
   * 移动用户分组
   * 
   * @date 2015-3-16
   * @author zhongjf
   */
  public void move() {
    AuthToken token = getToken();
    String access_token = token.getAccessToken();
    String openid = getPara("openid");
    String fzbh = getPara("fzbh");
    String result = webService.move(access_token, openid, fzbh);
    System.out.println(result);
    renderJson(result);
  }

  /**
   * 
   /** 批量移动用户分组
   * 
   * @param access_token
   * @param jsonStr
   *          {"openid_list":["oDF3iYx0ro3_7jD4HFRDfrjdCM58",
   *          "oDF3iY9FGSSRHom3B-0w5j4jlEyY"],"to_groupid":108}
   * @return
   * @date 2015-3-16
   * @author zhongjf
   */
  public void batchupdate() {
    AuthToken token = getToken();
    String access_token = token.getAccessToken();
    String openidList = getPara("openidList");
    JSONArray jsonList = JSONArray.fromObject(openidList);
    String fzbh = getPara("fzbh");
    String result = webService.batchupdate(access_token, jsonList, fzbh);
    renderJson(result);

  }

  public void delete() {
    AuthToken token = getToken();
    String access_token = token.getAccessToken();
    String fzbh = getPara("fzbh");
    String jsonStr = webService.deleteGroup(access_token, fzbh);
    renderJson(jsonStr);
  }

  public static void main(String[] args) {
    String host = "http://127.0.0.1:8088", method = "/api/group/create", url;

    Map<String, String> querys = ParaMap.create().put("grant_type", "client_credential")
        .put("appid", "wx429394bf7b590082")
        .put("secret", EncryptionKit.md5Encrypt("c2fb2ea03db9f0c33e4701ba0b7af8ce")).getData();
    String token = JSONObject.fromObject(HttpKit.get(host + "/api/token", querys)).getString(
        "access_token");
    token = "?access_token=" + token;
    method = "/api/group/sync";
    url = host + method + token;
    Map<String, String> queryParas = ParaMap.create().put("fzbh", "1000001382").getData();
    String result = HttpKit.get(url, queryParas);
    System.out.println(result);
  }
}

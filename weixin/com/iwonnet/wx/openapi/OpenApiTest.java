package com.iwonnet.wx.openapi;

import java.io.UnsupportedEncodingException;
import java.util.Map;

import net.sf.json.JSONObject;

import com.jfinal.kit.EncryptionKit;
import com.jfinal.kit.HttpKit;
import com.jfinal.weixin.sdk.kit.ParaMap;

public class OpenApiTest {

  public static void main(String[] args) throws UnsupportedEncodingException {
    String host = "http://127.0.0.1:8088", method, url;
    // -------------------------------------------------------------------------------------------------------
    Map<String, String> querys = ParaMap.create().put("grant_type", "client_credential").put(
        "appid", "wx429394bf7b590082").put("secret",
        EncryptionKit.md5Encrypt("c2fb2ea03db9f0c33e4701ba0b7af8ce")).getData();
    String token = JSONObject.fromObject(HttpKit.get(host + "/api/token", querys)).getString(
        "access_token");
    token = "?access_token=" + token;
    // method = "/api/group/sync";
    // method = "/api/user/get";
    method = "/api/media/uploadnews";
    url = host + method + token;
    Map<String, String> queryParas = ParaMap.create().put("yxbh", "1000001141").put("wxid", "1").getData();
    System.out.println("请求URL：" + url);
    System.out.println("返回信息：" + HttpKit.get(url, queryParas));
  }
}

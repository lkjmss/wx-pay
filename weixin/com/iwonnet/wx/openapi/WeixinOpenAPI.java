package com.iwonnet.wx.openapi;

import java.io.BufferedReader;
import java.io.IOException;

import javax.servlet.http.HttpServletRequest;



import net.sf.json.JSON;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.iwonnet.wx.openapi.token.AuthToken;
import com.iwonnet.wx.openapi.token.CacheTokenKit;
import com.iwonnet.wx.service.IWebService;
import com.jfinal.aop.Before;
import com.jfinal.aop.ClearInterceptor;
import com.jfinal.core.Controller;
import com.jfinal.plugin.spring.Inject;
import com.jfinal.weixin.sdk.api.ReturnCode;

/**
 * OpenAPI基础类
 * 
 * @author yanglh
 */
@Before(AuthInterceptor.class)
public class WeixinOpenAPI extends Controller {

  @Inject.BY_NAME
  private IWebService webService;
  private final static String POST = "POST";
  private final static String GET = "GET";

  protected IWebService getService() {
    return this.webService;
  }

  private JSON getJSON(HttpServletRequest request) {
    StringBuffer buffer = new StringBuffer();
    try {
      BufferedReader reader = request.getReader();
      String string;
      while ((string = reader.readLine()) != null) {
        buffer.append(string);
      }
      reader.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    try {
      return JSONObject.fromObject(buffer.toString());
    } catch (Exception e) {
      return JSONArray.fromObject(buffer.toString());
    }
  }

  protected AuthToken getToken() {
    return CacheTokenKit.getCacheToken(getPara("access_token"), "access_token");
  }

  /**
   * 获取与微信服务端连接的Token<br>
   * RestFul URL : /api/token
   */
  @ClearInterceptor
  public void token() {
    String methodName = getRequest().getMethod();
    String retVal;
    if (POST.equalsIgnoreCase(methodName)) {
      JSONObject requestJSON = (JSONObject) getJSON(getRequest());
      retVal = webService.getToken(requestJSON.getString("appid"), requestJSON.getString("secret"));
    } else if (GET.equalsIgnoreCase(methodName)) {
      retVal = webService.getToken(getPara("appid"), getPara("secret"));
    } else {
      retVal = ReturnCode.bulidError(40038);// 40038 不合法的请求格式
    }
    System.out.println(retVal);
    renderJson(retVal);
  }
  //
  // /**
  // * 获取关注者列表
  // */
  // @ActionKey("/api/user/get")
  // public void userget() {
  // String retVal = "{}";
  // AuthToken authToken = CacheTokenKit.getCacheToken(getPara("access_token"),
  // "access_token");
  // String next_openid = getPara("next_openid");
  // if (authToken == null) {
  // retVal = ReturnCode.bulidError(40014);// 40014 不合法的access_token
  // } else {
  // if (StrKit.isBlank(next_openid)) {
  // retVal =
  // UserApi.getFollows(CacheManager.getAccessToken(authToken.getToken())).getJsonStr();
  // } else {
  // retVal =
  // UserApi.getFollowers(CacheManager.getAccessToken(authToken.getToken()),
  // next_openid).getJsonStr();
  // }
  // }
  // renderJson(retVal);
  // }
  //
  // @ActionKey("/api/user/info")
  // public void userinfo() {
  // String retVal = "{}";
  // AuthToken authToken = CacheTokenKit.getCacheToken(getPara("access_token"),
  // "access_token");
  // String openId = getPara("openid");
  // if (authToken == null) {
  // retVal = ReturnCode.bulidError(40014);// 40014 不合法的access_token
  // } else {
  // if (StrKit.isBlank(openId)) {
  // retVal = ReturnCode.bulidError(40003);
  // } else {
  // retVal =
  // UserApi.getUserInfo(CacheManager.getAccessToken(authToken.getToken()),
  // openId)
  // .getJsonStr();
  // }
  // }
  // renderJson(retVal);
  // }
  //
  // @ActionKey("/api/user/info/updateremark")
  // public void remark() {
  // renderNull();
  // }
  //
  // public void menu() {
  // String retVal = "{}";
  // String methodName = getRequest().getMethod();
  // AuthToken authToken = CacheTokenKit.getCacheToken(getPara("access_token"),
  // "access_token");
  // if (authToken == null) {
  // retVal = ReturnCode.bulidError(40014);// 40014 不合法的access_token
  // } else {
  // if (POST.equalsIgnoreCase(methodName)) {
  // } else if (GET.equalsIgnoreCase(methodName)) {
  // retVal =
  // MenuApi.getMenu(CacheManager.getAccessToken(authToken.getToken())).getJsonStr();
  // } else {
  // retVal = ReturnCode.bulidError(40038);// 40038 不合法的请求格式
  // }
  // }
  // renderJson(retVal);
  // }
}
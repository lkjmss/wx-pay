package com.iwonnet.wx.beanfactory;

import com.iwonnet.framework.spring.SpringBeanUtil;
import com.iwonnet.wx.business.IWXBusiness;

public class BusinessBeanFactory {

  private static final BusinessBeanFactory INSTANCE = new BusinessBeanFactory();

  public static BusinessBeanFactory getInstance() {
    return INSTANCE;
  }

  public IWXBusiness getWXBusiness() {
    return SpringBeanUtil.getBean("wxBusiness", IWXBusiness.class);
  }
}

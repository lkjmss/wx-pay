package com.iwonnet.wx.beanfactory;

import com.iwonnet.framework.spring.SpringBeanUtil;
import com.iwonnet.wx.payapi.IPayConfig;
import com.iwonnet.wx.payapi.Pay;

public class PayBeanFactory {

	private static PayBeanFactory instance = new PayBeanFactory();

	private PayBeanFactory() {

	}

	public static PayBeanFactory getInstance() {
		return instance;
	}

	public Pay getPay(IPayConfig config) {
		Pay pay = SpringBeanUtil.getBean("pay", Pay.class);
		pay.setConfig(config);
		return pay;
	}
}

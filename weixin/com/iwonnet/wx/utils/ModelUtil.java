package com.iwonnet.wx.utils;

import java.util.List;

import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class ModelUtil {

  public static JSONArray conver(List<? extends Model<?>> list) {
    JSONArray retJSON = new JSONArray();
    for (Model<? extends Model> model : list) {
      retJSON.add(JSONObject.fromObject(model.toJson()));
    }
    return retJSON;
  }

  public static JSONObject conver(Page<? extends Model<?>> page) {
    JSONObject retJSON = JSONObject.fromObject(page);
    JSONArray arrJSON = conver(page.getList());
    retJSON.remove("list");
    retJSON.put("list", arrJSON);
    return retJSON;
  }
}

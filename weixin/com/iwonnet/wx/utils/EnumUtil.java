package com.iwonnet.wx.utils;

public class EnumUtil {

  // 是否标志：否0，是1。
  public static enum SFBZ {
    NO("0", "否"), YES("1", "是");

    private String value;
    private String name;

    SFBZ(String value, String name) {
      this.value = value;
      this.name = name;
    }

    public String getValue() {
      return this.value;
    }

    public String getName() {
      return this.name;
    }
  }

  /**
   * 消息类型，文本为text，图片为image，语音为voice，视频消息为video，音乐消息为music，图文消息为news
   * 
   * @author yang
   * 
   */
  public static enum MsgType {
    text("10001", "text"), image("10002", "image"), voice("10003", "voice"), video("10004", "video"), music(
        "10005", "music"), news("10006", "news");

    private String code;
    private String value;

    MsgType(String code, String value) {
      this.code = code;
      this.value = value;
    }

    public String getCode() {
      return code;
    }

    public String getValue() {
      return value;
    }
  }

  public static enum MpMsgType {
    text("10001", "text"), image("10002", "image"), voice("10003", "voice"), mpvideo("10004",
        "mpvideo"), mpnews("10006", "mpnews");

    private String code;
    private String value;

    MpMsgType(String code, String value) {
      this.code = code;
      this.value = value;
    }

    public String getCode() {
      return code;
    }

    public String getValue() {
      return value;
    }
  }

  public static enum UploadFileType {
    image("1", "image"), voice("2", "voice"), video("3", "video"), thumb("4", "thumb");

    private String code;
    private String value;

    UploadFileType(String code, String value) {
      this.code = code;
      this.value = value;
    }

    public String getCode() {
      return code;
    }

    public String getValue() {
      return value;
    }
  }

  public static enum XYLX {
    CLCIK("20002", "click"), VIEW("20001", "view");

    private String code;
    private String value;

    XYLX(String code, String value) {
      this.code = code;
      this.value = value;
    }

    public String getCode() {
      return code;
    }

    public String getValue() {
      return value;
    }
  }

  public static enum GZZT {
    WGZ("0", "未关注"), YGZ("1", "已关注"), QXGZ("9", "已取消关注");

    private String code;
    private String name;

    GZZT(String code, String name) {
      this.code = code;
      this.name = name;
    }

    public String getCode() {
      return code;
    }

    public String getName() {
      return name;
    }
  }
}

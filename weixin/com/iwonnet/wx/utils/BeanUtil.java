package com.iwonnet.wx.utils;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.io.StringReader;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.sql.Clob;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import org.apache.commons.beanutils.BeanUtils;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

@SuppressWarnings("unchecked")
public class BeanUtil {
  
  
  /***
   * 将List<HashMap>转换成 List<Object>对象
   * @param hmList 需要转换的 List<HashMap>
   * @param className 目标 Object 的classname
   * @return
   * @throws ClassNotFoundException
   * @throws Exception
   */
  public static <T> List<T> MapListToObjectList(List<Map> hmList, Class<T> beanClass) throws ClassNotFoundException,Exception{
    if(BeanUtil.isEmpty(hmList)){
      return new ArrayList<T>();
    }
    List<T> returnList = new ArrayList<T>();
    for(Map map :hmList){
      T obj = (T) BeanUtil.MapToObject(map,beanClass);
      returnList.add(obj);
    }
    return returnList;
  }

  /**
   * 把HashMap对象的内容转换为某个 bean 类的对象 HashMap格式 (name=property value==属性值)
   * 
   * @param map
   * @param className
   * @return
   * @throws java.lang.ClassNotFoundException
   * @throws java.lang.Exception
   */
  public static Object MapToObject(Map map, Class<?> beanClass) throws ClassNotFoundException,
      Exception {
    if (BeanUtil.isEmpty(map)) {
      return null;
    }
    Object obj = beanClass.newInstance();
    org.apache.commons.beanutils.BeanUtils.populate(obj, map);
    return obj;
  }

  /**
   * <str>字符串 转换成 整型，如果不能转换，则返回默认值<defInt>
   * 
   * @param str
   * @param defInt
   * @return
   */
  public static int StrToInt(String str, int defInt) {
    try {
      return Integer.parseInt(str);
    } catch (Exception e) {
      return defInt;
    }
  }

  /**
   * 对象转换成int值，转换失败则返回0
   * 
   * @param obj
   * @return
   */
  public static int toInt(Object obj) {
    return toInt(obj, 0);
  }

  /**
   * 对象转换成int值，转换失败则返回默认值
   * 
   * @param obj
   * @param defInt
   * @return
   */
  public static int toInt(Object obj, int defInt) {
    try {
      return Integer.parseInt(obj.toString());
    } catch (Exception e) {
      return defInt;
    }
  }

  /**
   * 对象转换成double，如果失败则返回0.0
   * 
   * @param obj
   * @return
   */
  public static double toDouble(Object obj) {
    return toDouble(obj, 0.00);
  }

  /**
   * 对象转换成double，如果失败则返回defDbl
   * 
   * @param obj
   * @param defDbl
   * @return
   */
  public static double toDouble(Object obj, double defDbl) {
    try {
      return Double.parseDouble(obj.toString());
    } catch (Exception e) {
      return defDbl;
    }
  }

  /**
   * 判断对象是否为null
   * 
   * @param obj
   * @return
   */
  public static boolean isNull(Object obj) {
    return obj == null;
  }

  /**
   * 判断对象是否不为null
   * 
   * @param obj
   * @return
   */
  public static boolean isNotNull(Object obj) {
    return obj != null;
  }

  /**
   * 判断对象内容是否为 空
   * 
   * @param obj
   * @return
   */
  public static boolean isEmpty(Object obj) {
    return !isNotEmpty(obj);
  }

  /**
   * 判断对象内容是否为 非空
   * 
   * @param obj
   * @return
   */
  public static boolean isNotEmpty(Object obj) {
    if (obj != null) {
      if (obj instanceof String)
        return ((String) obj).trim().length() > 0;
      if (obj instanceof StringBuffer)
        return ((StringBuffer) obj).toString().trim().length() > 0;
      if (obj instanceof List)
        return ((List) obj).size() > 0;
      if (obj instanceof Vector)
        return ((Vector) obj).size() > 0;
      if (obj instanceof HashMap)
        return ((HashMap) obj).size() > 0;
      if (obj instanceof Iterator)
        return ((Iterator) obj).hasNext();
      return true;
    }
    return false;
  }

  /**
   * 将日期字符串(yyyy-MM-dd)，转换为制定格式(yyyyMMdd)日期字符串 示例1: sDate = "2004-2-3"
   * 返回:20040203 示例2: sDate = "2004-12-3" 返回:20041203
   * 
   * @param sDate
   * @param sDateFormat
   *          日期字符串格式，默认："yyyy-MM-dd"
   * @param retDateFormat
   *          返回的日期字符串格式，默认："yyyyMMdd"
   * @return
   * @throws Exception
   */
  public static String formatDataString(String sDate) throws Exception {
    String ret = sDate;
    try {
      ret = formatDataString(sDate, "yyyy-MM-dd", "yyyyMMdd");
    } catch (Exception ex) {
      try {
        ret = formatDataString(sDate, "yyyyMMdd", "yyyyMMdd");
      } catch (Exception e) {
        throw new Exception("日期转换异常", e);
      }
    }
    return ret;
  }

  /**
   * 将日期字符串(yyyy-MM-dd hh：mi：ss)，转换为制定格式(yyyyMMddhhmiss)日期字符串 示例1: sDate =
   * "20040203001122" 返回:20040203001122 示例2: sDate = "2004-12-3 05:33:11"
   * 返回:20040203001122
   * 
   * @param sDate
   * @param sDateFormat
   *          日期字符串格式，默认："yyyy-MM-dd hh：mi：ss"
   * @param retDateFormat
   *          返回的日期字符串格式，默认："yyyyMMddhhmiss"
   * @return
   * @throws Exception
   */
  public static String formatTimeString(String sTime) throws Exception {
    String ret = sTime;
    try {
      ret = formatDataString(sTime, "yyyy-MM-dd HH:mm:ss", "yyyyMMddHHmmss");
    } catch (Exception ex) {
      try {
        ret = formatDataString(sTime, "yyyyMMddHHmmss", "yyyyMMddHHmmss");
      } catch (Exception e) {
        throw new Exception("时间转换异常", e);
      }
    }
    return ret;
  }

  /**
   * 将日期字符串(yyyyMMddhhmiss)，转换为制定格式(yyyy-MM-dd HH：mm：ss)日期字符串 示例1: sDate =
   * "20040203001122" 返回:2004-12-3 05:33:11 示例2: sDate = "20040203001122"
   * 返回:2004-12-3 05:33:11
   * 
   * @param sDate
   * @param sDateFormat
   *          日期字符串格式，默认："yyyyMMddhhmiss"
   * @param retDateFormat
   *          返回的日期字符串格式，默认："yyyy-MM-dd HH：mm：ss"
   * @return
   * @throws Exception
   */
  public static String formatTime2String(String sTime) throws Exception {
    String ret = sTime;
    try {
      ret = formatDataString(sTime, "yyyyMMddHHmmss", "yyyy-MM-dd HH:mm:ss");
    } catch (Exception ex) {
      try {
        ret = formatDataString(sTime, "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm:ss");
      } catch (Exception e) {
        throw new Exception("时间转换异常", e);
      }
    }
    return ret;
  }

  /**
   * 将日期字符串(yyyy-MM-dd)，转换为制定格式(yyyyMMdd)日期字符串 示例1: sDate = "2004-2-3"
   * 返回:20040203 示例2: sDate = "2004-12-3" 返回:20041203
   * 
   * @param sDate
   * @param sDateFormat
   *          日期字符串格式，默认："yyyy-MM-dd"
   * @param retDateFormat
   *          返回的日期字符串格式，默认："yyyyMMdd"
   * @return
   * @throws Exception
   */
  public static String formatDataString(String sDate, String sDateFormat, String retDateFormat)
      throws Exception {
    SimpleDateFormat dateFormate = new SimpleDateFormat(sDateFormat);
    Date d = dateFormate.parse(sDate);

    dateFormate.applyPattern(retDateFormat);
    return dateFormate.format(d);
  }

  /**
   * 返回当前时间的字符串， 示例：返回2007061819110
   * 
   * @return
   * @throws Exception
   */
  public static String todayTime() throws Exception {
    return today("yyyyMMddHHmmss");
  }

  /**
   * 返回当前时间的字符串， 示例：返回20070618
   * 
   * @return
   * @throws Exception
   */
  public static String today() throws Exception {
    return today("yyyyMMdd");
  }

  /**
   * 获取当前时间的日期字符串
   * 
   * @param retDateFormat
   *          返回的日期字符串格式，默认为“yyyyMMdd”
   * @return
   * @throws Exception
   */
  public static String today(String retDateFormat) throws Exception {
    SimpleDateFormat dateFormate = new SimpleDateFormat(retDateFormat);
    return dateFormate.format(Calendar.getInstance().getTime());
  }

  /**
   * 计算两个日期之间的天数差
   * 
   * @param time1
   *          减前面的时间，
   * @param time2
   *          减后面的时间，
   * @return 天数
   * @throws ParseException
   */
  public static long getQuot(String time1, String time2) throws ParseException {
    long quot = 0;
    SimpleDateFormat ft = new SimpleDateFormat("yyyyMMdd");

    Date date1 = ft.parse(time1);
    Date date2 = ft.parse(time2);
    quot = date1.getTime() - date2.getTime();
    quot = quot / 1000 / 60 / 60 / 24;
    return quot;
  }

  /**
   * 根据xml字符串，返回List<HashMap>集合
   * <record><field1>value11</field1><field2>value12</field2></record>
   * <record><field1>value21</field1><field2>value22</field2></record> 返回：
   * List<HashMap> HashMap (key=field1 value=value11) (key=field2 value=value12)
   * HashMap (key=field1 value=value21) (key=field2 value=value22)
   * 
   * @param sXML
   * @return
   * @throws Exception
   */
  public static List convertXmlString(String sXML) throws Exception {
    List lstRet = new ArrayList();

    sXML = "<autoroot>" + sXML + "</autoroot>";
    StringReader in = new StringReader(sXML);
    SAXReader builder = new SAXReader();
    Document doc = builder.read(in);
    Element root = doc.getRootElement();

    Iterator iteRecord = root.elementIterator("record");
    while (iteRecord.hasNext()) {
      // 循环所有记录

      Element eRecord = (Element) iteRecord.next();
      HashMap hmRecord = new HashMap();

      Iterator iteField = eRecord.elementIterator();
      while (iteField.hasNext()) {
        // 循环所以字段属性

        Element eField = (Element) iteField.next();
        hmRecord.put(eField.getName(), eField.getTextTrim());
      }

      // 增加到集合中
      lstRet.add(hmRecord);
    }
    return lstRet;
  }

  /**
   * 将字符串中的特殊字符（主要是回车符），转换为JS的字符串。
   * 
   * @param str
   * @return
   */
  public static String toJSstring(String str) {
    if (str == null || str.equals("")) {
      return "";
    }
    str = str.replaceAll("\"", "&quot;");
    str = str.replaceAll("'", "\\\\'");
    str = str.replaceAll("\r", "\\\\r");
    str = str.replaceAll("\n", "\\\\n");
    return str;
  }

  public static String toString(Object obj) {
    return toString(obj, "");
  }

  public static String toString(Object obj, String def) {
    try {
      return obj.toString();
    } catch (Exception ex) {
      return def;
    }
  }

  /**
   * 读取文本文件内容，以字符串形式返回内容，默认每行以“回车”符进行分割。
   * 
   * @param sFileName
   *          文件名（最好用绝对路径）
   * @return
   * @throws Exception
   */
  public static String readFileContent(String sFileName) throws Exception {
    return readFileContent(sFileName, "\r\n");
  }

  /**
   * 读取文本文件内容，以字符串形式返回内容
   * 
   * @param sFileName
   *          文件名（最好用绝对路径）
   * @param cr
   *          每行的换行符号
   * @return
   * @throws Exception
   */
  public static String readFileContent(String sFileName, String cr) throws Exception {
    StringBuffer sFileContent = new StringBuffer(); // 文件内容
    LineNumberReader lnreader = null;
    try {
      lnreader = new LineNumberReader(new FileReader(sFileName));
      while (true) {
        String sRowContent = lnreader.readLine();
        if (sRowContent == null)
          break;
        if (sRowContent.trim().length() == 0)
          continue;
        sFileContent.append(sRowContent);
        sFileContent.append(cr);
      }
    } finally {
      try {
        lnreader.close();
      } catch (Exception ex) {

      }
    }
    return sFileContent.toString();
  }

  /**
   * 以UTF-8编码格式，从输入流中读取信息。
   * 
   * @param is
   * @return
   * @throws Exception
   */
  public static String getContentFromStream(InputStream is) throws Exception {
    return getContentFromStream(is, null, null);
  }

  /**
   * 
   * 以UTF-8编码格式，从输入流中读取信息。
   * 
   * @param is
   * @param cr
   *          换行符
   * @return
   * @throws Exception
   */
  public static String getContentFromStream(InputStream is, String cr) throws Exception {
    return getContentFromStream(is, cr, null);
  }

  /**
   * 从输入流中读取信息。
   * 
   * @param is
   * @param cr
   *          换行符
   * @param charsetName
   *          编码格式，默认UTF-8
   * @return
   * @throws Exception
   */
  public static String getContentFromStream(InputStream is, String cr, String charsetName)
      throws Exception {
    StringBuffer sb = new StringBuffer();
    BufferedReader reader = null;
    try {
      // 默认编码格式--UTF-8
      if (BeanUtil.isEmpty(charsetName))
        charsetName = "UTF-8";
      // 默认换行符--回车键
      if (BeanUtil.isEmpty(cr))
        cr = "\r\n";
      reader = new BufferedReader(new InputStreamReader(is, charsetName));
      String line;
      while ((line = reader.readLine()) != null) {
        sb.append(line);
        sb.append(cr);
      }
    } finally {
      try {
        reader.close();
      } catch (Exception ex) {
      }
    }
    return sb.toString();
  }

  /**
   * 从jar中，获取资源内容，并且编码格式为UTF-8
   * 
   * @param resName
   *          资源名称
   * @return
   * @throws Exception
   */
  public static String getContentForResource(String resName) throws Exception {
    InputStream is = BeanUtil.class.getResourceAsStream(resName);
    return BeanUtil.getContentFromStream(is);
  }

  /**
   * 从jar等资源中获取内容，并且编码格式为UTF-8
   * 
   * @param resName
   *          资源名称
   * @param charsetName
   *          编码格式，默认UTF-8
   * @return
   * @throws Exception
   */
  public static String getContentForResource(String resName, String charsetName) throws Exception {
    InputStream is = BeanUtil.class.getResourceAsStream(resName);
    return BeanUtil.getContentFromStream(is, "\r\n", charsetName);
  }

  /**
   * 从文件中、或资源中获取内容，以UTF-8编码格式返回内容
   * 
   * @param name
   *          文件名称、或者资源名称
   * @return
   * @throws Exception
   */
  public static String getContent(String name) throws Exception {
    return getContent(name, null);
  }

  /**
   * 从文件中、或资源中获取内容
   * 
   * @param name
   * @param charsetName
   * @return
   * @throws Exception
   */
  public static String getContent(String name, String charsetName) throws Exception {
    String ret = null;
    try {
      ret = readFileContent(name);
    } catch (Exception ex) {
      try {
        ret = getContentForResource(name, charsetName);
      } catch (Exception ex1) {
        throw new Exception(name + "不存在！", ex1);
      }
    }
    return ret;
  }


  /**
   * 把页面上拿到的map封装成HashMap,但出现重复值的时候此方法不可行，
   * 因为页面上传过来的map中，value中放的是string类型的数组，作用是防止重复name被覆盖
   * 
   * @param requestMap
   * @return
   */
  public static Map requsetMapToHashMap(Map requestMap) {
    Map map = new HashMap();
    Set keys = requestMap.keySet();
    Iterator keysIt = keys.iterator();
    while (keysIt.hasNext()) {
      String key = (String) keysIt.next();
      map.put(key, ((String[]) requestMap.get(key))[0]);
    }
    return map;
  }




  /***
   * 如果String对象为null则返回“”
   */
  public static String getStringEmptyDefault(String o) {
    if (BeanUtil.isEmpty(o)) {
      return "";
    }
    return o;
  }

  /**
   * 如果Double类型的数据为空，则返回0
   */
  public static Double getDoubleEmptyDefault(Double d) {
    if (BeanUtil.isEmpty(d)) {
      return 0d;
    } else {
      return d;
    }
  }

  /**
   * 如果Long类型的数据为空，则返回0
   */
  public static Long getLongEmptyDefault(Long l) {
    if (BeanUtil.isEmpty(l)) {
      return 0l;
    } else {
      return l;
    }
  }

  // /**
  // * 如果Blob类型的数据为空，则返回0*/
  // public static Blob getBlobEmptyDefault(Blob b){
  // if(BeanUtil.isEmpty(b)){
  // String imgPath = System.getProperty( "user.dir " ) + File.separator +
  // " WebRoot/public/imagesNew/ " + File.separator + " mrtp.jpg " ;
  // FileInputStream fis;
  // Blob photo = null;
  // try {
  // fis = new FileInputStream(imgPath);
  // try {
  // photo = Hibernate.createBlob(fis);
  // } catch (IOException e) {
  // e.printStackTrace();
  // }
  // } catch (FileNotFoundException e) {
  // e.printStackTrace();
  // }
  // return photo;
  // }else{
  // return b;
  // }
  // }

  /**
   * 根据对象获取象名。省略前面的包名 shaojj
   */
  public static String getClassName(Object object) {
    String[] temp = object.getClass().getName().split("\\.");
    int i = temp.length;
    return temp[i - 1];
  }

  /**
   * 获取服务器的机器名
   * 
   * @return
   */
  public static String getHostName() {
    InetAddress a;
    String hostName = "";
    try {
      a = InetAddress.getLocalHost();
      hostName = a.getHostName();
    } catch (UnknownHostException e) {
      e.printStackTrace();
    }
    return hostName;
  }

  

  /**
   * 从原有的List中复制fromIndex 到 toIndex的值出来并返回
   * 
   * @param fromList
   *          原list
   * @param fromIndex
   *          开始索引，第一位从0计数
   * @param toIndex
   *          结束索引
   * @return
   */
  public static List copySubList(List fromList, int fromIndex, int toIndex) throws Exception {
    if (isEmpty(fromList)) {
      return null;
    }
    if (fromIndex < 0 || toIndex < 0 || (toIndex - fromIndex) < 0) {
      throw new Exception("提取subList的时候原始fromIndex和fromIndex错误:fromIndex=" + fromIndex
          + ",toIndex=" + toIndex);
    }
    if (fromList.size() < (toIndex + 1)) {
      throw new Exception("提取subList的时候原始fromList的长度小于需要提取的长度fromList.size="
          + fromList.size() + ",toIndex=" + toIndex);
    }
    List returnList = new ArrayList();
    for (int i = fromIndex; i <= toIndex; i++) {
      returnList.add(fromList.get(i));
    }
    return returnList;
  }

  /**
   * 将源Bean中不为空的属性赋值到目标Bean中
   * 
   * @param source
   *          源Bean
   * @param target
   *          目标Bean
   * @return
   * @throws IllegalAccessException
   * @throws IllegalArgumentException
   * @throws InvocationTargetException
   */
  public static void copyProperty(Object source, Object target) throws IllegalArgumentException,
      IllegalAccessException, InvocationTargetException {
    Field[] fields = source.getClass().getDeclaredFields();
    Field.setAccessible(fields, true);
    for (Field field : fields) {
      if (BeanUtil.isNotEmpty(field.get(source))) {
        BeanUtils.copyProperty(target, field.getName(), field.get(source));
      }
    }
  }

  /** 把clob类型的转为string */
  public static String clob2String(Clob clob) {
    StringBuilder strBuilder = new StringBuilder("");
    BufferedReader reader = null;
    if (null != clob) {
      try {
        reader = new BufferedReader(clob.getCharacterStream());
      } catch (SQLException e) {
        e.printStackTrace();
      }
      String st = "";
      try {
        while (null != (st = reader.readLine())) {
          strBuilder.append(st + "\r\n");
        }
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    return strBuilder.toString();
  }
}
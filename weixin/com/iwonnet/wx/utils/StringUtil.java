package com.iwonnet.wx.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.sql.Clob;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.TimeZone;

/**
 * 
 * 
 * @author baojl
 * @version 1.0
 */
/**
 * @author blackpoint
 * 
 */
@SuppressWarnings({ "unchecked", "deprecation", "unused" })
public class StringUtil {

  /**
   * 将具有年月日长度为8位的字符串返回以-分割的年月日
   * 
   * @param str
   *          20061203
   * @return 2006-12-03
   */
  public static String stringDateToDashDateString(String str) {
    if (str == null || str.equalsIgnoreCase(""))
      return "";
    str = padRight(str, 8, '0');
    String sYear = str.substring(0, 4);
    String sMonth = str.substring(4, 6);
    String sDay = str.substring(6, 8);
    String sNewDate = sYear + "-" + sMonth + "-" + sDay;
    return sNewDate;
  }

  /**
   * 将具有年月日时分秒长度为14位的字符串返回以":"分割的时分秒
   * 
   * @param 20061203115623
   * @return 11:56:23
   */
  public static String stringDateTimeToColonTimeString(String str) {
    if (str == null || str.equalsIgnoreCase(""))
      return "";
    str = padLeft(str, 14, '0');
    String sHour = str.substring(8, 10);
    String sMinute = str.substring(10, 12);
    String sSecond = str.substring(12, 14);
    String sNewDate = sHour + ":" + sMinute + ":" + sSecond;
    return sNewDate;
  }

  /**
   * 将具有年月日时分秒长度为14位的字符串返回"YYYYMMDD/HHNNSS/"
   * 
   * @param 20061203115623
   * @return 20061203/115623/
   */
  public static String stringDateTimeToDate(String str) {
    if (str == null || str.equalsIgnoreCase(""))
      return "";
    str = padLeft(str, 14, '0');
    String sfm = str.substring(8, 14);
    String sNewDate = "/" + sfm + "/";
    return sNewDate;
  }

  /**
   * 格式化时间字符串 把“2006-1-5” 格式为“20060105”
   * 
   * @param str
   *          “yyyy-mm-dd”
   * @return“yyyymmdd”
   * 
   */
  public static String stringDashDateDToDateString(String str) {
    if (str == null || str.equalsIgnoreCase(""))
      return "";
    String[] temp = str.split("-");
    if (temp.length != 3) {
      return "";
    }
    str = padLeft(str, 14, '0');
    String sYear = padLeft(temp[0], 2, '0');
    String sMonth = padLeft(temp[1], 2, '0');
    String sDay = padLeft(temp[2], 2, '0');
    String sNewDate = sYear + sMonth + sDay;

    return sNewDate;
  }

  /**
   * 将具有年月日时分秒长度为14位的字符串返回以"-"分割的年月日和以":"分割时分秒
   * 
   * 
   * @param 20061203115623
   * @return 2006-12-03 11:56:23
   */
  public static String stringDateTimeToDashDateColonTimeString(String str) {
    if (str == null || str.equalsIgnoreCase(""))
      return "";
    str = padRight(str, 14, '0');
    String sYear = str.substring(0, 4);
    String sMonth = str.substring(4, 6);
    String sDay = str.substring(6, 8);
    String sHour = str.substring(8, 10);
    String sMinute = str.substring(10, 12);
    String sSecond = str.substring(12, 14);
    String sNewDate = sYear + "-" + sMonth + "-" + sDay + " " + sHour + ":" + sMinute + ":"
        + sSecond;
    return sNewDate;
  }

  /**
   * 将具有"-"分割的年月日和以":"分割时分秒 返回以年月日时分秒长度为14位的字符串
   * 
   * 
   * 
   * @param 2006-12-03 11:56:23
   * @return 20061203115623
   */
  public static String dashDateColonTimeStringTOstringDateTime(String str) {
    SimpleDateFormat dateFormate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    try {
      Date date = dateFormate.parse(str);
      dateFormate = new SimpleDateFormat("yyyyMMddHHmmss");
      return dateFormate.format(date);
    } catch (Exception e) {
      return "";
    }
  }

  /**
   * 以"-"分割的年月日转换成年月日
   * 
   * @param str
   *          2007-12-11
   * @return 20071211
   * @throws ParseException
   */
  public static String dashDateStringToDateString(String str) throws Exception {
    try {

      if (BeanUtil.isNotEmpty(str)) {
        if (str.indexOf("-") != -1) {
          return dateTimeToDateString(dashStringDateToDate(str.trim()));
        } else {
          return str;
        }
      } else {
        return "";
      }
    } catch (ParseException e) {
      throw new Exception("日期格式转换错误，日期格式不正确,正确的格式是：2009-09-01");
    }
  }

  /**
   * 把日期字符串转成中文的年月
   * 
   * 
   * @param str
   *          200708
   * @return 2007年08月
   * 
   */
  public static String dateStringToChineseDateMonthString(String str) {
    if (str == null || str.equalsIgnoreCase(""))
      return "";
    str = padRight(str, 6, '0');
    String sYear = str.substring(0, 4);
    String sMonth = str.substring(4, 6);
    String sNewDate = sYear + "年" + sMonth + "月";
    return sNewDate;
  }

  /**
   * 把年月日时的字符串转换成时日
   * 
   * @param str
   *          20061201
   * @return 01
   */
  public static String stringDateTimeToDayString(String str) {
    if (str == null || str.equalsIgnoreCase(""))
      return "";
    if (str == null || str.equalsIgnoreCase(""))
      return "";
    str = padRight(str, 8, '0');

    return str.substring(6, 8);
  }

  /**
   * 把年月日时的字符串转换成时月
   * 
   * @param str
   *          20061201
   * @return 12
   */
  public static String stringDateTimeToMonthString(String str) {
    if (str == null || str.equalsIgnoreCase(""))
      return "";
    if (str == null || str.equalsIgnoreCase(""))
      return "";
    str = padRight(str, 6, '0');

    return str.substring(4, 6);
  }

  /**
   * 把年月日时分秒的字符串转换成时分秒
   * 
   * 
   * @param str
   *          20061211071232
   * @return 071232
   */
  public static String stringDateTimeToTimeString(String str) {
    if (str == null || str.equalsIgnoreCase(""))
      return "";
    str = padRight(str, 14, '0');

    String sHour = str.substring(8, 10);
    String sMinute = str.substring(10, 12);
    String sSecond = str.substring(12, 14);
    String sNewDate = sHour + sMinute + sSecond;
    return sNewDate;

  }

  /**
   * 
   * @param time
   *          以":"分割的时分秒 12:3:5
   * @return 时分秒 120305
   */
  public static String colonTimeStringToTimeString(String time) {
    if (time == null || time.equalsIgnoreCase(""))
      return "";
    StringBuffer sb = new StringBuffer(8);
    String[] temp = time.split(":");
    for (int i = 0; i < temp.length; i++) {
      sb.append(padLeft(temp[i], 2, '0'));
    }
    return sb.toString();

  }

  /**
   * 把时分秒转换成带":"的时分秒
   * 
   * @param time
   *          122304
   * @return 12:23:04
   */
  public static String timeStringToColonTimeString(String time) {
    if (time == null || time.equalsIgnoreCase(""))
      return "";
    time = padRight(time, 6, '0');

    return time.substring(0, 2) + ":" + time.substring(2, 4) + ":" + time.substring(4, 6);
  }

  /**
   * 注意日期要自然日减一
   * 
   * @param calendar
   *          Calendar calendar = new GregorianCalendar(2007,5,8,1,2,3);
   * @return 2007年6月8日 星期五
   * 
   */
  public static String dateTimeToChineseDateTimeWeekString(Calendar calendar) {
    DateFormat df = DateFormat.getDateInstance(DateFormat.FULL, Locale.CHINESE);
    Date d = calendar.getTime();

    return df.format(d);
  }

  /**
   * 
   * @param calendar
   *          时间
   * @return 年月日时分秒 20070608010203
   */
  public static String dateTimeToDateTimeString(Calendar calendar) {
    return dateTimeToDateTimeString(calendar.getTime());

  }

  /**
   * 
   * @param date
   *          时间
   * @return 年月日时分秒 20070608010203
   */
  public static String dateTimeToDateTimeString(Date date) {
    SimpleDateFormat dateFormate = new SimpleDateFormat("yyyyMMddHHmmss");
    return dateFormate.format(date);

  }

  /**
   * 
   * @param date
   *          时间
   * @return 年月日 20070601
   */
  public static String dataTimetoDateString(Date date) {
    SimpleDateFormat dateFormate = new SimpleDateFormat("yyyyMMdd");
    return dateFormate.format(date);
  }

  /**
   * 
   * @param date
   *          时间
   * @param formart
   *          格式化字符串 如：'yyyyMMdd','yyyy'
   * @return 格式化后的时间格式
   * 
   */
  public static String dataTimetoFormatString(Date date, String formart) {
    SimpleDateFormat dateFormate = new SimpleDateFormat(formart);
    return dateFormate.format(date);
  }

  /**
   * 
   * @param calendar
   *          时间
   * @return 年月日 200706
   */
  public static String dateTimeToDateMonthString(Calendar calendar) {
    return dateTimeToDateMonthString(calendar.getTime());
  }

  /**
   * 
   * @param date
   *          时间
   * @return 年月日 200706
   */
  public static String dateTimeToDateMonthString(Date date) {
    SimpleDateFormat dateFormate = new SimpleDateFormat("yyyyMM");
    return dateFormate.format(date);
  }

  /**
   * 
   * @param calendar
   *          时间
   * @return 年月日 20070611
   */
  public static String dateTimeToDateString(Calendar calendar) {
    return dateTimeToDateString(calendar.getTime());
  }

  /**
   * 
   * @param date
   *          时间
   * @return 年月日 20070611
   */
  public static String dateTimeToDateString(Date date) {
    SimpleDateFormat dateFormate = new SimpleDateFormat("yyyyMMdd");
    return dateFormate.format(date);
  }

  /**
   * 把以“-”分割的字符串转化为时间类型
   * 
   * @param date
   *          2000-12-12
   * @return date
   * @throws ParseException
   */
  public static Date dashStringDateToDate(String date) throws ParseException {
    SimpleDateFormat dateFormate = new SimpleDateFormat("yyyy-MM-dd");
    // String string = dateFormate.format(date);
    dateFormate.setLenient(false);
    return dateFormate.parse(date);
  }

  /**
   * 计算日期间的差值
   * 
   * @param sBgnDateTime
   * @param sEndDateTime
   * @param sType
   *          传入日期类型 yyyy-MM-dd 或者 yyyyMMdd
   * @return
   * @throws ParseException
   */
  public static Long DateSubtration(String sBgnDateTime, String sEndDateTime, String sType)
      throws ParseException {
    Date oBgnDate = null;
    Date oEndDate = null;
    // DateFormat df=DataFormat.getDataInstance("yyyy-MM-dd");
    SimpleDateFormat formatter = new SimpleDateFormat(sType);

    Long nDuration = new Long(0);

    if (sBgnDateTime != null && !sBgnDateTime.equals("") && sEndDateTime != null
        && !sEndDateTime.equals("")) {
      oBgnDate = formatter.parse(sBgnDateTime);
      oEndDate = formatter.parse(sEndDateTime);
      nDuration = new Long((oEndDate.getTime() - oBgnDate.getTime()) / 1000);
    }
    return nDuration;

  }

  /**
   * 把字符串转化为时间类型
   * 
   * @param date
   *          20001212
   * @return date
   * @throws ParseException
   */
  public static Date StringDateToDate(String date) throws ParseException {
    SimpleDateFormat dateFormate = new SimpleDateFormat("yyyyMMdd");
    dateFormate.setLenient(false);
    return dateFormate.parse(date);
  }

  /**
   * 
   * @param s
   *          填充的字符串
   * @param len
   *          字符串要返回的长度
   * 
   * @param c
   *          字符串在长度不够时，左边填充的字符
   * 
   * @return 长度为length的字符串
   */
  public static String padLeft(String s, int len, char c) {

    if (s == null) {
      s = "";
    }
    s = s.trim();
    int sLength = s.getBytes().length;
    if (sLength >= len) {
      return s;
    }

    StringBuffer d = new StringBuffer(len);
    int fill = len - sLength;

    while (fill-- > 0) {
      d.append(c);
    }

    d.append(s);
    return d.toString();
  }

  /**
   * 
   * @param s
   *          填充的字符串
   * @param len
   *          字符串要返回的长度
   * 
   * @param c
   *          字符串在长度不够时，右边填充的字符
   * 
   * @return 长度为length的字符串
   */
  public static String padRight(String s, int len, char c) {

    if (s == null) {
      s = "";
    }
    s = s.trim();
    int sLength = s.getBytes().length;
    if (sLength >= len) {
      return s;
    }

    StringBuffer d = new StringBuffer(len);
    int fill = len - sLength;

    while (fill-- > 0) {
      d.append(c);
    }

    return s.concat(d.toString());
  }

  /**
   * 字符串进行重复count次
   * 
   * @param s
   *          待重复字符串
   * @param count
   *          重复次数
   * @return
   */
  public static String getRepeatString(String s, int count) {
    StringBuffer sb = new StringBuffer();
    while (count-- > 0)
      sb.append(s);
    return sb.toString();
  }

  /**
   * Object类型转化为String
   * 
   * @param obj
   *          Object参数类型
   * @return 返回String类型
   */
  public static String objectToString(Object obj) {
    return objectToString(obj, "");
  }

  /**
   * Object类型转化为String
   * 
   * @param obj
   *          Object参数类型
   * @param defValue
   *          默认值
   * 
   * @return 返回String类型
   */
  public static String objectToString(Object obj, String defValue) {

    try {
      return obj.toString();
    } catch (Exception ex) {
      return defValue;
    }
  }

  /**
   * 将Object类型转化为Boolean
   * 
   * @param obj
   * @return
   */
  public static boolean objectToBoolean(Object obj) {
    try {
      String ret = obj.toString();
      if (ret.toUpperCase().equals("TRUE") || EnumUtil.SFBZ.YES.getValue().equals(ret)) {
        return true;
      } else {
        return false;
      }
    } catch (Exception e) {
      return false;
    }
  }

  /**
   * Object类型转化为int
   * 
   * @param obj
   *          Object参数类型
   * @return 返回int类型
   */
  public static int objectToInt(Object obj) {
    return objectToInt(obj, -1);
  }

  /**
   * Object类型转化为int
   * 
   * @param obj
   *          Object参数类型
   * @param defValue
   *          默认值
   * 
   * @return 返回int类型
   */
  public static int objectToInt(Object obj, int defValue) {
    try {
      return Integer.parseInt(obj.toString());
    } catch (Exception ex) {
      return defValue;
    }
  }

  /**
   * Object类型转化为Long
   * 
   * @param obj
   *          Object参数类型
   * @return 返回Long类型
   */
  public static Long objectToLong(Object obj) {
    return objectToLong(obj, new Long(-1));
  }

  /**
   * Object类型转化为Long
   * 
   * @param obj
   *          Object参数类型
   * @param defValue
   *          默认值
   * 
   * @return 返回Long类型
   */
  public static Long objectToLong(Object obj, Long defValue) {
    try {
      if (obj == null) {
        return defValue;
      }
      return new Long(obj.toString());
    } catch (Exception ex) {
      return defValue;
    }
  }

  /**
   * 将格式化的金额字符串，转换成double型。
   * 
   * @param obj
   * @return
   */
  public static Double objectToCurrency(Object obj, Double defValue) {
    try {
      return new Double(obj.toString().replaceAll(",", ""));
    } catch (Exception ex) {
      return defValue;
    }
  }

  /**
   * 将格式化的金额字符串，转换成double型。 如不能转换，则返回null
   * 
   * @param obj
   * @return
   */
  public static Double objectToCurrency(Object obj) {
    return objectToCurrency(obj, null);
  }

  /**
   * 将格式化的金额字符串，转换成double型。
   * 
   * @param str
   *          金额字符串。如123,345.23
   * @param defValue
   * @return
   */
  public static Double stringToCurrency(String str, Double defValue) {
    try {
      return new Double(str.replaceAll(",", ""));
    } catch (Exception ex) {
      return defValue;
    }
  }

  /**
   * 将格式化的金额字符串，转换成double型。 如不能转换，则返回null
   * 
   * @param str
   *          金额字符串。如123,345.23
   * @return
   */
  public static Double stringToCurrency(String str) {
    return stringToCurrency(str, null);
  }

  public static Double objectToDouble(Object value) {
    return objectToDouble(value, null);
  }

  public static Double objectToDouble(Object value, Double defValue) {
    if (value == null) {
      return defValue;
    }
    try {
      return new Double(value.toString());
    } catch (Exception e) {
      return defValue;
    }
  }

  public static double objectTodouble(Object value) {
    return objectToDouble(value, 0);
  }

  public static double objectToDouble(Object value, double defValue) {
    if (value == null) {
      return defValue;
    }
    try {
      return new Double(value.toString()).doubleValue();
    } catch (Exception e) {
      return defValue;
    }
  }

  public static Object doubleToObject(Double value) {
    return doubleToObject(value, null);
  }

  public static Object doubleToObject(Double value, Object defValue) {
    if (value == null) {
      return defValue;
    }
    try {
      return value.toString();
    } catch (Exception e) {
      return defValue;
    }
  }

  /**
   * 返回当前的日期，以“-”分割 2006-12-12
   * 
   * @return
   */
  public static String getNowDashDateString() {
    SimpleDateFormat dateFormate = new SimpleDateFormat("yyyy-MM-dd");
    return dateFormate.format(getNowDate());
  }

  /**
   * 获取东八区的时间
   * 
   * @author baojl
   * @date Apr 10, 2012
   * @return
   */
  private static Date getNowDate() {
    // TimeZone zone = TimeZone.getTimeZone("ETC/GMT-8");
    // TimeZone.setDefault(zone);
    // Date date = Calendar.getInstance(zone).getTime();
    Date date = Calendar.getInstance().getTime();
    return date;
  }

  /**
   * 
   * @return 返回当前年份
   */
  public static String getCurrentYear() {
    SimpleDateFormat dateFormate = new SimpleDateFormat("yyyy");
    return dateFormate.format(getNowDate());
  }

  /**
   * 
   * @return 返回当前年月份
   * 
   */
  public static String getCurrentYearMonth() {
    SimpleDateFormat dateFormate = new SimpleDateFormat("yyyyMM");
    return dateFormate.format(getNowDate());
  }

  /**
   * 
   * @return 返回当前年月日 20061112
   * 
   */
  public static String getCurrentDate() {
    SimpleDateFormat dateFormate = new SimpleDateFormat("yyyyMMdd");
    return dateFormate.format(getNowDate());
  }

  /**
   * 返回当前的日期，"20061206011123"
   * 
   * @return "20061206011123"
   */
  public static String getCurrentTimeString() {

    SimpleDateFormat dateFormate = new SimpleDateFormat("yyyyMMddHHmmss");
    return dateFormate.format(getNowDate());
  }

  public static String doubleToString(Double value) {
    return doubleToString(value, "");
  }

  public static String doubleToString(Double value, String defValue) {
    if (value == null) {
      return defValue;
    }
    try {
      return value.toString();
    } catch (Exception e) {
      return defValue;
    }
  }

  public static String longToString(Long value) {
    return longToString(value, "");
  }

  public static String longToString(Long value, String defValue) {
    if (value == null) {
      return defValue;
    }
    try {
      return value.toString();
    } catch (Exception e) {
      return defValue;
    }
  }

  public static Double stringToDouble(String value) {
    return stringToDouble(value, null);
  }

  public static Double stringToDouble(String value, Double defValue) {
    if (value == null || value.equals("")) {
      return defValue;
    }
    try {
      return new Double(value);
    } catch (Exception e) {
      return defValue;
    }

  }

  public static Long stringToLong(String value) {
    return stringToLong(value, null);
  }

  public static Long stringToLong(String value, Long defValue) {
    if (value == null || value.equals("")) {
      return defValue;
    }
    try {
      return new Long(value);
    } catch (Exception e) {
      return defValue;
    }
  }

  public static int stringToInt(String value, int defValue) {
    if (value == null || value.equals("")) {
      return defValue;
    }
    try {
      return new Integer(value);
    } catch (Exception e) {
      return defValue;
    }
  }

  /**
   * 
   * 将字符串中的特殊字符（主要是回车符），转换为JS的字符串。
   * 
   * 
   * @param str
   * @return
   */
  public static String stringToJSString(String str) {
    str = str.replaceAll("\"", "&quot;");

    str = str.replaceAll("\r", "\\\\r");
    str = str.replaceAll("\n", "\\\\n");
    return str;
  }

  /**
   * 计算时间增加
   * 
   * @author pjhcandy
   * @param formatStr
   *          格式化时间的字符串
   * @param dateStr
   *          根据formateStr对应的时间
   * @param num
   *          要增加的数量
   * @param CalendarType
   *          被增加的日期类型
   * @return
   * @throws Exception
   */
  public static String dateAdded(String formatStr, String dateStr, int num, String CalendarType)
      throws Exception {
    SimpleDateFormat dateFormate = new SimpleDateFormat(formatStr);
    Date de = dateFormate.parse(dateStr);
    Calendar cal = new GregorianCalendar();
    cal.setTime(de);

    if (CalendarType.equals("day")) {
      cal.add(Calendar.DAY_OF_MONTH, num);
    }
    if (CalendarType.equals("month")) {
      cal.add(Calendar.MONTH, num);
    }
    if (CalendarType.equals("year")) {
      cal.add(Calendar.YEAR, num);
    }
    Date dat = (Date) cal.getTime();
    int in = (dat.getMonth() + 1);
    String month = (in < 10) ? ("0" + in) : (in + "");
    String day = (dat.getDate() < 10) ? ("0" + dat.getDate()) : (dat.getDate() + "");
    return (dat.getYear() + 1900) + "" + month + "" + day;
  }

  /**
   * 取得当前日期是多少周
   * 
   * @param date
   * @return
   */
  public static int getWeekOfYear(Date date) {
    Calendar c = new GregorianCalendar();
    c.setFirstDayOfWeek(Calendar.MONDAY);
    c.setMinimalDaysInFirstWeek(7);
    c.setTime(date);

    return c.get(Calendar.WEEK_OF_YEAR);
  }

  /**
   * 得到某一年周的总数
   * 
   * @param year
   * @return
   */
  public static int getMaxWeekNumOfYear(int year) {
    Calendar c = new GregorianCalendar();
    c.set(year, Calendar.DECEMBER, 31, 23, 59, 59);

    return getWeekOfYear(c.getTime());
  }

  /**
   * 得到某年某周的第一天
   * 
   * @param year
   * @param week
   * @return
   */
  public static Date getFirstDayOfWeek(int year, int week) {
    Calendar c = new GregorianCalendar();
    c.set(Calendar.YEAR, year);
    c.set(Calendar.MONTH, Calendar.JANUARY);
    c.set(Calendar.DATE, 1);

    Calendar cal = (GregorianCalendar) c.clone();
    cal.add(Calendar.DATE, week * 7);

    return getFirstDayOfWeek(cal.getTime());
  }

  /**
   * 得到某年某周的最后一天
   * 
   * @param year
   * @param week
   * @return
   */
  public static Date getLastDayOfWeek(int year, int week) {
    Calendar c = new GregorianCalendar();
    c.set(Calendar.YEAR, year);
    c.set(Calendar.MONTH, Calendar.JANUARY);
    c.set(Calendar.DATE, 1);

    Calendar cal = (GregorianCalendar) c.clone();
    cal.add(Calendar.DATE, week * 7);

    return getLastDayOfWeek(cal.getTime());
  }

  /**
   * 取得当前日期所在周的第一天
   * 
   * @param date
   * @return
   */
  public static Date getFirstDayOfWeek(Date date) {
    Calendar c = new GregorianCalendar();
    c.setFirstDayOfWeek(Calendar.MONDAY);
    c.setTime(date);
    c.set(Calendar.DAY_OF_WEEK, c.getFirstDayOfWeek()); // Monday
    return c.getTime();
  }

  /**
   * 取得当前日期所在周的最后一天
   * 
   * @param date
   * @return
   */
  public static Date getLastDayOfWeek(Date date) {
    Calendar c = new GregorianCalendar();

    c.setFirstDayOfWeek(Calendar.MONDAY);
    c.setTime(date);
    c.set(Calendar.DAY_OF_WEEK, c.getFirstDayOfWeek() + 6); // Sunday
    return c.getTime();
  }

  /**
   * 返回毫秒
   * 
   * @param date
   *          日期
   * @return 返回毫秒
   */
  public static long getMillis(java.util.Date date) {
    java.util.Calendar c = java.util.Calendar.getInstance();
    c.setTime(date);
    return c.getTimeInMillis();
  }

  /**
   * 
   * @param date
   * @return
   */
  public static String getDateMillisString(java.util.Date date) {
    SimpleDateFormat dateFormate = new SimpleDateFormat("yyyyMMddHHmmssSSS");
    return dateFormate.format(date);
  }

  /**
   * 日期相加
   * 
   * @param date
   *          日期
   * @param day
   *          天数
   * @return 返回相加后的日期
   */
  public static java.util.Date addDate(java.util.Date date, int day) {
    java.util.Calendar c = java.util.Calendar.getInstance();
    c.setTimeInMillis(getMillis(date) + ((long) day) * 24 * 3600 * 1000);
    return c.getTime();
  }

  /**
   * 日期减天数
   * 
   * @param date
   *          日期
   * @param day
   *          天数
   * @return 返回相减后的日期
   */
  public static java.util.Date subDate(java.util.Date date, int day) {
    java.util.Calendar c = java.util.Calendar.getInstance();
    c.setTimeInMillis(getMillis(date) - ((long) day) * 24 * 3600 * 1000);
    return c.getTime();
  }

  /**
   * 时间相减，
   * 
   * @param lowTime
   *          小时间
   * @param uppTime
   *          大时间
   * @return 相隔秒数
   * @throws ParseException
   */
  public static long diffTimeSecond(String lowTime, String uppTime) throws Exception {
    SimpleDateFormat dfs = new SimpleDateFormat("yyyyMMddHHmmss");
    java.util.Date begin = null;
    java.util.Date end = null;
    try {
      begin = dfs.parse(lowTime);
      end = dfs.parse(uppTime);
    } catch (Exception e) {
      throw new Exception("进行时间格式转换异常:" + e.getMessage());
    }
    long between = (end.getTime() - begin.getTime()) / 1000;// 除以1000是为了转换成秒
    return between;
  }


  /**
   * 日期相加
   * 
   * @param date
   *          日期
   * @param month
   *          月数
   * @return 返回相加后的日期
   */
  public static java.util.Date addDateMonth(java.util.Date date, int month) {
    java.util.Calendar c = java.util.Calendar.getInstance();
    c.setTime(date);
    c.add(Calendar.MONTH, month);
    return c.getTime();
  }

  /**
   * 日期相减
   * 
   * @param date
   *          日期
   * @param date1
   *          日期
   * @return 返回相减后的日期
   */
  public static int diffDate(java.util.Date date, java.util.Date date1) {
    return (int) ((getMillis(date) - getMillis(date1)) / (24 * 3600 * 1000));
  }

  /**
   * 日期相减，返回相差的天数
   * 
   * @param date1
   * @param date2
   * @return
   * @throws Exception
   */
  public static int diffDate(String date1, String date2) throws Exception {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
    Date d1 = dateFormat.parse(date1);
    Date d2 = dateFormat.parse(date2);
    return diffDate(d1, d2);
  }

  /**
   * 取得指定月份的第一天
   * 
   * @param strdate
   * @return
   */
  public static String getMonthBegin(int year, int month) {
    try {
      Calendar calendar = Calendar.getInstance();
      calendar.set(Calendar.YEAR, year);
      calendar.set(Calendar.MONTH, month - 1);
      calendar.set(Calendar.DATE, 1);

      SimpleDateFormat dateFormate = new SimpleDateFormat("yyyy-MM-dd");
      return dateFormate.format(calendar.getTime());
    } catch (Exception e) {
      return "";
    }

  }

  public static String getMonthBegin(Date date) {
    try {
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(date);
      calendar.set(Calendar.DATE, 1);

      SimpleDateFormat dateFormate = new SimpleDateFormat("yyyy-MM-dd");
      return dateFormate.format(calendar.getTime());
    } catch (Exception e) {
      return "";
    }

  }

  /**
   * 取得指定月份的最后一天
   * 
   * @param strdate
   * @return
   */
  public static String getMonthEnd(int year, int month) {
    try {
      Calendar calendar = Calendar.getInstance();
      calendar.set(Calendar.YEAR, year);
      calendar.set(Calendar.MONTH, month);
      calendar.set(Calendar.DATE, 1);

      calendar.add(Calendar.DATE, -1);
      SimpleDateFormat dateFormate = new SimpleDateFormat("yyyy-MM-dd");
      return dateFormate.format(calendar.getTime());
    } catch (Exception e) {
      return "";
    }
  }

  public static String getMonthEnd(Date date) {
    try {
      Calendar calendar = Calendar.getInstance();
      calendar.setTime(date);
      calendar.set(Calendar.DATE, 1);
      calendar.set(Calendar.MONTH, date.getMonth() + 1);

      calendar.add(Calendar.DATE, -1);
      SimpleDateFormat dateFormate = new SimpleDateFormat("yyyy-MM-dd");
      return dateFormate.format(calendar.getTime());
    } catch (Exception e) {
      return "";
    }
  }

  /**
   * 
   * @param sDate
   * @param dateType
   *          year or 6 年 halfyear or 5 半年 quarter or 4 季度 month or 3 月 week or
   *          2 周 day or 1 日
   * @return
   */
  public static String getEndDateByType(String sDate, String dateType) throws Exception {
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
    Date date = dateFormat.parse(sDate);

    if (dateType.equalsIgnoreCase("day") || dateType.equalsIgnoreCase("1"))
      return sDate.substring(0, 8);

    if (dateType.equalsIgnoreCase("week") || dateType.equalsIgnoreCase("2")) {
      date = getLastDayOfWeek(date);
      return dateFormat.format(date);
    }

    if (dateType.equalsIgnoreCase("month") || dateType.equalsIgnoreCase("3")) {
      return getMonthEnd(date);
    }

    if (dateType.equalsIgnoreCase("quarter") || dateType.equalsIgnoreCase("4")) {
      String month = getMonthBegin(date);
      String year = month.substring(0, 4);
      int m = Integer.parseInt(month.substring(4, 6));

      if (m <= 3)
        return year + "0331";
      if (m >= 4 && m <= 6)
        return year + "0630";
      if (m >= 7 && m <= 9)
        return year + "0930";
      return year + "1231";
    }

    if (dateType.equalsIgnoreCase("halfyear") || dateType.equalsIgnoreCase("5")) {
      String month = getMonthBegin(date);
      String year = month.substring(0, 4);
      int m = Integer.parseInt(month.substring(4, 6));

      if (m <= 6)
        return year + "0630";
      return year + "1231";
    }
    if (dateType.equalsIgnoreCase("year") || dateType.equalsIgnoreCase("6")) {
      String month = getMonthBegin(date);
      return month.substring(0, 4) + "1231";
    }
    return "";
  }

  /**
   * 格式化字符串,如："1,2,3"格式化为"'1','2','3'"
   * 
   * @param str
   * @return
   */
  public static String formatCommaString(String str) {

    if (str == null || str.trim().equals(""))
      return "";

    String[] aTmpStr = str.split(",");
    String sFormatStr = "";
    for (int i = 0; i < (aTmpStr.length - 1); i++) {
      sFormatStr += "'" + aTmpStr[i] + "',";
    }
    sFormatStr += "'" + aTmpStr[aTmpStr.length - 1] + "'";
    return sFormatStr;
  }

  /**
   * 计算上一个工作日
   * 
   * @param sNowDate
   *          (yyyyMMdd)
   * @return yyyyMMdd
   * @throws Exception
   */
  public static String getLastWorkDayString(String sNowDate) throws Exception {
    String retDate = "";
    Date dNowDate = StringUtil.StringDateToDate(sNowDate);
    String sweekString = StringUtil.getWeekDayString(dNowDate);
    if (sweekString == "星期一")
      retDate = StringUtil.dateTimeToDateString(StringUtil.addDate(dNowDate, -3));
    else if (sweekString == "星期天")
      retDate = StringUtil.dateTimeToDateString(StringUtil.addDate(dNowDate, -2));
    else
      retDate = StringUtil.dateTimeToDateString(StringUtil.addDate(dNowDate, -1));

    return retDate;
  }

  /**
   * 返回当前星期数
   * 
   * @param sNowDate
   * @return
   * @throws Exception
   */
  public static String getWeekDayString(Date dNowDate) throws Exception {
    String weekString = "";
    final String dayNames[] = { "星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六" };
    Calendar calendar = Calendar.getInstance();
    Date date = dNowDate;
    calendar.setTime(date);
    int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
    weekString = dayNames[dayOfWeek - 1];
    return weekString;
  }

  /**
   * 把8位的日期类型转换成以点号形式的日期类型
   * 
   * @param dateString
   *          ex:20091010
   * @return 2009.10.10
   */
  public static String convertDateToDian(String dateString) {
    if (dateString.length() != 8) {
      return dateString;
    } else {
      return dateString.substring(0, 4) + "." + dateString.substring(4, 6) + "."
          + dateString.substring(6, 8);
    }
  }

  /**
   * 传入2008年1月 返回200801
   * 
   * @param yearmonth
   * @return
   * @throws Exception
   */
  public static String getDashStringYearMonth(String yearmonth) throws Exception {
    if (yearmonth.length() > 8 || yearmonth.length() < 7) {
      throw (new Exception("传入时间字符串格式不正确"));
    } else if (yearmonth.length() > 7) {
      if (Integer.valueOf(yearmonth.substring(5, 7)).intValue() > 12) {
        throw (new Exception("传入时间字符串月份不正确"));
      }
      return yearmonth.substring(0, 4) + yearmonth.substring(5, 7);
    } else {
      return yearmonth.substring(0, 4) + "0" + yearmonth.substring(5, 6);
    }
  }

  /**
   * 解决乱码问题，转成utf-8编码
   * 
   * @param oldString
   * @return
   * @throws UnKnowException
   */
  public static String toUTFString(String oldString) throws Exception {
    if (null == oldString)
      return null;
    if ("".equals(oldString))
      return "";
    String myparam = "";
    try {
      myparam = new String(oldString.getBytes("ISO8859-1"), "utf-8");
    } catch (Exception e) {
      throw new Exception("转换编码异常", e);
    }
    return myparam;
  }

  /**
   * 人民币大写金额转换
   * 
   * @param number
   * @param pattern
   * @return
   * @throws GeneralException
   */
  public static String format(double number, String pattern) {
    DecimalFormat df = new DecimalFormat(pattern);
    return df.format(number);
  }

  public static String format(String number, String pattern) {
    double d = new Double(number).doubleValue();
    return format(d, pattern);
  }



  public static String getDecode(String linename) {
    if (BeanUtil.isEmpty(linename)) {
      return "";
    }
    try {
      return java.net.URLDecoder.decode(linename, "UTF-8");
    } catch (Exception e) {
      return linename;
    }

  }

  /**
   * 得到几天前的时间
   * 
   * @param d
   * @param day
   * @return
   */
  public static String getDateBefore(Date d, int day) {
    Calendar now = Calendar.getInstance();
    now.setTime(d);
    now.set(Calendar.DATE, now.get(Calendar.DATE) - day);
    SimpleDateFormat dateFormate = new SimpleDateFormat("yyyyMMdd");
    return dateFormate.format(now.getTime());
  }

  /**
   * 得到几天后的时间
   * 
   * @param d
   * @param day
   * @return
   */
  public static String getDateAfter(Date d, int day) {
    Calendar now = Calendar.getInstance();
    now.setTime(d);
    now.set(Calendar.DATE, now.get(Calendar.DATE) + day);
    SimpleDateFormat dateFormate = new SimpleDateFormat("yyyyMMdd");
    return dateFormate.format(now);
  }

  /**
   * 将字CLOB转成STRING类型
   * 
   * @author Doris Aug 19, 2011
   * @param clob
   * @return
   * @throws UnKnowException
   * @throws SQLException
   * @throws IOException
   */
  public static String ClobToString(Clob clob) throws Exception {
    StringBuffer reString = new StringBuffer();
    if (clob != null) {
      Reader is = null;
      BufferedReader br = null;
      try {

        is = clob.getCharacterStream();// 从clob得到流
        br = new BufferedReader(is);
        String readoneline;
        while ((readoneline = br.readLine()) != null) {
          reString.append(readoneline);
        }
      } catch (Exception e) {
        throw  e;
      } finally {
        try {
          if (BeanUtil.isNotEmpty(br)) {
            br.close();
          }
          if (BeanUtil.isNotEmpty(is)) {
            is.close();
          }
        } catch (Exception e1) {
          throw  e1;
        }
      }
    } else {
      reString = null;
    }
    return reString == null ? "" : reString.toString();
  }

  /**
   * 把日期字符串转成中文的年月日
   * 
   * 
   * @param str
   *          20070808
   * @return 2007年08月08日
   * 
   */
  public static String dateStringToChineseDateMonthYearString(String str) {
    if (str == null || str.equalsIgnoreCase(""))
      return "";
    str = padRight(str, 8, '0');
    String sYear = str.substring(0, 4);
    String sMonth = str.substring(4, 6);
    String sDay = str.substring(6, 8);
    String sNewDate = sYear + "年" + sMonth + "月" + sDay + "日";
    return sNewDate;
  }

  public static String FormatNumber(int str, int length) {
    StringBuffer sb = new StringBuffer();
    StringBuffer sb1 = new StringBuffer();
    sb.append(str);

    if (sb.toString().getBytes().length < length) {
      for (int i = 0; i < length - sb.toString().getBytes().length; i++) {
        sb1.append("0");
      }
      sb1.append(str);
      return sb1.toString();
    } else {
      return sb.toString();
    }
  }

  public static String FormatStringLeftSpace(String targetStr, int strLength) {
    if (targetStr == null) {
      targetStr = "";
    }
    int strle = targetStr.getBytes().length;
    int i = strLength - strle;
    StringBuffer sb = new StringBuffer();
    if (i > 0) {
      for (int k = 0; k < i; k++) {
        sb.append("0");
      }
    } else {
      return new String(targetStr.getBytes(), 0, strLength);
    }
    sb.append(targetStr);
    return sb.toString();
  }

  /**
   * 将1,2,3,转换成甲，乙，丙 药品分类
   * 
   * @author baojl
   * @date Apr 10, 2012
   * @param jyflFlag
   * @return
   */
  public static String FormatJyflToChiness(String jyflFlag) {
    if (BeanUtil.isEmpty(jyflFlag)) {
      return "";
    }
    if ("1".equals(jyflFlag)) {
      return "甲";
    }
    if ("2".equals(jyflFlag)) {
      return "乙";
    }
    if ("3".equals(jyflFlag)) {
      return "丙";
    }
    if ("甲".equals(jyflFlag) || "乙".equals(jyflFlag) || "丙".equals(jyflFlag)) {
      return jyflFlag;
    }

    return "甲";
  }

  /**
   * 将男女的代码转换成中文男女
   * 
   * @param code
   * @return
   */
  public static String getChinessXbByCode(String code) {
    if (BeanUtil.isEmpty(code)) {
      return "";
    }
    if ("1".equals(code)) {
      return "男";
    } else if ("2".equals(code)) {
      return "女";
    } else {
      return code;
    }
  }

  /**
   * 将病种标记转换成中文
   * 
   * @param code
   * @return
   */
  public static String getChinessBZBJByCode(String code) {
    if (BeanUtil.isEmpty(code)) {
      return "";
    }
    if ("1".equals(code)) {
      return "特殊病";
    } else if ("2".equals(code)) {
      return "慢性病";
    } else if ("0".equals(code)) {
      return "普通病";
    } else {
      return code;
    }
  }

  public static String getChineseYueByCode(String code) throws Exception {

    String[] chineseYue = new String[] { "一月", "二月", "三月", "四月", "五月", "六月", "七月", "八月", "九月",
        "十月", "十一月", "十二月" };
    Integer codeInt = StringUtil.objectToInt(code);
    String returnYue = "";
    try {
      returnYue = chineseYue[codeInt - 1];
    } catch (Exception e) {
      throw new Exception("月份转换中文失败，具体原因是：传入的参数必须是01-12之间的数字，但传入的是：" + code);
    }
    return returnYue;
  }

  /**
   * 将具有年月日时分秒长度为14位的字符串返回以汉字分割的年月日时分秒
   * 
   * @author zhangjiahu
   * @date 2012-09-21
   * 
   * @param 20061203115623
   * @return 2006年12月03日 11时56分23秒
   */
  public static String stringDateTimeToChinaTimeString(String str) {
    if (str == null || str.equalsIgnoreCase(""))
      return "";
    str = padRight(str, 14, '0');
    String sYear = str.substring(0, 4);
    String sMonth = str.substring(4, 6);
    String sDay = str.substring(6, 8);
    String sHour = str.substring(8, 10);
    String sMinute = str.substring(10, 12);
    String sSecond = str.substring(12, 14);
    String sNewDate = sYear + "年" + sMonth + "月" + sDay + "日" + sHour + ":" + sMinute + ":"
        + sSecond;
    return sNewDate;
  }

  public static boolean checkStringDate(String stringdate, String format) throws Exception {
    if (BeanUtil.isEmpty(format)) {
      throw new Exception("未指定格式");
    }
    if (BeanUtil.isEmpty(stringdate)) {
      throw new Exception("未指定检查的日期信息");
    }
    if (format.toUpperCase().equals("YYYYMMDD")) {
      try {
        StringUtil.diffDate(StringUtil.getCurrentDate(), stringdate);
      } catch (Exception e) {
        throw e;
      }
    } else {
      try {
        String isTrue = StringUtil.dashDateColonTimeStringTOstringDateTime(stringdate);
        if (BeanUtil.isEmpty(isTrue)) {
          return false;
        }
      } catch (Exception e) {
        return false;
      }
    }
    return true;
  }

  /**
   * getHoursAgoTime <b>获取几小时前的时间</b>
   * 
   * @author yanglh
   * @date Mar 4, 2014 2:28:04 PM
   * @param hoursAgo
   * @return
   */
  public static String getHoursAgoTime(int hoursAgo) {
    if (BeanUtil.isEmpty(hoursAgo)) {
      hoursAgo = 1;
    }
    String hoursAgoTime = "";
    Date d = new Date();
    SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
    sdf.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
    d.setHours(d.getHours() - hoursAgo);
    hoursAgoTime = sdf.format(d);
    return hoursAgoTime;
  }

  /**
   * 在当前的数字上加一
   * 
   * @param length
   *          长度
   * @param currentNumber
   *          当前的数字
   * @throws UnKnowException
   */
  public static String getNumberByLength(int length, String currentNumber) throws Exception {
    int current = Integer.parseInt(currentNumber) + 1;
    int sizeString = (current + "").length();
    if (sizeString > length) {
      throw new Exception("排序号超出当前的最大长度");
    }
    String zero = "";
    for (int i = 0; i < length - sizeString; i++) {
      zero += "0";
    }
    return zero + current;
  }

  /**
   * 判断形成年份是否符合规范
   * 
   * @author xz 2014-8-15
   * 
   * @param string
   * @return
   * 
   * @return boolean
   */
  public static boolean isNumber(String xcnf) {
    if (xcnf.matches("[0-9]+")) {
      if (Integer.parseInt(xcnf) >= 1949
          && Integer.parseInt(xcnf) <= Integer.parseInt(StringUtil.getCurrentYear())) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  /**
   * 根据日期格式化获取20位文件命名
   * 
   * @return
   */
  public static String getDateFormatFileName() {
    return new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date())
        + Math.round(Math.random() * 899 + 100);
  }
}

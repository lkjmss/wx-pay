package com.iwonnet.wx.utils;

import com.iwonnet.wx.utils.EnumUtil.UploadFileType;

public class ContentTypeUtil {

  public static UploadFileType conver(String content_type) {
    if ("image/jpeg".equalsIgnoreCase(content_type))
      return UploadFileType.image;
    else if ("audio/amr".equalsIgnoreCase(content_type)
        || "audio/mpeg".equalsIgnoreCase(content_type))
      return UploadFileType.voice;
    else if ("video/mp4".equalsIgnoreCase(content_type))
      return UploadFileType.video;
    else
      return null;
  }
}

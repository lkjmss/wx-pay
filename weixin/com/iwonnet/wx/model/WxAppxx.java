package com.iwonnet.wx.model;

import com.jfinal.plugin.tablebind.Table;

@Table(tableName = "wx_appxx", pkName = "id")
public class WxAppxx extends BaseModel<WxAppxx> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1855890077737659597L;

	public static WxAppxx Dao = new WxAppxx();
}

package com.iwonnet.wx.model;

import com.jfinal.plugin.tablebind.Table;

@Table(tableName = "bc_dlmx", pkName = "mxbh")
public class BcDlmx extends BaseModel<BcDlmx> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2309328190099036346L;

	public static final BcDlmx dao = new BcDlmx();
}

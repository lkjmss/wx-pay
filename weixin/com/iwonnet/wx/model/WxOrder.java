package com.iwonnet.wx.model;

import com.jfinal.plugin.tablebind.Table;

@Table(tableName = "wx_order", pkName = "id")
public class WxOrder extends BaseModel<WxOrder> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3851047902693133565L;

	public static final WxOrder dao = new WxOrder();
}

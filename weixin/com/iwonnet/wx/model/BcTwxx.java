package com.iwonnet.wx.model;

import com.jfinal.plugin.tablebind.Table;

@Table(tableName = "bc_twxx", pkName = "xxbh")
public class BcTwxx extends BaseModel<BcTwxx> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3533978679547359158L;

	public static final BcTwxx dao = new BcTwxx();
}

package com.iwonnet.wx.model;

import com.jfinal.plugin.tablebind.Table;

@Table(tableName = "bc_jk", pkName = "id")
public class BcJk extends BaseModel<BcJk> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2003899050093906876L;

	public static final BcJk dao = new BcJk();
}

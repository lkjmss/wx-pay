package com.iwonnet.wx.model;

import com.jfinal.plugin.tablebind.Table;

@Table(tableName = "wx_mb_fsjl", pkName = "id")
public class WxMbFsjl extends BaseModel<WxMbFsjl> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2708514197900639287L;

	public static final WxMbFsjl Dao = new WxMbFsjl();
}

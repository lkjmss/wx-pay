package com.iwonnet.wx.model;

import com.jfinal.plugin.tablebind.Table;

@Table(tableName = "wx_dmt", pkName = "id")
public class WxDmt extends BaseModel<WxDmt> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1574553361717808326L;

	public static WxDmt Dao = new WxDmt();
}

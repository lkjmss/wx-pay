package com.iwonnet.wx.model;

import com.jfinal.plugin.tablebind.Table;

@Table(tableName = "wx_zdhf", pkName = "hfbh")
public class WxZdhf extends BaseModel<WxZdhf> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1504215270140005838L;

	public static final WxZdhf Dao = new WxZdhf();
}

package com.iwonnet.wx.model;

import com.iwonnet.wx.utils.StringUtil;
import com.jfinal.plugin.activerecord.DbKit;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.TableMapping;

public class BaseModel<M extends Model<M>> extends Model<M> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4468171806005300625L;

	@Override
	public Long getLong(String attr) {
		return StringUtil.objectToLong(get(attr));
	}

	public M findFirstByCol(String column, Object param) {
		return findFirst(new StringBuilder(DbKit.getConfig(getClass()).getDialect().getQueryStringAll(TableMapping.me().getTable(getClass()))).append(" where `").append(column).append("`=?").toString(), param);
	}
}

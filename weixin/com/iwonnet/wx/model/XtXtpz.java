package com.iwonnet.wx.model;

import com.jfinal.plugin.tablebind.Table;

@Table(tableName = "xt_xtpz", pkName = "id")
public class XtXtpz extends BaseModel<XtXtpz> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5827129380517506834L;
	
	public static final XtXtpz dao = new XtXtpz();
}

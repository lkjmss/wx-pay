package com.iwonnet.wx.model;

import com.jfinal.plugin.tablebind.Table;

@Table(tableName = "wx_refund", pkName = "id")
public class WxRefund extends BaseModel<WxRefund> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5214009095252997091L;

	public static final WxRefund dao = new WxRefund();
}

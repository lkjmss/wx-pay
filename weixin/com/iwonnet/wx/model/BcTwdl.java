package com.iwonnet.wx.model;

import com.jfinal.plugin.tablebind.Table;

@Table(tableName = "bc_twdl", pkName = "dlbh")
public class BcTwdl extends BaseModel<BcTwdl> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3059053617875868670L;

	public static final BcTwdl dao = new BcTwdl();
}

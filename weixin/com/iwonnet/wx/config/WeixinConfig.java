package com.iwonnet.wx.config;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import com.alibaba.druid.filter.stat.StatFilter;
import com.iwonnet.wx.controller.IndexController;
import com.iwonnet.wx.controller.WXPayController;
import com.iwonnet.wx.controller.WeixinController;
import com.iwonnet.wx.openapi.AutoRespondOpenAPI;
import com.iwonnet.wx.openapi.GroupOpenApi;
import com.iwonnet.wx.openapi.MediaOpenAPI;
import com.iwonnet.wx.openapi.MenuOpenAPI;
import com.iwonnet.wx.openapi.MessageOpenAPI;
import com.iwonnet.wx.openapi.UserOpenApi;
import com.iwonnet.wx.openapi.WeixinOpenAPI;
import com.jfinal.config.Constants;
import com.jfinal.config.Handlers;
import com.jfinal.config.Interceptors;
import com.jfinal.config.JFinalConfig;
import com.jfinal.config.Plugins;
import com.jfinal.config.Routes;
import com.jfinal.core.JFinal;
import com.jfinal.ext.plugin.qiniu.QiNiuPlugin;
import com.jfinal.kit.PathKit;
import com.jfinal.plugin.activerecord.CaseInsensitiveContainerFactory;
import com.jfinal.plugin.activerecord.dialect.OracleDialect;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.plugin.druid.DruidStatViewHandler;
import com.jfinal.plugin.ehcache.EhCachePlugin;
import com.jfinal.plugin.spring.IocInterceptor;
import com.jfinal.plugin.spring.SpringPlugin;
import com.jfinal.plugin.tablebind.AutoTableBindPlugin;
import com.jfinal.render.ViewType;
import com.jfinal.weixin.sdk.api.ApiConfig;

public class WeixinConfig extends JFinalConfig {

  private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

  public Properties loadProp(String pro, String dev) {
    try {
      return loadPropertyFile(pro);
    } catch (Exception e) {
      return loadPropertyFile(dev);
    }
  }

  public void configConstant(Constants me) {
    me.setViewType(ViewType.JSP);
    // 如果生产环境配置文件存在，则优先加载该配置，否则加载开发环境配置文件
    loadProp("a_little_config_pro.properties", "a_little_config.properties");
    me.setDevMode(getPropertyToBoolean("devMode", false));

    // 配置微信 API 相关常量
    ApiConfig.setDevMode(me.getDevMode());
    ApiConfig.setUrl(getProperty("url"));
    ApiConfig.setToken(getProperty("token"));
    ApiConfig.setAppId(getProperty("appId"));
    ApiConfig.setAppSecret(getProperty("appSecret"));

    /**
     * 是否对消息进行加密，对应于微信平台的消息加解密方式： 1：true进行加密且必须配置 encodingAesKey
     * 2：false采用明文模式，同时也支持混合模式
     */
    ApiConfig.setEncryptMessage(getPropertyToBoolean("encryptMessage", false));
    ApiConfig.setEncodingAesKey(getProperty("encodingAesKey", "setting it in config file"));
  }

  public void configRoute(Routes me) {
    me.add("/", IndexController.class);
    me.add("/pay", WXPayController.class);
    me.add("/weixin", WeixinController.class);
    //me.add("/weixin/*", WeixinController.class);
    me.add("/api", WeixinOpenAPI.class);
    me.add("/api/message", MessageOpenAPI.class);
    me.add("/api/media", MediaOpenAPI.class);
    me.add("/api/menu", MenuOpenAPI.class);
    me.add("/api/respond", AutoRespondOpenAPI.class);
    me.add("/api/group", GroupOpenApi.class);
    me.add("/api/user", UserOpenApi.class);
  }

  public void configPlugin(Plugins me) {
    DruidPlugin druidPlugin = new DruidPlugin(getProperty("jdbc.url"),
        getProperty("jdbc.username"), getProperty("jdbc.password"));
    // druid监控用
    druidPlugin.addFilter(new StatFilter());
    me.add(druidPlugin);
    AutoTableBindPlugin atbp = new AutoTableBindPlugin(druidPlugin,
        AutoTableBindPlugin.TableNameStyle.LOWER_UNDERLINE);
    me.add(atbp);
    atbp.setShowSql(true);
//    atbp.setDialect(new OracleDialect());
    atbp.setContainerFactory(new CaseInsensitiveContainerFactory());
    me.add(new EhCachePlugin());
    // 添加spring支持
    me.add(new SpringPlugin("/"+PathKit.getWebRootPath() + "/WEB-INF/applicationContext.xml"));
    // 添加七牛存储支持
    me.add(new QiNiuPlugin());
  }

  public void configInterceptor(Interceptors me) {
    me.add(new IocInterceptor());
  }

  public void configHandler(Handlers me) {
    DruidStatViewHandler dvh = new DruidStatViewHandler("/druid");
    me.add(dvh);
  }

  public static void main(String[] args) {
    JFinal.start("WebRoot", 8080, "/", 5);
  }

  public void afterJFinalStart() {
    StringBuffer sb = new StringBuffer();
    sb.append("Finish deploy project ------- ").append(sdf.format(new Date())).append(
        " ------------------------------\n");
    sb.append("Project     : ").append("浙江网能微信公众服务管理平台").append("\n");
    sb.append("Corporate   : ").append("浙江网能信息技术有限公司").append("\n");
    sb.append("Deploy Path : ").append(PathKit.getWebRootPath()).append("\n");
    sb.append("--------------------------------------------------------------------------------\n");
    System.out.print(sb.toString());
  }
}

package com.iwonnet.wx.plugin.tx;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import com.jfinal.core.JFinal;
import com.jfinal.log.Logger;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.IAtom;

@Aspect
@Component
public class TxBusinessInterceptor {

  private final Logger logger = Logger.getLogger(TxBusinessInterceptor.class);
  private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
  private static final boolean devMode;
  private static List<String> transactionAttributes;

  public void setTransactionAttributes(List<String> transactionAttributes) {
    TxBusinessInterceptor.transactionAttributes = transactionAttributes;
  }

  static {
    devMode = JFinal.me().getConstants().getDevMode();
    if (devMode) {

    }
  }

  @Around("execution(public * com.iwonnet.wx.business.impl.*.*(..))")
  public Object invoke(ProceedingJoinPoint point) throws Throwable {
    Object target = point.getTarget();
    String methodName = point.getSignature().getName();
    StringBuffer sb = new StringBuffer();
    if (transactionAttributes != null) {
      for (Iterator<String> it = transactionAttributes.iterator(); it.hasNext();) {
        String regex = it.next();
        Pattern pattern = Pattern.compile(regex);
        if (pattern.matcher(methodName).matches()) {
          // 如果存在事务的话，需要使用到jfinal提供的事务支持。
          if (devMode) {
            sb.append("TxBusinessInterceptor ------- ").append(sdf.format(new Date()))
                .append(" ------------------------------\n");
            sb.append("Class       : ").append(target.getClass().getName()).append(".(")
                .append(target.getClass().getSimpleName()).append(".java:1)").append("\n");
            sb.append("Method      : ").append(methodName).append("\n");
            sb.append("Regex       : ").append(regex).append("\n");
            sb.append("--------------------------------------------------------------------------------\n");
            System.out.print(sb.toString());
          }
          TxInvoke invoke = new TxInvoke(point);
          Db.tx(invoke);
          return invoke.getResult();
        }
      }
    }
    // 如果没有事务的话，直接调用方法
    return point.proceed();
  }

  private class TxInvoke implements IAtom {

    private ProceedingJoinPoint invocation;
    private Object result = null;

    public TxInvoke(ProceedingJoinPoint invocation) {
      this.invocation = invocation;
    }

    @Override
    public boolean run() throws SQLException {
      try {
        result = invocation.proceed();
        return true; // 说明调用成功
      } catch (Throwable e) {
        logger.error("调用方法出现异常", e);
        throw new SQLException(e);
      }
    }

    public Object getResult() {
      return result;
    }
  }

  public static void main(String[] args) {
    System.out.println(Pattern.matches("^save.*", "saveOrUpdate"));
  }
}

package com.iwonnet.wx.plugin.tx;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import com.jfinal.core.JFinal;
import com.jfinal.weixin.sdk.msg.in.InMsg;
import com.jfinal.weixin.sdk.msg.in.event.InFollowEvent;
import com.jfinal.weixin.sdk.msg.in.event.InLocationEvent;
import com.jfinal.weixin.sdk.msg.in.event.InMenuEvent;
import com.jfinal.weixin.sdk.msg.in.event.InQrCodeEvent;
import com.jfinal.weixin.sdk.msg.in.event.InTemplateEvnet;

@Aspect
@Component
public class TxServiceInterceptor {

  private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
  private static final boolean devMode;

  static {
    devMode = JFinal.me().getConstants().getDevMode();
  }

  @Around("execution(public * com.iwonnet.wx.service.impl.*.processIn*(..))")
  public Object invoke(ProceedingJoinPoint point) throws Throwable {
    InMsg msg = (InMsg) point.getArgs()[1];
    if (msg instanceof InLocationEvent)
      return point.proceed();
    Object target = point.getTarget();
    String methodName = point.getSignature().getName();
    StringBuffer sb = new StringBuffer();
    String appid = (String) point.getArgs()[0];
    if (devMode) {
      sb.append("TxServiceInterceptor -------- ").append(sdf.format(new Date()))
          .append(" ------------------------------\n");
      sb.append("Interceptor : ").append(this.getClass().getName()).append(".(")
          .append(this.getClass().getSimpleName()).append(".java:1)").append("\n");
      sb.append("Class       : ").append(target.getClass().getName()).append(".(")
          .append(target.getClass().getSimpleName()).append(".java:1)").append("\n");
      sb.append("Method      : ").append(methodName).append("\n");
      sb.append("AppID       : ").append(appid).append("\n");
      sb.append("MsgType     : ").append(msg.getMsgType()).append("\n");
      if (msg instanceof InFollowEvent) {
        InFollowEvent _msg = (InFollowEvent) msg;
        sb.append("Event       : ").append(_msg.getEvent()).append("\n");
      } else if (msg instanceof InQrCodeEvent) {
        InQrCodeEvent _msg = (InQrCodeEvent) msg;
        sb.append("Event       : ").append(_msg.getEvent()).append("\n");
        sb.append("EventKey    : ").append(_msg.getEventKey()).append("\n");
      } else if (msg instanceof InMenuEvent) {
        InMenuEvent _msg = (InMenuEvent) msg;
        sb.append("Event       : ").append(_msg.getEvent()).append("\n");
        sb.append("EventKey    : ").append(_msg.getEventKey()).append("\n");
      } else if (msg instanceof InTemplateEvnet) {
        InTemplateEvnet _msg = (InTemplateEvnet) msg;
        sb.append("Event       : ").append(_msg.getEvent()).append("\n");
        sb.append("EventKey    : ").append(_msg.getEventKey()).append("\n");
      }
      sb.append("--------------------------------------------------------------------------------\n");
      System.out.print(sb.toString());
    }
    return point.proceed();
  }
}

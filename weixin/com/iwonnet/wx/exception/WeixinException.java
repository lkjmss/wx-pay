package com.iwonnet.wx.exception;

public class WeixinException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6211259113042351094L;

	public WeixinException(String message) {
		super(message);
	}

	public WeixinException(Throwable cause) {
		super(cause);
	}

	public WeixinException(String message, Throwable cause) {
		super(message, cause);
	}
}

package com.iwonnet.wx.service;

import java.io.IOException;

import net.sf.json.JSONArray;

import com.iwonnet.wx.exception.WeixinException;
import com.jfinal.upload.UploadFile;

public interface IWebService {

  public String getToken(String appid, String secret);

  public String fileUpload(String yhbh, String item, UploadFile uploadFile, String description,
      String sfgx) throws IOException;

  /**
   * 创建分组
   * 
   * @param access_token
   * @param fzbh
   *         分组编号
   * @return
   * @date 2015-3-16
   * @author zhongjf
   */
  public String groupCreate(String access_token, String fzbh);

  /**
   * 同步分组信息 <br>
   * 注意：本操作会覆盖本地为同步的信息。
   * 
   * @param wxid
   * @param access_token
   * @return
   * @date 2015-3-16
   * @author zhongjf
   */
  public String syncGroupList(Long wxid, String access_token);

  /**
   * 修改分组名称
   * 
   * @param wxid
   * @param access_token
   * @param ofzmc
   * @param nfzmc
   * @return
   * @date 2015-3-16
   * @author zhongjf
   */
  public String rename(String access_token, String fzbh, String fzmc);

  /**
   * 查询用户所在分组
   * 
   * @return
   * @date 2015-3-16
   * @author zhongjf
   */
  public String getid(Long wxid, String access_token, String openid);

  /**
   * 移动用户分组
   * 
   * @param access_token
   * @param openid
   * @param fzbh
   * @return
   * @date 2015-3-16
   * @author zhongjf
   */
  public String move(String access_token, String openid, String fzbh);

  /**
   * 批量移动
   * 
   * @param access_token
   * @param jsArr
   * @param fzbh
   * @return
   * @date 2015-3-17
   * @author zhongjf
   */
  public String batchupdate(String access_token, JSONArray jsArr, String fzbh);

  /**
   * 移动用户分组
   * 
   * @param access_token
   * @param openid
   * @param fzbh
   * @return
   * @date 2015-3-16
   * @author zhongjf
   */
  public String updateremark(String access_token, String openid, String name);

  /*****************************************************************************
   * 删除分组信息，不调用微信端，若数据为同步，直接delete
   * 
   * @param access_token
   * @param fzbh
   * @return
   * @date 2015-3-17
   * @author zhongjf
   */
  public String deleteGroup(String access_token, String fzbh);

  /**
   * 获取微信关注用户基本信息
   * 
   * @param access_token
   * @param openid
   *         关注用户的openid
   * @param wxid TODO
   * @return
   * @date 2015-3-18
   * @author zhongjf
   */
  public String getUserInfo(String access_token, String openid, Long wxid);

  /**
   * 同步全部关注用户
   * 
   * @param access_token
   * @param yhbh
   *         TODO
   * @param wxid
   *         TODO
   * @return
   * @throws WeixinException
   */
  public String getUserList(String access_token, String yhbh, String wxid) throws WeixinException;

  /**
   * 上传多媒体到微信端
   * 
   * @param access_token
   *         token
   * @param yxbh
   *         影像编号（BC_DMT）
   * @param wxid
   *         微信ID
   * @return
   * @date 2015-3-20
   * @author zhongjf
   */
  public String uploadnews(String access_token, Long yxbh, Long wxid);
  /**
   * 上传多媒体文件(图文信息)
   * @author xz 2015-3-25
   *
   * @param access_token
   * @param wxid
   * @param dlbh
   * @return
   *
   * @return String
   */
   public String messageUploadNews(String access_token, Long wxid, Long dlbh) ;
   /**
    * 群发信息
    * @author xz 2015-8-21
    *
    * @param access_token
    * @param wxid
    * @param dlbh
    * @return
    *
    * @return String
    */
   public String sendMessage(String access_token, String jsonParameter) ;
//   /**
//    * 获取关注用户openid
//    * @author xz 2015-8-14
//    *
//    * @param gzbh
//    * @return
//    *
//    * @return String
//    */
//   public String getOpenId(String gzbh);
   /**
    * 组装发送对象的openid
    * @author xz 2015-5-21
    *
    * @param sendType
    * @param sendYh
    * @return
    * @throws DaoException
    * @throws UnKnowException
    *
    * @return JSONArray
    */
   public String createSendOpenid(String sendType,String sendYh);
}

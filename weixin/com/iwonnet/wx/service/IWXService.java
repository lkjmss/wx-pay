package com.iwonnet.wx.service;

import com.jfinal.weixin.sdk.msg.in.InImageMsg;
import com.jfinal.weixin.sdk.msg.in.InLinkMsg;
import com.jfinal.weixin.sdk.msg.in.InLocationMsg;
import com.jfinal.weixin.sdk.msg.in.InTextMsg;
import com.jfinal.weixin.sdk.msg.in.InVideoMsg;
import com.jfinal.weixin.sdk.msg.in.InVoiceMsg;
import com.jfinal.weixin.sdk.msg.in.event.InFollowEvent;
import com.jfinal.weixin.sdk.msg.in.event.InLocationEvent;
import com.jfinal.weixin.sdk.msg.in.event.InMenuEvent;
import com.jfinal.weixin.sdk.msg.in.event.InQrCodeEvent;
import com.jfinal.weixin.sdk.msg.in.event.InTemplateEvnet;
import com.jfinal.weixin.sdk.msg.in.speech_recognition.InSpeechRecognitionResults;
import com.jfinal.weixin.sdk.msg.out.OutMsg;

public interface IWXService {

  // 处理接收到的文本消息
  public OutMsg processInTextMsg(String appid, InTextMsg inTextMsg);

  // 处理接收到的图片消息
  public OutMsg processInImageMsg(String appid, InImageMsg inImageMsg);

  // 处理接收到的语音消息
  public OutMsg processInVoiceMsg(String appid, InVoiceMsg inVoiceMsg);

  // 处理接收到的视频消息
  public OutMsg processInVideoMsg(String appid, InVideoMsg inVideoMsg);

  // 处理接收到的地址位置消息
  public OutMsg processInLocationMsg(String appid, InLocationMsg inLocationMsg);

  // 处理接收到的链接消息
  public OutMsg processInLinkMsg(String appid, InLinkMsg inLinkMsg);

  // 处理接收到的关注/取消关注事件
  public OutMsg processInFollowEvent(String appid, InFollowEvent inFollowEvent);

  // 处理接收到的扫描带参数二维码事件
  public OutMsg processInQrCodeEvent(String appid, InQrCodeEvent inQrCodeEvent);

  // 处理接收到的上报地理位置事件
  public OutMsg processInLocationEvent(String appid, InLocationEvent inLocationEvent);

  // 处理接收到的自定义菜单事件
  public OutMsg processInMenuEvent(String appid, InMenuEvent inMenuEvent);

  // 处理接收到的语音识别结果
  public OutMsg processInSpeechRecognitionResults(String appid,
      InSpeechRecognitionResults inSpeechRecognitionResults);

  // 模版消息发送结果接受
  public OutMsg processInTemplateEvent(String appid, InTemplateEvnet inTemplateEvnet);
}

package com.iwonnet.wx.service;

import java.util.Map;

import com.iwonnet.wx.exception.WeixinException;

public interface IPayService {

	/**
	 * 获取openid
	 * 
	 * @param code
	 * @param wxid
	 * @return
	 * @throws WeixinException
	 */
	public abstract String getOpenid(String code, String wxid) throws WeixinException;

	/**
	 * 响应支付结果通知
	 * 
	 * @param attach
	 * @param notifyXml
	 * @throws Exception
	 */
	public abstract void responseNotify(String attach, String notifyXml) throws Exception;

	/**
	 * 退款结果通知
	 * 
	 * @param notifyXml
	 * @throws Exception
	 */
	public abstract void refundNotify(String notifyXml) throws Exception;

	/**
	 * 统一下单
	 * 
	 * @param wxid
	 * @param orderInfo
	 * @return
	 * @throws Exception
	 */
	public abstract Map<String, String> unifiedOrder(Long wxid, String orderInfo) throws Exception;

	/**
	 * 查询订单
	 * 
	 * @param wxid
	 * @param orderNo
	 * @return
	 * @throws Exception
	 */
	public abstract Map<String, String> orderQuery(Long wxid, String orderNo) throws Exception;

	/**
	 * 关闭订单
	 * 
	 * @param wxid
	 * @param orderNo
	 * @return
	 * @throws Exception
	 */
	public abstract Map<String, String> closeOrder(Long wxid, String orderNo) throws Exception;

	/**
	 * 申请退款
	 * 
	 * @param wxid
	 * @param refundInfo
	 * @return
	 * @throws Exception
	 */
	public abstract Map<String, String> refund(Long wxid, String refundInfo) throws Exception;

	/**
	 * 退款查询
	 * 
	 * @param wxid
	 * @param refundNo
	 * @return
	 * @throws Exception
	 */
	public abstract Map<String, String> refundQuery(Long wxid, String refundNo) throws Exception;

	public abstract String sendPicture(String nc, String url) throws Exception;
}

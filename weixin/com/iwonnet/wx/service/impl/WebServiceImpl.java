package com.iwonnet.wx.service.impl;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.iwonnet.wx.beanfactory.BusinessBeanFactory;
import com.iwonnet.wx.business.IWXBusiness;
import com.iwonnet.wx.exception.WeixinException;
import com.iwonnet.wx.model.BcDlmx;
import com.iwonnet.wx.model.BcDmt;
import com.iwonnet.wx.model.BcTwdl;
import com.iwonnet.wx.model.BcTwxx;
import com.iwonnet.wx.model.WxAppxx;
import com.iwonnet.wx.model.WxDmt;
import com.iwonnet.wx.model.WxGzyh;
import com.iwonnet.wx.model.WxYhfz;
import com.iwonnet.wx.model.XtXtpz;
import com.iwonnet.wx.openapi.token.AuthToken;
import com.iwonnet.wx.openapi.token.CacheTokenKit;
import com.iwonnet.wx.service.IWebService;
import com.iwonnet.wx.utils.BeanUtil;
import com.iwonnet.wx.utils.ContentTypeUtil;
import com.iwonnet.wx.utils.EnumUtil;
import com.iwonnet.wx.utils.StringUtil;
import com.iwonnet.wx.utils.EnumUtil.UploadFileType;
import com.jfinal.ext.plugin.qiniu.QiNiuKit;
import com.jfinal.kit.EncryptionKit;
import com.jfinal.kit.StrKit;
import com.jfinal.upload.UploadFile;
import com.jfinal.weixin.sdk.api.ApiResult;
import com.jfinal.weixin.sdk.api.Article;
import com.jfinal.weixin.sdk.api.GroupApi;
import com.jfinal.weixin.sdk.api.MediaUploadApi;
import com.jfinal.weixin.sdk.api.MessageSendApi;
import com.jfinal.weixin.sdk.api.ReturnCode;
import com.jfinal.weixin.sdk.api.User;
import com.jfinal.weixin.sdk.api.UserApi;

public class WebServiceImpl implements IWebService {

  private final SimpleDateFormat fileNameFormat = new SimpleDateFormat("yyyyMMddHHmmssSSS");

  private IWXBusiness getWXBusiness() {
    return BusinessBeanFactory.getInstance().getWXBusiness();
  }

  /**
   * 根据 OPENID 得到 WxGzyh
   * 
   * @param openid
   * @param wxid TODO
   * @return WxGzyh对象
   * @date 2015-3-17
   * @author zhongjf
   */
  public WxGzyh getWXgzyh(String openid) {
    return WxGzyh.dao.findFirst("select * from wx_gzyh where openid = ?", new Object[] { openid });
  }

  /**
   * 得到TOKEN
   * 
   * @param appid
   * @param secret
   * @return
   * @date 2015-3-16
   * @author zhongjf
   */
  public String getToken(String appid, String secret) {
    WxAppxx appxx = getWXBusiness().searchAppxx(appid, secret);
    if (appxx != null) {
      String user_id = appxx.getStr("USERID");
      String timestamp = String.valueOf(System.currentTimeMillis());
      String[] array = { appid, secret, user_id, timestamp };
      Arrays.sort(array);
      String tokenStr = new StringBuilder().append(array[0] + array[1] + array[2] + array[3])
          .toString();
      tokenStr = EncryptionKit.sha1Encrypt(tokenStr).toUpperCase();
      // 将Access_Token 放入 缓存中,保存7200秒
      // 获取 CacheKit.get("access_token", getPara("appid"))
      CacheTokenKit.saveCacheToken(new AuthToken(appxx.getLong("ID"), appid, tokenStr, appxx
          .getStr("appsecret"), timestamp), "access_token");
      JSONObject json = new JSONObject();
      json.put("access_token", tokenStr);
      json.put("expires_in", 7200);
      return json.toString();
    } else {
      return ReturnCode.bulidError(40013);// 40013 不合法的APPID
    }
  }

  /**
   * 文件上传
   * 
   * @param yhbh
   * @param item
   * @param uploadFile
   * @param description
   * @param sfgx
   * @return
   * @throws IOException
   * @date 2015-3-16
   * @author zhongjf
   */
  @Override
  public String fileUpload(String yhbh, String item, UploadFile uploadFile, String description,
      String sfgx) throws IOException {
    String retVal = ReturnCode.bulidError(0);
    BcDmt bcDmt = new BcDmt();
    String ext = uploadFile.getOriginalFileName().substring(
        uploadFile.getOriginalFileName().lastIndexOf("."));
    String fileName = fileNameFormat.format(new Date()) + ext;
    UploadFileType type = ContentTypeUtil.conver(uploadFile.getContentType());
    if (StrKit.notNull(type)) {
      bcDmt.set("YXLX", type.getCode());
      bcDmt.set("MC", uploadFile.getOriginalFileName());
      bcDmt.set("DZ", fileName);
      if (StrKit.notBlank(description))
        bcDmt.set("SM", description);
      if (StrKit.isBlank(sfgx))
        sfgx = "0";
      bcDmt.set("SFGX", sfgx);
      QiNiuKit.put("wechatmedia", fileName, uploadFile.getFile());
      bcDmt.set("YHBH", yhbh);
      bcDmt.set("CZR", yhbh);
      if (!(StrKit.isBlank(item) || item.equals("1"))) {
        bcDmt.set("FZBH", item);
      }
      this.getWXBusiness().saveBcDmt(bcDmt);
    } else {
      retVal = ReturnCode.bulidError(40004);// 不合法的媒体文件类型
    }
    return retVal;
  }

  public void mediaUpload(String appid, String access_token, String file_type, File uploadFile,
      String yxbh) throws IOException, WeixinException {
    WxAppxx appxx = WxAppxx.Dao.findFirst("select * from wx_appxx where appid = ? and zxbz = '0'",
        appid);
    if (appxx == null)
      return;
    JSONObject retJSON = MediaUploadApi.mediaUpload(access_token, file_type, uploadFile).getJson();
    if (retJSON.get("errcode") != null) {
      throw new WeixinException(ReturnCode.bulidError(retJSON.getInt("errcode")));
    } else {
      WxDmt wxDmt = new WxDmt();
      String media_id = retJSON.getString("media_id");
//      wxDmt.set("ID", "SEQ_ID.nextval");
      wxDmt.set("YXBH", yxbh);
      wxDmt.set("WXID", appxx.getStr("ID"));
      wxDmt.set("MEDIAID", media_id);
      wxDmt.save();
    }
  }

  /**
   * 创建用户分组
   * 
   * @param access_token
   * @param fzbh
   * @return
   * @date 2015-3-16
   * @author zhongjf
   */
  public String groupCreate(String access_token, String fzbh) {
    WxYhfz yhfz = WxYhfz.dao.findFirst("select * from wx_yhfz where fzbh = ?",
        new Object[] { fzbh });
    String fzmc = yhfz.getStr("FZMC");
    try {
      JSONObject createJson = new JSONObject();
      createJson.put("name", fzmc);
      JSONObject json = new JSONObject();
      json.put("group", createJson);
      ApiResult reslut = GroupApi.create(access_token, json.toString());
      if (reslut.isSucceed()) {
        yhfz.set("TBZT", EnumUtil.SFBZ.YES.getValue());
        int tbid = reslut.getJson().getJSONObject("group").getInt("id");
        System.out.println(tbid);
        yhfz.set("TBID", tbid);
        yhfz.update();
      }
      return reslut.getJsonStr();
    } catch (Exception e) {
      return ReturnCode.bulidError(-1);
    }
  }

  /**
   * @param wxid
   * @param access_token
   * @return
   * @date 2015-3-16
   * @author zhongjf
   */
  public String syncGroupList(Long wxid, String access_token) {
    WxAppxx appxx = WxAppxx.Dao.findById(wxid);
    String yhbh = appxx.getLong("YHBH").toString();
    // {"groups":[{"id":0,"name":"未分组","count":42},{"id":1,"name":"黑名单","count":0},{"id":2,"name":"星标组","count":0},{"id":102,"name":"vip组","count":0},{"id":103,"name":"432","count":0},{"id":104,"name":"12","count":0},{"id":105,"name":"12","count":0},{"id":106,"name":"12","count":0},{"id":107,"name":"12","count":0},{"id":108,"name":"12","count":0},{"id":109,"name":"12","count":0},{"id":110,"name":"12","count":0}]}
    ApiResult result = GroupApi.get(access_token);
    System.out.println(result.getJsonStr());
    JSONArray groups = result.getJson().getJSONArray("groups");
    try {
      for (Iterator<JSONObject> it = groups.iterator(); it.hasNext();) {
        JSONObject group = it.next();
        int sid = group.getInt("id");
        String name = group.getString("name");
        WxYhfz fz = WxYhfz.dao.findFirst("select * from wx_yhfz where wxid = ? and tbid = ?",
            new Object[] { wxid, sid });
        if (StrKit.notNull(fz)) {
          if (!fz.getStr("FZMC").equals(name)) {
            fz.set("FZMC", name);
            fz.set("TBZT", EnumUtil.SFBZ.YES.getValue());
            fz.update();
          } else {
            continue;
          }
        } else {
          fz = new WxYhfz();
          fz.set("WXID", wxid);
          fz.set("YHBH", yhbh);
          fz.set("FZMC", name);
          fz.set("TBZT", EnumUtil.SFBZ.YES.getValue());
          fz.set("TBID", sid);
          this.getWXBusiness().saveWxYhfz(fz);
        }
      }
      return ReturnCode.bulidError(0);
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }

  /**
   * 修改分组名称
   * 
   * @param wxid
   * @param access_token
   * @param ofzmc
   * @param nfzmc
   * @return
   * @date 2015-3-16
   * @author zhongjf
   */
  public String rename(String access_token, String fzbh, String fzmc) {
    WxYhfz yhfz = WxYhfz.dao.findFirst("select * from wx_yhfz where fzbh = ?",
        new Object[] { fzbh });
    JSONObject createJson = new JSONObject();
    createJson.put("id", yhfz.getLong("TBID"));
    createJson.put("name", fzmc);
    JSONObject json = new JSONObject();
    json.put("group", createJson);
    ApiResult result = GroupApi.rename(access_token, json.toString());
    System.out.println(result.getJsonStr());
    if (result.isSucceed()) {
      yhfz.set("FZMC", fzmc);
      yhfz.set("TBZT", EnumUtil.SFBZ.YES.getValue());
      yhfz.update();
    }
    return result.getJsonStr();
  }

  /**
   * 查询用户所在分组
   * 
   * @return
   * @date 2015-3-16
   * @author zhongjf
   */
  public String getid(Long wxid, String access_token, String openid) {
    JSONObject createJson = new JSONObject();
    createJson.put("openid", openid);
    ApiResult result = GroupApi.getid(access_token, createJson.toString());
    if (result.isSucceed()) {
      int tbid = result.getJson().getJSONObject("group").getInt("groupid");
      WxYhfz yhfz = WxYhfz.dao.findFirst("select * from wx_yhfz where wxid = ? and tbid=? ",
          new Object[] { wxid, tbid });
      if (StrKit.notNull(yhfz)) {
        String fzbh = yhfz.getStr("FZBH");
        JSONObject json = new JSONObject();
        json.put("fzbh", fzbh);
        return json.toString();
      } else {
        return ReturnCode.bulidError(-1);
      }
    } else {
      return result.getJsonStr();
    }
  }

  /**
   * 移动用户分组
   * 
   * @param access_token
   * @param openid
   * @param fzbh
   * @return
   * @date 2015-3-16
   * @author zhongjf
   */
  public String move(String access_token, String openid, String fzbh) {
    WxYhfz yhfz = WxYhfz.dao.findFirst("select * from wx_yhfz where fzbh = ?",
        new Object[] { fzbh });
    JSONObject createJson = new JSONObject();
    createJson.put("to_groupid", yhfz.getLong("TBID"));
    createJson.put("openid", openid);
    ApiResult result = GroupApi.move(access_token, createJson.toString());
    if (result.isSucceed()) {
      WxGzyh gxyh = WxGzyh.dao.findFirst("select * from wx_gzyh where openid = ?",
          new Object[] { openid });
      if (StrKit.notNull(gxyh)) {
        gxyh.set("FZBH", fzbh);
        gxyh.update();
      } else {
        return ReturnCode.bulidError(-1);
      }
    }
    return result.getJsonStr();
  }

  /**
   * 批量移动
   * 
   * @param access_token
   * @param jsArr
   * @param fzbh
   * @return
   * @date 2015-3-17
   * @author zhongjf
   */
  public String batchupdate(String access_token, JSONArray jsArr, String fzbh) {
    /*
     * WxYhfz yhfz = WxYhfz.dao.findFirst("select * from Wx_yhfz where fzbh =
     * ?", new Object[] { fzbh }); JSONObject createJson = new JSONObject();
     * createJson.put("to_groupid", yhfz.getBigDecimal("TBID"));
     * createJson.put("openid_list", jsArr); ApiResult result =
     * GroupApi.batchupdate(access_token, createJson.toString());
     * System.out.println(result.getJsonStr()); if (result.isSucceed()) { for
     * (int i = 0; i < jsArr.size(); i++) { String openid =
     * jsArr.getJSONObject(i).getString("openid"); WxGzyh gxyh =
     * this.getWXgzyh(openid); if (StrKit.notNull(gxyh)) { gxyh.set("FZBH",
     * fzbh); gxyh.update(); } else { return ReturnCode.bulidError(-1); } } }
     * return result.getJsonStr();
     */
    String result = ReturnCode.bulidError(0);
    for (Iterator<JSONObject> it = jsArr.iterator(); it.hasNext();) {
      JSONObject openid_json = it.next();
      String openid = openid_json.getString("openid");
      ApiResult _api = new ApiResult(move(access_token, openid, fzbh));
      if (!_api.isSucceed()) {
        return _api.getJsonStr();
      }
    }
    return result;
  }

  /**
   * 移动用户分组
   * 
   * @param access_token
   * @param openid
   * @param fzbh
   * @return
   * @date 2015-3-16
   * @author zhongjf
   */
  public String updateremark(String access_token, String openid, String name) {

    JSONObject createJson = new JSONObject();
    createJson.put("remark", name);
    createJson.put("openid", openid);
    ApiResult result = UserApi.updateremark(access_token, createJson.toString());
    if (result.isSucceed()) {
      WxGzyh gxyh = getWXgzyh(openid);
      gxyh.set("BZ", name);
      gxyh.update();
    } else {
      return ReturnCode.bulidError(-1);
    }
    return result.getJsonStr();

  }

  /*****************************************************************************
   * 删除分组信息，不调用微信端，若数据为同步，直接delete
   * 
   * @param access_token
   * @param fzbh
   * @return
   * @date 2015-3-17
   * @author zhongjf
   */
  public String deleteGroup(String access_token, String fzbh) {
    WxYhfz yhfz = WxYhfz.dao.findById(fzbh);
    if (StrKit.notNull(yhfz)) {
      String tbzt = yhfz.get("TBZT");
      if (EnumUtil.SFBZ.NO.getValue().equals(tbzt)) {
        return ReturnCode.bulidError(99999);
      } else {
    	JSONObject deleteJson = new JSONObject();
    	deleteJson.put("id", yhfz.getLong("TBID"));
    	JSONObject json = new JSONObject();
    	json.put("group", deleteJson);
    	ApiResult reslut = GroupApi.delete(access_token, json.toString());
    	if(reslut.isSucceed()){
    		yhfz.delete();
            return ReturnCode.bulidError(0);
    	}else{
    		return reslut.getJsonStr();
    	}
      }
    } else {
      return ReturnCode.bulidError(99998);
    }
  }

  /**
   * 获取微信关注用户基本信息
   * 
   * @param access_token
   * @param openid
   *         关注用户的openid
   * @return
   * @date 2015-3-18
   * @author zhongjf
   */
  public String getUserInfo(String access_token, String openid, Long wxid) {

    JSONObject createJson = new JSONObject();
    createJson.put("openid", openid);
    ApiResult result = UserApi.getUserInfo(access_token, openid);
    if (result.isSucceed()) {
      String subscribe = result.getJson().getString("subscribe");
      if (subscribe.equals("0")) {
        return ReturnCode.bulidError(0);
      } else {
        WxGzyh gxyh = getWXgzyh(openid);
        boolean isInsert = false;
        //如果为空，则获取一个
        if(BeanUtil.isEmpty(gxyh)){
        	isInsert = true;
        	gxyh = new WxGzyh();
        	gxyh.set("WXID", wxid);
        	gxyh.set("YHZT", "1");
        	gxyh.set("OPENID", openid);
        	gxyh.set("YHBH", 1L);
        }
        JSONObject js =  result.getJson();
        gxyh.set("NC", StringUtil.objectToString(js.get("nickname")));
        gxyh.set("XB", StringUtil.objectToString(js.get("sex")));
        gxyh.set("SZCS", StringUtil.objectToString(js.get("city")));
        gxyh.set("SZGJ", StringUtil.objectToString(js.get("country")));
        gxyh.set("SZSF", StringUtil.objectToString(js.get("province")));
        gxyh.set("YHTX", StringUtil.objectToString(js.get("headimgurl")));
        gxyh.set("GZSJ", StringUtil.objectToString(js.get("subscribe_time")));
        gxyh.set("UNIONID", StringUtil.objectToString(js.get("unionid")));
        gxyh.set("BZ", StringUtil.objectToString(js.get("remark")));
        gxyh.set("GZSJ", StringUtil.getCurrentTimeString());
        String groupID = StringUtil.objectToString(js.get("groupid"));
        //处理分组编号
        if(BeanUtil.isNotEmpty(groupID)&& BeanUtil.isEmpty(wxid)){
          WxYhfz yhfz = WxYhfz.dao.findFirst("select * wx_yhfz t where t.wxid = ? and tbid = ?",wxid,groupID);
          if(BeanUtil.isNotEmpty(yhfz)){
          	gxyh.set("FZBH", StringUtil.objectToString(yhfz.get("FZBH")));
          }
        }
        gxyh.set("TBZT", "1");
      	gxyh.set("CZR", "ZDGZ");
      	gxyh.set("CZSJ", StringUtil.getCurrentTimeString());
      	gxyh.set("CZRIP", "0.0.0.0");
        if(isInsert){
        	gxyh.save();
        }else{
          gxyh.update();
        }
      }

    } else {
      return ReturnCode.bulidError(-1);
    }
    return result.getJsonStr();

  }

  /*
   * (non-Javadoc)
   * 
   * @see com.iwonnet.wx.service.IWebService#getUserList(java.lang.String)
   */
  public String getUserList(String access_token, String yhbh, String wxid) throws WeixinException {
    List<User> users = new ArrayList<User>();
    String next_openid = "";
    while (true) {
      JSONObject _u_json = UserApi.getFollowers(access_token, next_openid).getJson();
      if (_u_json.getInt("count") == 0) {
        break;
      } else {
        for (Iterator<String> it = _u_json.getJSONObject("data").getJSONArray("openid").iterator(); it
            .hasNext();) {
          String openid = it.next();
          JSONObject user = UserApi.getUserInfo(access_token, openid).getJson();
          users.add((User) JSONObject.toBean(user, User.class));
        }
        next_openid = _u_json.getString("next_openid");
      }
    }
    for (User user : users) {
      this.getWXBusiness().saveOrUpdateGzyh(access_token, user, yhbh, wxid);
    }
    this.getWXBusiness().gzOrQxgz(users,wxid);
    return ReturnCode.bulidError(0);
  }

  public String uploadnews(String access_token, Long yxbh, Long wxid) {
    String load = QiNiuKit.getQiNiuLoadHost();
    BcDmt dmt = BcDmt.Dao.findById(yxbh);
    if (StrKit.notNull(dmt)) {
      String dz = dmt.getStr("DZ");
      load += dz;
      String yxlx = dmt.getStr("YXLX");
      if (EnumUtil.UploadFileType.image.getCode().equals(yxlx)) {
        yxlx = EnumUtil.UploadFileType.image.getValue();
      } else if (EnumUtil.UploadFileType.video.getCode().equals(yxlx)) {
        yxlx = EnumUtil.UploadFileType.video.getValue();
      } else if (EnumUtil.UploadFileType.voice.getCode().equals(yxlx)) {
        yxlx = EnumUtil.UploadFileType.voice.getValue();
      } else {
        return ReturnCode.bulidError(40004);
      }
      String filename = dmt.getStr("MC");
      try {
        URL url = new URL(load);
        ApiResult result = MediaUploadApi.mediaUpload(access_token, yxlx, filename, url);
        if (result.isSucceed()) {
          String media_id = result.getJson().getString("media_id");
          WxDmt _dmt = new WxDmt();
          _dmt.set("YXBH", yxbh);
          _dmt.set("WXID", wxid);
          _dmt.set("MEDIAID", media_id);
          _dmt.save();
        }
        return result.getJsonStr();
      } catch (Exception e) {
        e.printStackTrace();
        return ReturnCode.bulidError(-1, e.getMessage());
      }
    } else {
      return ReturnCode.bulidError(-1, "未找到需要上传的影像！");
    }
  }
 /**
  * 上传多媒体文件(图文信息)
  * @author xz 2015-3-25
  *
  * @param access_token
  * @param wxid
  * @param dlbh
  * @return
  *
  * @return String
  */
  public String messageUploadNews(String access_token, Long wxid, Long dlbh) {
    BcTwdl twdl = BcTwdl.dao.findById(dlbh);
    if (StrKit.notNull(twdl)) {
      String dlmx_sql = "select * from bc_dlmx where dlbh = ?";
      List<BcDlmx> dlmx = BcDlmx.dao.find(dlmx_sql, dlbh);
      List<Article> articles = new ArrayList<Article>();
      String media_id = "";
      for (BcDlmx bcDlmx : dlmx) {
        String thumb_media_id, // 图文消息缩略图的media_id，可以在基础支持-上传多媒体文件接口中获得
        author, // 图文消息的作者
        title, // 图文消息的标题
        content_source_url, // 在图文消息页面点击“阅读原文”后的页面
        content, // 图文消息页面的内容，支持HTML标签
        digest, // 图文消息的描述
        show_cover_pic;// 是否显示封面，1为显示，0为不显示z
        BcTwxx twxx = BcTwxx.dao.findById(bcDlmx.getLong("XXBH"));
        if (StrKit.notNull(twxx)) {
          Long yxbh = twxx.getLong("DTBH");
//          WxDmt dmt = WxDmt.Dao.findFirst("select * from wx_dmt where yxbh = ?", yxbh);
//          if (StrKit.notNull(dmt)) {
//            thumb_media_id = dmt.getStr("MEDIAID");
//          }
          ApiResult upload_result = new ApiResult(this.uploadnews(access_token, yxbh, wxid));
          thumb_media_id = upload_result.getJson().getString("media_id");
          author = twxx.getStr("ZZ");
          title = twxx.getStr("BT");
          content_source_url = twxx.getStr("YWLJ");
          content = twxx.get("ZW");
          digest = twxx.getStr("ZY");
          show_cover_pic = twxx.getStr("ZWTP");
          Article articleItem = new Article(thumb_media_id,author,title,content_source_url,content,digest,
                          show_cover_pic);
          articles.add(articleItem);
        } else {
          return ReturnCode.bulidError(-1, "未找到需要上传到图文信息！");
        }
      }
      JSONObject jObject = new JSONObject();
      jObject.put("articles", articles);
      System.out.println("传入参数："+jObject.toString());
      ApiResult result = MediaUploadApi.newsUpload(access_token, jObject.toString());
      return result.getJsonStr();
    } else {
      return ReturnCode.bulidError(-1, "未找到需要上传到图文信息！");
    }
  }
  /**
   * 群发信息
   * @author xz 2015-8-21
   *
   * @param access_token
   * @param wxid
   * @param dlbh
   * @return
   *
   * @return String
   */
  public String sendMessage(String access_token, String jsonParameter) {
    if (StrKit.notNull(jsonParameter)) {
      try {
        return MessageSendApi.send(access_token, jsonParameter).getJson().toString();
      } catch (Exception e) {
        e.printStackTrace();
        return ReturnCode.bulidError(-1, "群发信息失败："+e.getMessage());
      }
    } else {
      return ReturnCode.bulidError(-1, "群发信息失败：参数为空！");
    }
  }
//  /**
//   * 获取关注用户openid
//   * @author xz 2015-8-14
//   *
//   * @param gzbh
//   * @return
//   *
//   * @return String
//   */
//  public String getOpenId(String gzbh){
//    WxGzyh gzyh = WxGzyh.dao.findFirst("select * from wx_gzyh where yxbh = ?", gzbh);
//    if(StrKit.notNull(gzyh)){
//      return gzyh.getStr("openid");
//    }else{
//      return ReturnCode.bulidError(-1, "未找到要发送的关注用户信息！");
//    }
//  }
  /**
   * 组装发送对象的openid
   * @author xz 2015-5-21
   *
   * @param sendType
   * @param sendYh
   * @return
   * @throws DaoException
   * @throws UnKnowException
   *
   * @return JSONArray
   */
  public String createSendOpenid(String sendType,String sendYh) {
      JSONArray jsonArray = new JSONArray();
      String[] gzbh = null;
//      JSONArray jArray = new JSONArray();
//    for (int i = 0; i < jArray.size(); i++) {
//      String mbbh = jArray.getString(i);
//      this.getIWxBusiness().saveTextMessage(wxid,sendObject,dlbh,mbbh,userBean);
//    }
      try {
        if("1".equals(sendType)){//指定用户
          if(sendYh.indexOf(",") != -1){//发送多个关注用户
            gzbh = sendYh.split(",");
          }else{
            String[] gzbhOne ={sendYh};
            gzbh = gzbhOne;
          }
          for (int i = 0; i < gzbh.length; i++) {
            WxGzyh wxgzyh = WxGzyh.dao.findFirst("select * from wx_gzyh where gzbh = ?",new Object[] {gzbh[i]});
            jsonArray.add(wxgzyh.getStr("OPENID"));
          }
        }else{
          List<WxGzyh> gzyhList = new ArrayList<WxGzyh>();
          if("2".equals(sendType)){//指定分组
            gzyhList = WxGzyh.dao.find("from wx_gzyh t where t.fzbh = ?", sendYh);
            if(StrKit.notNull(gzyhList)){
              return ReturnCode.bulidError(-1, "该组没有关注用户！");
            }
          }else{//全部用户
            gzyhList = WxGzyh.dao.find("from wx_gzyh t where t.fzbh = ?", null);
            if(StrKit.notNull(gzyhList)){
              return ReturnCode.bulidError(-1, "当前微信号下没有关注用户！");
            }
          }
          for (WxGzyh wxGzyh : gzyhList) {
            jsonArray.add(wxGzyh.getStr("OPENID"));
          }
        }
      } catch (Exception e) {
        return ReturnCode.bulidError(-1, "组装发送对象（openid）出错！:"+e.getMessage());
      }
      return jsonArray.toString();
    }
}

package com.iwonnet.wx.service.impl;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.util.HashMap;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import sun.misc.BASE64Decoder;

import com.iwonnet.wx.beanfactory.PayBeanFactory;
import com.iwonnet.wx.business.IPayBusiness;
import com.iwonnet.wx.exception.WeixinException;
import com.iwonnet.wx.model.WxAppxx;
import com.iwonnet.wx.model.WxOrder;
import com.iwonnet.wx.openapi.token.AuthToken;
import com.iwonnet.wx.payapi.IPayConfig;
import com.iwonnet.wx.payapi.Pay;
import com.iwonnet.wx.payapi.PayConfigImpl;
import com.iwonnet.wx.payapi.PayConstants.OrderType;
import com.iwonnet.wx.payapi.PayUtil;
import com.iwonnet.wx.service.IPayService;
import com.iwonnet.wx.utils.BeanUtil;
import com.iwonnet.wx.utils.StringUtil;
import com.jfinal.kit.HttpKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.ehcache.CacheKit;
import com.jfinal.weixin.sdk.api.ApiResult;

public class PayServiceImpl implements IPayService {

	private static final Log LOGGER = LogFactory.getLog(PayServiceImpl.class);

	private IPayBusiness payBusiness;

	private String notifyPrefix;

	/**
	 * 商户配置信息
	 */
	private Map<Long, IPayConfig> configurations = new HashMap<Long, IPayConfig>();

	public void setPayBusiness(IPayBusiness payBusiness) {
		this.payBusiness = payBusiness;
	}

	public void setNotifyPrefix(String notifyPrefix) {
		this.notifyPrefix = notifyPrefix;
	}

	private JSONObject parsePayInfo(String payInfo) throws UnsupportedEncodingException {
		// payInfo = new String(payInfo.getBytes("ISO-8859-1"), "UTF-8");
		return JSONObject.fromObject(payInfo);
	}

	/**
	 * 获取商户配置
	 * 
	 * @param wxid
	 * @return
	 * @throws WeixinException
	 */
	private IPayConfig getConfig(Long wxid) throws WeixinException {
		IPayConfig config = configurations.get(wxid);
		if (config == null) {
			Map<String, Object> key = payBusiness.queryPayKey(wxid);
			synchronized (this) {
				if (configurations.get(wxid) == null) {
					LOGGER.debug("查询商户配置：" + wxid + "\n" + key);
					config = new PayConfigImpl(key);
					configurations.put(wxid, config);
				}
			}
		}
		LOGGER.debug("获取商户配置：" + wxid + "\n" + config.getKey());
		return config;
	}

	@Override
	public String sendPicture(String nc, String url) throws Exception {
		StringBuilder render = new StringBuilder();
		try {
			String token = null;
			for (Object o : CacheKit.getKeys("access_token")) {
				AuthToken access = CacheKit.get("access_token", o, AuthToken.class);
				if (access != null && access.getWxid().equals(1L)) {
					token = access.getAccessToken();
					break;
				}
			}
			if (token == null) {
				token = ((AuthToken) CacheKit.get("access_token", JSONObject.fromObject(new WebServiceImpl().getToken("wx429394bf7b590082", PayUtil.MD5("c2fb2ea03db9f0c33e4701ba0b7af8ce"))).get("access_token"))).getAccessToken();
			}
			System.out.println(token);

			String suffix = "jpg";
			HttpURLConnection sourceConn = HttpKit.getHttpURLConnection(url, "GET", null);
			System.out.print(HttpKit.printReqHeader(sourceConn));
			sourceConn.connect();
			System.out.print(HttpKit.printResHeader(sourceConn));
			render.append(HttpKit.printResHeader(sourceConn));
			if (url.substring(url.length() - 3).toLowerCase().equals("gif") || "image/gif".equalsIgnoreCase(sourceConn.getHeaderField("Content-Type"))) {
				suffix = "gif";
			}
			// 通过输入流获取图片数据
			InputStream in = new DataInputStream(sourceConn.getInputStream());

			Map<String, String> headers = new HashMap<String, String>();
			// 设置边界
			String boundary = "----------" + System.currentTimeMillis();
			headers.put("Content-Type", "multipart/form-data; boundary=" + boundary);
			boundary = "\r\n--" + boundary;
			HttpURLConnection targetConn = HttpKit.getHttpURLConnection("https://api.weixin.qq.com/cgi-bin/media/upload?access_token=" + token + "&type=image", "POST", headers);
			// POST方式不能使用缓存
			// targetConn.setUseCaches(false);
			System.out.print(HttpKit.printReqHeader(targetConn));
			targetConn.connect();
			// 获得输出流
			OutputStream out = new DataOutputStream(targetConn.getOutputStream());

			// 请求正文信息：
			// 表头部分
			StringBuilder sb = new StringBuilder();
			sb.append(boundary).append("\r\n")
					.append("Content-Disposition: form-data; name=\"image\"; filename=\"temp.").append(suffix).append("\"\r\n")
					.append("Content-Type: image/").append(suffix.equals("gif") ? suffix : "jpeg").append("\r\n\r\n");
			// .append("Content-Type: application/octet-stream\r\n\r\n");
			byte[] head = sb.toString().getBytes("UTF-8");
			// 输出表头
			out.write(head);
			// 正文部分
			int length = 0;
			byte[] buff = new byte[1024];
			while ((length = in.read(buff, 0, buff.length)) != -1) {
				out.write(buff, 0, length);
			}
			in.close();
			sourceConn.disconnect();
			// 结尾部分
			// 定义最后数据分隔线
			byte[] foot = (boundary + "--\r\n").getBytes("UTF-8");
			out.write(foot);
			out.flush();
			out.close();
			System.out.print(HttpKit.printResHeader(targetConn));

			StringBuilder res = new StringBuilder();
			BufferedReader reader = new BufferedReader(new InputStreamReader(targetConn.getInputStream()));
			String line = null;
			while ((line = reader.readLine()) != null) {
				res.append(line);
				// res.append("\n");
			}
			targetConn.disconnect();
			ApiResult result = new ApiResult(res.toString());

			if (result.isSucceed()) {
				String media_id = result.getStr("media_id");
				JSONObject json = new JSONObject();
				String openid = Db.queryStr("select openid from wx_gzyh where wxid=1 and nc like ?", "%" + nc + "%");
				if (BeanUtil.isEmpty(openid)) {
					json.put("towxname", nc);
				} else {
					json.put("touser", openid);
				}
				json.put("msgtype", "image");
				JSONObject mediaIdObject = new JSONObject();
				mediaIdObject.put("media_id", media_id);
				json.put("image", mediaIdObject);
				result = new ApiResult(HttpKit.post("https://api.weixin.qq.com/cgi-bin/message/mass/preview?access_token=" + token, json.toString()));
				if (result.isSucceed()) {
					render.insert(0, "\n").insert(0, result.getStr("errmsg"));
				} else {
					render.insert(0, "\n").insert(0, result.getJsonStr());
				}
			} else {
				render.insert(0, "\n").insert(0, result.getJsonStr());
			}
		} catch (Exception e) {
			e.printStackTrace();
			StringWriter sw = null;
			PrintWriter pw = null;
			try {
				sw = new StringWriter();
				pw = new PrintWriter(sw);
				e.printStackTrace(pw);
				render.insert(0, "\n").insert(0, sw.toString());
			} finally {
				pw.close();
				try {
					sw.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
		return render.toString();
	}

	@Override
	public String getOpenid(String code, String wxid) throws WeixinException {
		WxAppxx appxx = payBusiness.queryByCol("id", StringUtil.objectToLong(wxid), WxAppxx.class);
		if (appxx == null) {
			throw new WeixinException("查询公众号信息失败！");
		}
		String url = new StringBuilder("https://api.weixin.qq.com/sns/oauth2/access_token")
				.append("?appid=").append(appxx.getStr("appid"))
				.append("&secret=").append(appxx.getStr("appsecret"))
				.append("&code=").append(code)
				.append("&grant_type=authorization_code").toString();
		JSONObject result = JSONObject.fromObject(HttpKit.get(url));
		Object openid = result.get("openid");
		if (BeanUtil.isEmpty(openid)) {
			throw new WeixinException(result.getString("errmsg"));
		}
		return openid.toString();
	}

	@Override
	public void responseNotify(String attach, String notifyXml) throws Exception {
		if (BeanUtil.isEmpty(attach) || BeanUtil.isEmpty(notifyXml)) {
			throw new WeixinException("非法调用！");
		}
		Map<String, String> notify = PayUtil.xmlToMap(notifyXml);
		if (attach.length() < notifyPrefix.length() + 1 || !attach.startsWith(notifyPrefix) || !attach.equals(notify.get("attach"))) {
			throw new WeixinException("请求参数不合法！");
		}
		Long wxid = StringUtil.objectToLong(attach.substring(notifyPrefix.length()));
		IPayConfig config = configurations.get(wxid);
		if (config == null) {
			throw new WeixinException("无法获取商户配置！");
		}
		Pay pay = PayBeanFactory.getInstance().getPay(config);
		if (!pay.isPayResultNotifySignatureValid(notify)) {
			throw new WeixinException("签名验证失败！");
		}
		WxOrder order = payBusiness.queryByCol(OrderType.out_trade_no.name(), notify.get("out_trade_no"), WxOrder.class);
		if (!order.getLong("total_fee").equals(StringUtil.objectToLong(notify.get("total_fee")))) {
			throw new WeixinException("订单金额不一致！");
		}
		payBusiness.responseNotify(pay, notify);
	}

	@Override
	public void refundNotify(String notifyXml) throws Exception {
		if (BeanUtil.isEmpty(notifyXml)) {
			throw new WeixinException("非法调用！");
		}
		Map<String, String> notify = PayUtil.xmlToMap(notifyXml);
		String appid = notify.get("appid");
		String key = null;
		for (IPayConfig config : configurations.values()) {
			if (config.getAppID().equals(appid)) {
				key = config.getKey();
				break;
			}
		}
		if (key == null) {
			key = Db.queryStr("select `key` from wx_pay where appid=? group by `key`", appid);
		}
		if (key == null) {
			throw new WeixinException("无法获取商户配置！");
		}
		try {
			byte[] password = PayUtil.MD5(key).toLowerCase().getBytes("UTF-8");
			byte[] encrypt = new BASE64Decoder().decodeBuffer(notify.get("req_info"));
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			SecretKeySpec secretKey = new SecretKeySpec(password, "AES");
			cipher.init(Cipher.DECRYPT_MODE, secretKey);
			notify = PayUtil.xmlToMap(new String(cipher.doFinal(encrypt), "UTF-8"));
		} catch (Exception e) {
			throw new WeixinException("解密验证失败！", e);
		}
		int wxid = Db.queryInt("select p.wxid from wx_refund r inner join wx_order o on r.out_trade_no=o.out_trade_no inner join wx_pay p on o.payid=p.id where r.out_refund_no=?", notify.get("out_refund_no"));
		payBusiness.refundNotify(getConfig(StringUtil.objectToLong(wxid)), notify);
	}

	@Override
	public Map<String, String> unifiedOrder(Long wxid, String orderInfo) throws Exception {
		JSONObject orderJson = parsePayInfo(orderInfo);
		orderJson.put("attach", notifyPrefix + wxid);
		return payBusiness.unifiedOrder(getConfig(wxid), orderJson);
	}

	@Override
	public Map<String, String> orderQuery(Long wxid, String orderNo) throws Exception {
		return payBusiness.orderQuery(getConfig(wxid), orderNo, OrderType.out_trade_no);
	}

	@Override
	public Map<String, String> closeOrder(Long wxid, String orderNo) throws Exception {
		return payBusiness.closeOrder(getConfig(wxid), orderNo);
	}

	@Override
	public Map<String, String> refund(Long wxid, String refundInfo) throws Exception {
		return payBusiness.refund(getConfig(wxid), parsePayInfo(refundInfo));
	}

	@Override
	public Map<String, String> refundQuery(Long wxid, String refundNo) throws Exception {
		return payBusiness.refundQuery(getConfig(wxid), refundNo, OrderType.out_refund_no);
	}
}

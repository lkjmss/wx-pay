package com.iwonnet.wx.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.iwonnet.wx.beanfactory.BusinessBeanFactory;
import com.iwonnet.wx.business.AutoReplyFactory;
import com.iwonnet.wx.business.IWXBusiness;
import com.iwonnet.wx.business.IWxAutoReply;
import com.iwonnet.wx.model.BcDlmx;
import com.iwonnet.wx.model.BcDmt;
import com.iwonnet.wx.model.BcJk;
import com.iwonnet.wx.model.BcTwxx;
import com.iwonnet.wx.model.WxAppxx;
import com.iwonnet.wx.model.WxCd;
import com.iwonnet.wx.model.WxGzyh;
import com.iwonnet.wx.model.WxZdhf;
import com.iwonnet.wx.service.IWXService;
import com.iwonnet.wx.utils.BeanUtil;
import com.iwonnet.wx.utils.StringUtil;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;
import com.jfinal.weixin.sdk.api.AccessTokenApi;
import com.jfinal.weixin.sdk.msg.in.InImageMsg;
import com.jfinal.weixin.sdk.msg.in.InLinkMsg;
import com.jfinal.weixin.sdk.msg.in.InLocationMsg;
import com.jfinal.weixin.sdk.msg.in.InMsg;
import com.jfinal.weixin.sdk.msg.in.InTextMsg;
import com.jfinal.weixin.sdk.msg.in.InVideoMsg;
import com.jfinal.weixin.sdk.msg.in.InVoiceMsg;
import com.jfinal.weixin.sdk.msg.in.event.InFollowEvent;
import com.jfinal.weixin.sdk.msg.in.event.InLocationEvent;
import com.jfinal.weixin.sdk.msg.in.event.InMenuEvent;
import com.jfinal.weixin.sdk.msg.in.event.InQrCodeEvent;
import com.jfinal.weixin.sdk.msg.in.event.InTemplateEvnet;
import com.jfinal.weixin.sdk.msg.in.speech_recognition.InSpeechRecognitionResults;
import com.jfinal.weixin.sdk.msg.out.OutImageMsg;
import com.jfinal.weixin.sdk.msg.out.OutMsg;
import com.jfinal.weixin.sdk.msg.out.OutNewsMsg;
import com.jfinal.weixin.sdk.msg.out.OutTextMsg;
import com.jfinal.weixin.sdk.msg.out.OutVoiceMsg;

public class WXServiceImpl implements IWXService {

	private static final String QniuAddress = "http://7j1yxx.com1.z0.glb.clouddn.com/";
	private static final String helpStr = "\t您好，目前公众号处于测试阶段，您目前可以点击菜单中的操作手册获取每一个功能操作说明，"
			+ "您也可以联系客服直接联系在线客服提问；长三角地区（浙江，江苏）用户可致电：4000-018-028，西南地区（重庆）"
			+ "用户可致电：400-023-1171进行电话指导。公众号持续更新中，想要更多惊喜欢迎每天关注 ^_^";
	private static final Logger log = Logger.getLogger(WXServiceImpl.class);

	private IWXBusiness getWXBusiness() {
		return BusinessBeanFactory.getInstance().getWXBusiness();
	}

	private OutMsg autoOutMsg(String appid, InMsg inMsg) {
		WxAppxx appxx = WxAppxx.Dao.findFirst("select * from wx_appxx where appid = ?", appid);
		String wxid = appxx.getStr("ID");
		if (inMsg instanceof InTextMsg) {
			InTextMsg msg = (InTextMsg) inMsg;

		}
		WxZdhf.Dao.find("select * from wx_zdhf where wxid = ?");
		return null;
	}

	/**
	 * 实现父类抽方法，处理文本消息 本例子中根据消息中的不同文本内容分别做出了不同的响应，同时也是为了测试 jfinal weixin
	 * sdk的基本功能： 本方法仅测试了 OutTextMsg、OutNewsMsg、OutMusicMsg 三种类型的OutMsg，
	 * 其它类型的消息会在随后的方法中进行测试
	 */
	public OutMsg processInTextMsg(String appid, InTextMsg inTextMsg) {
		/**
		 * 将Help放入数据库，通过appid进行动态获取
		 */
		String msgContent = inTextMsg.getContent().trim();
		String checkIn = inTextMsg.getContent();
		return getMsgForZdhf(appid, checkIn, inTextMsg);
	}

	public IWxAutoReply getBcJk(String appid, boolean isMenu, String msg) throws Exception {
		List paramList = new ArrayList();
		paramList.add(appid);
		if (isMenu) {
			paramList.add("1");
		} else {
			paramList.add("0");
		}
		paramList.add(msg.trim());
		BcJk jk = BcJk.dao.findFirst("select * from bc_jk where appid = ? and sfcd = ? and xxnr = ?", paramList.toArray());
		IWxAutoReply apf = AutoReplyFactory.getInstance().getReply(jk);
		apf.setIsMenu(isMenu);
		apf.setInMsg(msg);
		return apf;
	}

	/**
	 * 实现父类抽方法，处理图片消息
	 */
	public OutMsg processInImageMsg(String appid, InImageMsg inImageMsg) {
		OutImageMsg outMsg = new OutImageMsg(inImageMsg);
		// 将刚发过来的图片再发回去
		outMsg.setMediaId(inImageMsg.getMediaId());
		return outMsg;
	}

	/**
	 * 实现父类抽方法，处理语音消息
	 */
	public OutMsg processInVoiceMsg(String appid, InVoiceMsg inVoiceMsg) {
		OutVoiceMsg outMsg = new OutVoiceMsg(inVoiceMsg);
		// 将刚发过来的语音再发回去
		outMsg.setMediaId(inVoiceMsg.getMediaId());
		return outMsg;
	}

	/**
	 * 实现父类抽方法，处理视频消息
	 */
	public OutMsg processInVideoMsg(String appid, InVideoMsg inVideoMsg) {
		/*
		 * 腾讯 api 有 bug，无法回复视频消息，暂时回复文本消息代码测试 OutVideoMsg outMsg = new
		 * OutVideoMsg(inVideoMsg); outMsg.setTitle("OutVideoMsg 发送");
		 * outMsg.setDescription("刚刚发来的视频再发回去"); // 将刚发过来的视频再发回去，经测试证明是腾讯官方的 api
		 * 有 bug，待 api bug 却除后再试 outMsg.setMediaId(inVideoMsg.getMediaId());
		 * render(outMsg);
		 */
		OutTextMsg outMsg = new OutTextMsg(inVideoMsg);
		outMsg.setContent("\t视频消息已成功接收，该视频的 mediaId 为: " + inVideoMsg.getMediaId());
		return outMsg;
	}

	/**
	 * 实现父类抽方法，处理地址位置消息
	 */
	public OutMsg processInLocationMsg(String appid, InLocationMsg inLocationMsg) {
		OutTextMsg outMsg = new OutTextMsg(inLocationMsg);
		outMsg.setContent("已收到地理位置消息:" + "\nlocation_X = " + inLocationMsg.getLocation_X()
				+ "\nlocation_Y = " + inLocationMsg.getLocation_Y() + "\nscale = "
				+ inLocationMsg.getScale() + "\nlabel = " + inLocationMsg.getLabel());
		return outMsg;
	}

	/**
	 * 实现父类抽方法，处理链接消息 特别注意：测试时需要发送我的收藏中的曾经收藏过的图文消息，直接发送链接地址会当做文本消息来发送
	 */
	public OutMsg processInLinkMsg(String appid, InLinkMsg inLinkMsg) {
		OutNewsMsg outMsg = new OutNewsMsg(inLinkMsg);
		outMsg
				.addNews(
						"链接消息已成功接收",
						"链接使用图文消息的方式发回给你，还可以使用文本方式发回。点击图文消息可跳转到链接地址页面，是不是很好玩 :)",
						"http://mmbiz.qpic.cn/mmbiz/zz3Q6WSrzq1ibBkhSA1BibMuMxLuHIvUfiaGsK7CC4kIzeh178IYSHbYQ5eg9tVxgEcbegAu22Qhwgl5IhZFWWXUw/0",
						inLinkMsg.getUrl());
		return outMsg;
	}

	/**
	 * 实现父类抽方法，处理关注/取消关注消息
	 */
	public OutMsg processInFollowEvent(String appid, InFollowEvent inFollowEvent) {
		// 订阅：subscribe
		// 取消订阅：unsubscribe
		String event = inFollowEvent.getEvent();
		String openid = inFollowEvent.getFromUserName();
		// 订阅，查询关注用户中是否有该用户，如果有，更新其小心
		WxAppxx appxx = WxAppxx.Dao.findFirst("select * from wx_appxx where appid = ?", appid);
		WxGzyh gzyh = WxGzyh.dao.findFirst("select * from wx_gzyh t where t.openid = ? and t.wxid = ?", openid, appxx.getLong("ID"));
		if ("subscribe".equals(event)) {
			String access_token = AccessTokenApi.getAccessToken().getAccessToken();
			WebServiceImpl wsi = new WebServiceImpl();
			wsi.getUserInfo(access_token, openid, appxx.getLong("ID"));
			// 取消订阅，查询关注用户钟是否有该用户，如果有，则删除该用户
		} else {
			if (BeanUtil.isNotEmpty(gzyh)) {
				gzyh.delete();
			}
		}
		return getMsgForZdhf(appid, "wxgz", inFollowEvent);
	}

	/**
	 * 实现父类抽方法，处理扫描带参数二维码事件
	 */
	public OutMsg processInQrCodeEvent(String appid, InQrCodeEvent inQrCodeEvent) {
		return getMsgForZdhf(appid, "help", inQrCodeEvent);
	}

	/**
	 * 实现父类抽方法，处理上报地理位置事件
	 */
	public OutMsg processInLocationEvent(String appid, InLocationEvent inLocationEvent) {
		// OutTextMsg outMsg = new OutTextMsg(inLocationEvent);
		// outMsg.setContent("processInLocationEvent() 方法测试成功");
		// render(outMsg);
		return null;
	}

	/**
	 * 实现父类抽方法，处理自定义菜单事件
	 */
	public OutMsg processInMenuEvent(String appid, InMenuEvent inMenuEvent) {
		WxAppxx appxx = WxAppxx.Dao.findFirst("select * from wx_appxx where appid=?", appid);
		WxCd wxcd = WxCd.dao.findFirst("select * from wx_cd where wbnr=? and wxid=?", inMenuEvent.getEventKey(), appxx.getLong("id"));
		OutMsg outMsg = null;
		// 如果没有自动回复
		if (BeanUtil.isEmpty(wxcd)) {
			outMsg = (OutTextMsg) getMegIfEmptyMes(appxx);
			// 如果图文消息不为空
		} else if (BeanUtil.isNotEmpty(wxcd.get("XXBH"))) {
			Long xxbh = wxcd.getLong("XXBH");
			outMsg = getOutNewMsgForDlbh(xxbh, inMenuEvent.getFromUserName());
			// 如果文本内容不为空
		} else if (BeanUtil.isNotEmpty(wxcd.get("WBXY"))) {
			String wbxy = wxcd.getStr("WBXY");
			if (wbxy.startsWith("BussinessTemplate-")) {
				String openid = inMenuEvent.getFromUserName();
				String tableName = wbxy.substring("BussinessTemplate-".length(), wbxy.length());
				if ("HisRjhz".equals(tableName)) {
					Record result = Db.findFirst("select z.zhmc,r.xjje,r.ybjsje,DATE_FORMAT(NOW(),'%Y-%m-%d') jzrq,r.zje,case r.dzjg when '0' then '一致' when '-1' then '不一致' when '9' then '未对账' else '无数据' end dzjg from his_rjhz r inner join his_zhxx z on r.yybh=z.yybh inner join his_zhbd b on z.yybh=b.yybh where jzrq=DATE_FORMAT(NOW()-INTERVAL 1 DAY,'%Y%m%d') and b.openid=? order by r.gxrq desc,r.jzrq desc limit 1", openid);
					StringBuffer temp = new StringBuffer();
					temp.append("【日结汇总查询】");
						if(BeanUtil.isEmpty(result)){//假如查询不到当天的信息，则取出该用户下任意一个机构名做显示用
							result = Db.findFirst("select z.zhmc from his_zhxx z inner join his_zhbd b on z.yybh=b.yybh where b.openid=?  limit 1", openid);
							if(BeanUtil.isNotEmpty(result)){
								temp.append("\n租户名称：").append(result.getStr("zhmc").toString())	
								.append("\n\n今日无新记录");
								temp.append("\n\n<a href='http://www.iwonnet.com:5678/wx/mobile/jsp/sjRjhz.html?openID="+openid+"'>点此查询更多</a>");
							}else{
								temp.append("请前往机构绑定页面进行绑定")
								.append("\n\n<a href='http://www.iwonnet.com:5678/Business/mobile_toUserBind.ac?wxOpenID="+openid+"'>前往绑定页面</a>");
							}
						}else{
							temp.append("\n\n今日最新记录");
							temp.append("\n租户名称：").append(result.getStr("zhmc"))	
							.append("\n发生日期：").append(result.getStr("jzrq"))
							.append("\n总金额：").append(result.get("zje"))
							.append("\n现金金额：").append(result.get("xjje"))
							.append("\n医保金额：").append(result.get("ybjsje"))
							.append("\n对账结果：").append(result.getStr("dzjg"));
							temp.append("\n\n<a href='http://www.iwonnet.com:5678/wx/mobile/jsp/sjRjhz.html?openID="+openid+"'>点此查询更多</a>");
						}
					wbxy = temp.toString();
				} else if ("HisKcdj".equals(tableName)) {
					Record result = Db.findFirst("select z.zhmc,DATE_FORMAT(NOW(),'%Y-%m-%d') dzrq,y.dmxmc djlx,k.jjfsje from (select dzrq,sum(jjfsje) jjfsje,yybh,djlx,gxrq,id from his_kcdj GROUP BY dzrq,jjfsje,yybh,djlx,gxrq,id HAVING dzrq=DATE_FORMAT(NOW()-INTERVAL 1 DAY,'%Y%m%d')) k inner join his_zhxx z on k.yybh=z.yybh inner join his_zhbd b on z.yybh=b.yybh left outer join xt_yhdm y on k.djlx=y.dmxz and y.dmfl='KFYW' where b.openid=? order by k.gxrq desc,k.dzrq desc,k.id desc limit 1", openid);
					StringBuffer temp = new StringBuffer();
					temp.append("【单据业务查询】");
					if(BeanUtil.isEmpty(result)){
						result = Db.findFirst("select z.zhmc,DATE_FORMAT(NOW(),'%Y-%m-%d') dzrq,y.dmxmc djlx from  his_zhxx z inner join his_zhbd b on z.yybh=b.yybh left outer join xt_yhdm y on  y.dmfl='KFYW' where b.openid=? limit 1", openid);
						if(BeanUtil.isNotEmpty(result)){
							temp.append("\n租户名称：").append(result.getStr("zhmc").toString())
							.append("\n\n今日无新记录");
							temp.append("\n\n<a href='http://www.iwonnet.com:5678/wx/mobile/jsp/sjDjyw.html?openID="+openid+"'>点此查询更多</a>");
						}else{
							temp.append("请前往机构绑定页面进行绑定")
							.append("\n\n<a href='http://www.iwonnet.com:5678/Business/mobile_toUserBind.ac?wxOpenID="+openid+"'>前往绑定页面</a>");
						}
					}else{
						temp.append("\n\n最新一条记录")
						.append("\n租户名称：").append(result.getStr("zhmc"))
						.append("\n发生日期：").append(result.getStr("dzrq"))
						.append("\n进价发生金额：").append(result.get("jjfsje"))
						.append("\n单据类型：").append(result.getStr("djlx"));
						temp.append("\n\n<a href='http://www.iwonnet.com:5678/wx/mobile/jsp/sjDjyw.html?openID="+openid+"'>点此查询更多</a>");
					}
					wbxy = temp.toString();
				} else if ("HisYdzjg".equals(tableName)) {
					Record result = Db.findFirst("select z.zhmc,y.dzrq,y.ybbfje,y.dzjg from his_ydzjg y inner join his_zhxx z on y.yybh=z.yybh inner join his_zhbd b on z.yybh=b.yybh where y.dzrq=DATE_FORMAT(NOW(),'%Y%m') and b.openid=? order by y.czrq desc,y.dzrq desc limit 1", openid);
					StringBuffer temp = new StringBuffer();
					temp.append("【月度账务查询】");
						if(BeanUtil.isEmpty(result)){
							result = Db.findFirst("select z.zhmc from his_zhxx z inner join his_zhbd b on z.yybh=b.yybh where b.openid=? limit 1", openid);
							if(BeanUtil.isNotEmpty(result)){
								temp.append("\n租户名称：").append(result.get("zhmc").toString())
								.append("\n\n本月无新记录");
								temp.append("\n\n<a href='http://www.iwonnet.com:5678/wx/mobile/jsp/ydzcx.html?openID="+openid+"'>点此查询更多</a>");
							}else{
								temp.append("请前往机构绑定页面进行绑定")
								.append("\n\n<a href='http://www.iwonnet.com:5678/Business/mobile_toUserBind.ac?wxOpenID="+openid+"'>前往绑定页面</a>");
							}
						}else{
							temp.append("\n\n最新一条记录")
								.append("\n租户名称：").append(result.getStr("zhmc"))
								.append("\n结账日期：").append(result.getStr("dzrq"))
								.append("\n医保拨付金额：").append(result.get("ybbfje"))
								.append("\n对账结果：").append(result.getStr("dzjg"));
							temp.append("\n\n<a href='http://www.iwonnet.com:5678/wx/mobile/jsp/ydzcx.html?openID="+openid+"'>点此查询更多</a>");
						}
					wbxy = temp.toString();
				}
			}
			outMsg = new OutTextMsg().setContent(wbxy);
		} else {
			outMsg = (OutTextMsg) getMegIfEmptyMes(appxx);
		}
		outMsg.setCreateTime(outMsg.now());
		outMsg.setFromUserName(inMenuEvent.getToUserName());
		outMsg.setToUserName(inMenuEvent.getFromUserName());
		return outMsg;
		// renderOutTextMsg("你点击了自定义菜单事件，返回值：" + inMenuEvent.getEventKey());
		/**
		 * String eventKey = inMenuEvent.getEventKey(); try{ IWxAutoReply
		 * autoReply = getBcJk(appid,true, eventKey); String outMsgType =
		 * autoReply.getReplyType(); if("txt".equals(outMsgType)){ String msg =
		 * autoReply.getStringReply(); return renderOutTextMsg(msg,
		 * inMenuEvent); }else if("news".equals(outMsgType)){ OutNewsMsg outMsg
		 * = new OutNewsMsg(inMenuEvent); List newsList =
		 * autoReply.getArticles(); for(int i = 0; i<newsList.size();i++){ News
		 * oneNews = (News) newsList.get(i); outMsg.addNews(oneNews); } return
		 * outMsg; }else{ String msg = "[ERROR]本微信目前处于测试阶段，此功能暂未实现，将于近期完成上线...";
		 * return renderOutTextMsg(msg, inMenuEvent); } }catch(Exception e){
		 * log.error("自动处理"); log.error(e); return
		 * renderOutTextMsg("[ERROR]本微信目前处于测试阶段，此功能暂未实现，将于近期完成上线...",
		 * inMenuEvent); }
		 **/
	}

	private OutMsg renderOutTextMsg(String content, InMsg inMsg) {
		OutTextMsg outMsg = new OutTextMsg(inMsg);
		outMsg.setContent(content);
		return outMsg;
	}

	/**
	 * 实现父类抽方法，处理接收语音识别结果
	 */
	public OutMsg processInSpeechRecognitionResults(String appid,
			InSpeechRecognitionResults inSpeechRecognitionResults) {
		return renderOutTextMsg("语音识别结果： " + inSpeechRecognitionResults.getRecognition(),
				inSpeechRecognitionResults);
	}

	@Override
	public OutMsg processInTemplateEvent(String appid, InTemplateEvnet inTemplateEvnet) {
		getWXBusiness().saveOrUpdateInTemplateEvent(inTemplateEvnet);
		return null;
	}

	/**
	 * 根据图文队列编号查询出对应的图文信息并生成一个OutMsg返回
	 * 
	 * @param dlbh
	 * @param wxOpenID
	 *            TODO
	 * @return
	 */
	private OutMsg getOutNewMsgForDlbh(Long dlbh, String wxOpenID) {
		OutNewsMsg msg = new OutNewsMsg();
		BcDlmx dlmx = new BcDlmx();
		// 根据队列编号获取队列明细集合
		List<BcDlmx> list = dlmx.find("select * from bc_dlmx where dlbh = ?", dlbh);
		for (int i = 0; i < list.size(); i++) {
			Long xxbh = list.get(i).getLong("XXBH");
			// 循环队列明细，取出对应的图文消息，并通过图文消息的影像编号去bc_dmt中查询出图片位置，最后组合成一个msg
			BcTwxx bctwxx = BcTwxx.dao.findFirst("select * from bc_twxx where xxbh = ?", xxbh);
			Long dtbh = bctwxx.getLong("DTBH");
			Long xtbh = bctwxx.getLong("XTBH");
			String url = bctwxx.getStr("URL");
			if (BeanUtil.isNotEmpty(wxOpenID)) {
				if (url.indexOf("?") >= 0) {
					if (url.indexOf("&") <= 0) {
						url = url + "1=1";
					}
					url = url + "&wxOpenID=" + wxOpenID;
				} else {
					url = url + "?wxOpenID=" + wxOpenID;
				}
			}
			String bt = bctwxx.getStr("BT");
			String ds = bctwxx.getStr("ZW");
			Long selectTp = (i == 0 ? dtbh : xtbh);
			BcDmt dmt = BcDmt.Dao.findFirst("select * from bc_dmt where yxbh = ?", selectTp);
			String dz = dmt.getStr("DZ");
			// 往图文消息对象里添加队列明细
			msg.addNews(bt, ds, QniuAddress + dz, url);
		}
		return msg;
	}

	/****
	 * 通用自动回复功能
	 */

	private OutMsg getMsgForZdhf(String appid, String gjc, InMsg inMSG) {
		WxAppxx appxx = WxAppxx.Dao.findFirst("select * from wx_appxx where appid = ?", appid);
		WxZdhf wxZdhf = this.getWXBusiness().searchWxZdhf(appxx.getLong("ID"), gjc);
		OutMsg outMsg = null;
		// 如果没有自动回复
		if (BeanUtil.isEmpty(wxZdhf)) {
			outMsg = (OutTextMsg) getMegIfEmptyMes(appxx);
		} else {
			// 如果图文消息不为空
			String xylx = wxZdhf.getStr("XYLX");
			if (xylx.equals("10006")) {
				Long xxbh = wxZdhf.getLong("XXBH");
				outMsg = getOutNewMsgForDlbh(xxbh, null);
			} else {
				// 如果文本内容为空
				if (!xylx.equals("10001")) {
					outMsg = (OutTextMsg) getMegIfEmptyMes(appxx);
				} else {
					OutTextMsg outTMsg = new OutTextMsg();
					outTMsg.setContent(StringUtil.objectToString(wxZdhf.get("WBNR")));
					outMsg = outTMsg;
				}
			}
		}
		outMsg.setCreateTime(outMsg.now());
		outMsg.setFromUserName(inMSG.getToUserName());
		outMsg.setToUserName(inMSG.getFromUserName());
		return outMsg;
	}

	/**
	 * 当系统没有相应的消息自动回复时，回复帮助消息，当没有帮助消息时，回复指定的文字
	 * 
	 * @param inTextMsg
	 * @param appxx
	 * @return
	 */
	private OutMsg getMegIfEmptyMes(WxAppxx appxx) {
		WxZdhf wxZdhf = this.getWXBusiness().searchWxZdhf(appxx.getLong("ID"), "help");
		if (BeanUtil.isNotEmpty(wxZdhf)) {
			OutTextMsg outMsg = new OutTextMsg();
			outMsg.setContent(StringUtil.objectToString(wxZdhf.get("WBNR")));
			return outMsg;
		} else {
			return getMsgForEmpty();
		}
	}

	private OutMsg getMsgForEmpty() {
		OutTextMsg outMsg = new OutTextMsg();
		outMsg.setContent("该微信的消息回复机制还不完善，将不久更新此内容！");
		return outMsg;
	}
}

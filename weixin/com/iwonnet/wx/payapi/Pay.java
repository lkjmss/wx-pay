package com.iwonnet.wx.payapi;

import java.io.InputStream;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.iwonnet.wx.payapi.PayConstants.SignType;
import com.iwonnet.wx.payapi.PayConstants.TradeType;
import com.jfinal.kit.HttpKit;

public class Pay {

	private static final Log LOGGER = LogFactory.getLog(Pay.class);

	private static String IP = "127.0.0.1";
	private static long expired = 0;

	private boolean useSandbox;
	private String notifyUrl;
	private int expires = 60 * 60;

	private SignType signType;

	private IPayConfig config;

	public void setUseSandbox(boolean useSandbox) {
		this.useSandbox = useSandbox;
		if (useSandbox) {
			signType = SignType.MD5; // 沙箱环境
		} else {
			signType = SignType.HMACSHA256;
		}
	}

	public String getNotifyUrl() {
		return notifyUrl;
	}

	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}

	public void setExpires(int expires) {
		this.expires = expires;
	}

	public IPayConfig getConfig() {
		return config;
	}

	public void setConfig(IPayConfig config) {
		this.config = config;
	}

	private String getUrl(String urlSuffix) {
		return "https://" + PayConstants.DOMAIN_API + urlSuffix;
	}

	private String getIP() {
		long refreshTime = System.currentTimeMillis();
		if (refreshTime > expired) {
			synchronized (Pay.class) {
				if (refreshTime > expired) {
					try {
						IP = HttpKit.get("http://ip.3322.net");
						expired = refreshTime + expires * 1000;
					} catch (Exception e) {
					}
				}
			}
		}
		return IP;
	}

	/**
	 * 判断支付结果通知中的sign是否有效
	 * 
	 * @param reqData
	 *            向wxpay post的请求数据
	 * @return 签名是否有效
	 * @throws Exception
	 */
	public boolean isPayResultNotifySignatureValid(Map<String, String> reqData) throws Exception {
		String signTypeInData = reqData.get(PayConstants.FIELD_SIGN_TYPE);
		SignType signType;
		if (signTypeInData == null) {
			signType = SignType.MD5;
		} else {
			signTypeInData = signTypeInData.trim();
			if (signTypeInData.length() == 0) {
				signType = SignType.MD5;
			} else if (PayConstants.MD5.equals(signTypeInData)) {
				signType = SignType.MD5;
			} else if (PayConstants.HMACSHA256.equals(signTypeInData)) {
				signType = SignType.HMACSHA256;
			} else {
				throw new Exception(String.format("Unsupported sign_type: %s", signTypeInData));
			}
		}
		signType = this.signType;
		return PayUtil.isSignatureValid(reqData, config.getKey(), signType);
	}

	/**
	 * 不需要证书的请求
	 * 
	 * @param urlSuffix
	 *            String
	 * @param reqData
	 *            向wxpay post的请求数据
	 * @return API返回数据
	 * @throws Exception
	 */
	private String requestWithoutCert(String urlSuffix, Map<String, String> reqData) throws Exception {
		String requestData = PayUtil.mapToXml(reqData);
		LOGGER.debug("RequestData:\n" + requestData);
		String responseData = HttpKit.post(getUrl(urlSuffix), requestData);
		LOGGER.debug("ResponseData:\n" + responseData);
		return responseData;
	}

	/**
	 * 需要证书的请求
	 * 
	 * @param urlSuffix
	 *            String
	 * @param reqData
	 *            向wxpay post的请求数据 Map
	 * @return API返回数据
	 * @throws Exception
	 */
	private String requestWithCert(String urlSuffix, Map<String, String> reqData) throws Exception {
		String requestData = PayUtil.mapToXml(reqData);
		LOGGER.debug("RequestData:\n" + requestData);

		char[] password = config.getMchID().toCharArray();
		InputStream certStream = config.getCertStream();
		KeyStore ks = KeyStore.getInstance("PKCS12");
		ks.load(certStream, password);
		KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
		kmf.init(ks, password);
		SSLContext sslContext = SSLContext.getInstance("TLSv1");
		sslContext.init(kmf.getKeyManagers(), null, new SecureRandom());

		String responseData = HttpKit.post(getUrl(urlSuffix), requestData, sslContext.getSocketFactory());
		LOGGER.debug("ResponseData:\n" + responseData);
		return responseData;
	}

	/**
	 * 作用：统一下单<br>
	 * 场景：公共号支付、扫码支付、APP支付
	 * 
	 * @param reqData
	 *            向wxpay post的请求数据
	 * @return API返回数据
	 * @throws Exception
	 */
	public Map<String, String> unifiedOrder(Map<String, String> reqData) throws Exception {
		String url;
		if (useSandbox) {
			url = PayConstants.SANDBOX_UNIFIEDORDER_URL_SUFFIX;
		} else {
			url = PayConstants.UNIFIEDORDER_URL_SUFFIX;
		}
		if (TradeType.NATIVE.name().equals(reqData.get("trade_type"))) {
			reqData.put("spbill_create_ip", getIP());
		}
		reqData.put("notify_url", notifyUrl + reqData.get("attach"));
		String respXml = requestWithoutCert(url, fillRequestData(reqData));
		return processResponseXml(respXml);
	}

	/**
	 * 作用：查询订单<br>
	 * 场景：刷卡支付、公共号支付、扫码支付、APP支付
	 * 
	 * @param reqData
	 *            向wxpay post的请求数据
	 * @return API返回数据
	 * @throws Exception
	 */
	public Map<String, String> orderQuery(Map<String, String> reqData) throws Exception {
		String url;
		if (useSandbox) {
			url = PayConstants.SANDBOX_ORDERQUERY_URL_SUFFIX;
		} else {
			url = PayConstants.ORDERQUERY_URL_SUFFIX;
		}
		String respXml = requestWithoutCert(url, fillRequestData(reqData));
		return processResponseXml(respXml);
	}

	/**
	 * 作用：关闭订单<br>
	 * 场景：公共号支付、扫码支付、APP支付
	 * 
	 * @param reqData
	 *            向wxpay post的请求数据
	 * @return API返回数据
	 * @throws Exception
	 */
	public Map<String, String> closeOrder(Map<String, String> reqData) throws Exception {
		String url;
		if (useSandbox) {
			url = PayConstants.SANDBOX_CLOSEORDER_URL_SUFFIX;
		} else {
			url = PayConstants.CLOSEORDER_URL_SUFFIX;
		}
		String respXml = requestWithoutCert(url, fillRequestData(reqData));
		return processResponseXml(respXml);
	}

	/**
	 * 作用：申请退款<br>
	 * 场景：刷卡支付、公共号支付、扫码支付、APP支付
	 * 
	 * @param reqData
	 *            向wxpay post的请求数据
	 * @return API返回数据
	 * @throws Exception
	 */
	public Map<String, String> refund(Map<String, String> reqData) throws Exception {
		String url;
		if (useSandbox) {
			url = PayConstants.SANDBOX_REFUND_URL_SUFFIX;
		} else {
			url = PayConstants.REFUND_URL_SUFFIX;
		}
		String respXml = requestWithCert(url, fillRequestData(reqData));
		return processResponseXml(respXml);
	}

	/**
	 * 作用：退款查询<br>
	 * 场景：刷卡支付、公共号支付、扫码支付、APP支付
	 * 
	 * @param reqData
	 *            向wxpay post的请求数据
	 * @return API返回数据
	 * @throws Exception
	 */
	public Map<String, String> refundQuery(Map<String, String> reqData) throws Exception {
		String url;
		if (useSandbox) {
			url = PayConstants.SANDBOX_REFUNDQUERY_URL_SUFFIX;
		} else {
			url = PayConstants.REFUNDQUERY_URL_SUFFIX;
		}
		String respXml = requestWithoutCert(url, fillRequestData(reqData));
		return processResponseXml(respXml);
	}

	/**
	 * 向 Map 中添加 appid、mch_id、nonce_str、sign_type、sign <br>
	 * 该函数适用于商户适用于统一下单等接口，不适用于红包、代金券接口
	 * 
	 * @param reqData
	 * @return
	 * @throws Exception
	 */
	private Map<String, String> fillRequestData(Map<String, String> reqData) throws Exception {
		reqData.put("appid", config.getAppID());
		reqData.put("mch_id", config.getMchID());
		reqData.put("nonce_str", PayUtil.generateUUID());
		if (SignType.MD5.equals(signType)) {
			reqData.put("sign_type", PayConstants.MD5);
		} else if (SignType.HMACSHA256.equals(signType)) {
			reqData.put("sign_type", PayConstants.HMACSHA256);
		}
		reqData.put("sign", PayUtil.generateSignature(reqData, config.getKey(), signType));
		return reqData;
	}

	/**
	 * 处理 HTTPS API返回数据，转换成Map对象。return_code为SUCCESS时，验证签名。
	 * 
	 * @param xmlStr
	 *            API返回的XML格式数据
	 * @return Map类型数据
	 * @throws Exception
	 */
	private Map<String, String> processResponseXml(String xmlStr) throws Exception {
		String RETURN_CODE = "return_code";
		String return_code;
		Map<String, String> respData = PayUtil.xmlToMap(xmlStr);
		if (respData.containsKey(RETURN_CODE)) {
			return_code = respData.get(RETURN_CODE);
		} else {
			throw new Exception(String.format("No `return_code` in XML: %s", xmlStr));
		}
		if (return_code.equals(PayConstants.FAIL)) {
			return respData;
		} else if (return_code.equals(PayConstants.SUCCESS)) {
			if (PayUtil.isSignatureValid(respData, config.getKey(), signType)) {
				return respData;
			} else {
				throw new Exception(String.format("Invalid sign value in XML: %s", xmlStr));
			}
		} else {
			throw new Exception(String.format("return_code value %s is invalid in XML: %s", return_code, xmlStr));
		}
	}

	/**
	 * 作用：提交刷卡支付<br>
	 * 场景：刷卡支付
	 * 
	 * @param reqData
	 *            向wxpay post的请求数据
	 * @return API返回数据
	 * @throws Exception
	 */
	public Map<String, String> microPay(Map<String, String> reqData) throws Exception {
		return this.microPay(reqData, 7000, 10000);
	}

	/**
	 * 作用：提交刷卡支付<br>
	 * 场景：刷卡支付
	 * 
	 * @param reqData
	 *            向wxpay post的请求数据
	 * @param connectTimeoutMs
	 *            连接超时时间，单位是毫秒
	 * @param readTimeoutMs
	 *            读超时时间，单位是毫秒
	 * @return API返回数据
	 * @throws Exception
	 */
	public Map<String, String> microPay(Map<String, String> reqData, int connectTimeoutMs, int readTimeoutMs) throws Exception {
		String url;
		if (this.useSandbox) {
			url = PayConstants.SANDBOX_MICROPAY_URL_SUFFIX;
		}
		else {
			url = PayConstants.MICROPAY_URL_SUFFIX;
		}
		String respXml = this.requestWithoutCert(url, this.fillRequestData(reqData));
		return this.processResponseXml(respXml);
	}

	/**
	 * 提交刷卡支付，针对软POS，尽可能做成功 内置重试机制，最多60s
	 * 
	 * @param reqData
	 * @return
	 * @throws Exception
	 */
	public Map<String, String> microPayWithPos(Map<String, String> reqData) throws Exception {
		return this.microPayWithPos(reqData, 7000);
	}

	/**
	 * 提交刷卡支付，针对软POS，尽可能做成功 内置重试机制，最多60s
	 * 
	 * @param reqData
	 * @param connectTimeoutMs
	 * @return
	 * @throws Exception
	 */
	public Map<String, String> microPayWithPos(Map<String, String> reqData, int connectTimeoutMs) throws Exception {
		int remainingTimeMs = 60 * 1000;
		long startTimestampMs = 0;
		Map<String, String> lastResult = null;
		Exception lastException = null;

		while (true) {
			startTimestampMs = PayUtil.getCurrentTimestampMs();
			int readTimeoutMs = remainingTimeMs - connectTimeoutMs;
			if (readTimeoutMs > 1000) {
				try {
					lastResult = this.microPay(reqData, connectTimeoutMs, readTimeoutMs);
					String returnCode = lastResult.get("return_code");
					if (returnCode.equals("SUCCESS")) {
						String resultCode = lastResult.get("result_code");
						String errCode = lastResult.get("err_code");
						if (resultCode.equals("SUCCESS")) {
							break;
						}
						else {
							// 看错误码，若支付结果未知，则重试提交刷卡支付
							if (errCode.equals("SYSTEMERROR") || errCode.equals("BANKERROR") || errCode.equals("USERPAYING")) {
								remainingTimeMs = remainingTimeMs - (int) (PayUtil.getCurrentTimestampMs() - startTimestampMs);
								if (remainingTimeMs <= 100) {
									break;
								}
								else {
									PayUtil.getLogger().info("microPayWithPos: try micropay again");
									if (remainingTimeMs > 5 * 1000) {
										Thread.sleep(5 * 1000);
									}
									else {
										Thread.sleep(1 * 1000);
									}
									continue;
								}
							}
							else {
								break;
							}
						}
					}
					else {
						break;
					}
				} catch (Exception ex) {
					lastResult = null;
					lastException = ex;
				}
			}
			else {
				break;
			}
		}

		if (lastResult == null) {
			throw lastException;
		}
		else {
			return lastResult;
		}
	}

	/**
	 * 作用：撤销订单<br>
	 * 场景：刷卡支付
	 * 
	 * @param reqData
	 *            向wxpay post的请求数据
	 * @return API返回数据
	 * @throws Exception
	 */
	public Map<String, String> reverse(Map<String, String> reqData) throws Exception {
		return this.reverse(reqData, 7000, 10000);
	}

	/**
	 * 作用：撤销订单<br>
	 * 场景：刷卡支付<br>
	 * 其他：需要证书
	 * 
	 * @param reqData
	 *            向wxpay post的请求数据
	 * @param connectTimeoutMs
	 *            连接超时时间，单位是毫秒
	 * @param readTimeoutMs
	 *            读超时时间，单位是毫秒
	 * @return API返回数据
	 * @throws Exception
	 */
	public Map<String, String> reverse(Map<String, String> reqData, int connectTimeoutMs, int readTimeoutMs) throws Exception {
		String url;
		if (this.useSandbox) {
			url = PayConstants.SANDBOX_REVERSE_URL_SUFFIX;
		}
		else {
			url = PayConstants.REVERSE_URL_SUFFIX;
		}
		String respXml = this.requestWithCert(url, this.fillRequestData(reqData));
		return this.processResponseXml(respXml);
	}

	/**
	 * 作用：对账单下载（成功时返回对账单数据，失败时返回XML格式数据）<br>
	 * 场景：刷卡支付、公共号支付、扫码支付、APP支付
	 * 
	 * @param reqData
	 *            向wxpay post的请求数据
	 * @return API返回数据
	 * @throws Exception
	 */
	public Map<String, String> downloadBill(Map<String, String> reqData) throws Exception {
		return this.downloadBill(reqData, 7000, 10000);
	}

	/**
	 * 作用：对账单下载<br>
	 * 场景：刷卡支付、公共号支付、扫码支付、APP支付<br>
	 * 其他：无论是否成功都返回Map。若成功，返回的Map中含有return_code、return_msg、data，
	 * 其中return_code为`SUCCESS`，data为对账单数据。
	 * 
	 * @param reqData
	 *            向wxpay post的请求数据
	 * @param connectTimeoutMs
	 *            连接超时时间，单位是毫秒
	 * @param readTimeoutMs
	 *            读超时时间，单位是毫秒
	 * @return 经过封装的API返回数据
	 * @throws Exception
	 */
	public Map<String, String> downloadBill(Map<String, String> reqData, int connectTimeoutMs, int readTimeoutMs) throws Exception {
		String url;
		if (this.useSandbox) {
			url = PayConstants.SANDBOX_DOWNLOADBILL_URL_SUFFIX;
		}
		else {
			url = PayConstants.DOWNLOADBILL_URL_SUFFIX;
		}
		String respStr = this.requestWithoutCert(url, this.fillRequestData(reqData)).trim();
		Map<String, String> ret;
		// 出现错误，返回XML数据
		if (respStr.indexOf("<") == 0) {
			ret = PayUtil.xmlToMap(respStr);
		}
		else {
			// 正常返回csv数据
			ret = new HashMap<String, String>();
			ret.put("return_code", PayConstants.SUCCESS);
			ret.put("return_msg", "ok");
			ret.put("data", respStr);
		}
		return ret;
	}

	/**
	 * 作用：交易保障<br>
	 * 场景：刷卡支付、公共号支付、扫码支付、APP支付
	 * 
	 * @param reqData
	 *            向wxpay post的请求数据
	 * @return API返回数据
	 * @throws Exception
	 */
	public Map<String, String> report(Map<String, String> reqData) throws Exception {
		return this.report(reqData, 7000, 10000);
	}

	/**
	 * 作用：交易保障<br>
	 * 场景：刷卡支付、公共号支付、扫码支付、APP支付
	 * 
	 * @param reqData
	 *            向wxpay post的请求数据
	 * @param connectTimeoutMs
	 *            连接超时时间，单位是毫秒
	 * @param readTimeoutMs
	 *            读超时时间，单位是毫秒
	 * @return API返回数据
	 * @throws Exception
	 */
	public Map<String, String> report(Map<String, String> reqData, int connectTimeoutMs, int readTimeoutMs) throws Exception {
		String url;
		if (this.useSandbox) {
			url = PayConstants.SANDBOX_REPORT_URL_SUFFIX;
		}
		else {
			url = PayConstants.REPORT_URL_SUFFIX;
		}
		String respXml = this.requestWithoutCert(url, this.fillRequestData(reqData));
		return PayUtil.xmlToMap(respXml);
	}

	/**
	 * 作用：转换短链接<br>
	 * 场景：刷卡支付、扫码支付
	 * 
	 * @param reqData
	 *            向wxpay post的请求数据
	 * @return API返回数据
	 * @throws Exception
	 */
	public Map<String, String> shortUrl(Map<String, String> reqData) throws Exception {
		return this.shortUrl(reqData, 7000, 10000);
	}

	/**
	 * 作用：转换短链接<br>
	 * 场景：刷卡支付、扫码支付
	 * 
	 * @param reqData
	 *            向wxpay post的请求数据
	 * @return API返回数据
	 * @throws Exception
	 */
	public Map<String, String> shortUrl(Map<String, String> reqData, int connectTimeoutMs, int readTimeoutMs) throws Exception {
		String url;
		if (this.useSandbox) {
			url = PayConstants.SANDBOX_SHORTURL_URL_SUFFIX;
		}
		else {
			url = PayConstants.SHORTURL_URL_SUFFIX;
		}
		String respXml = this.requestWithoutCert(url, this.fillRequestData(reqData));
		return this.processResponseXml(respXml);
	}

	/**
	 * 作用：授权码查询OPENID接口<br>
	 * 场景：刷卡支付
	 * 
	 * @param reqData
	 *            向wxpay post的请求数据
	 * @return API返回数据
	 * @throws Exception
	 */
	public Map<String, String> authCodeToOpenid(Map<String, String> reqData) throws Exception {
		return this.authCodeToOpenid(reqData, 7000, 10000);
	}

	/**
	 * 作用：授权码查询OPENID接口<br>
	 * 场景：刷卡支付
	 * 
	 * @param reqData
	 *            向wxpay post的请求数据
	 * @param connectTimeoutMs
	 *            连接超时时间，单位是毫秒
	 * @param readTimeoutMs
	 *            读超时时间，单位是毫秒
	 * @return API返回数据
	 * @throws Exception
	 */
	public Map<String, String> authCodeToOpenid(Map<String, String> reqData, int connectTimeoutMs, int readTimeoutMs) throws Exception {
		String url;
		if (this.useSandbox) {
			url = PayConstants.SANDBOX_AUTHCODETOOPENID_URL_SUFFIX;
		}
		else {
			url = PayConstants.AUTHCODETOOPENID_URL_SUFFIX;
		}
		String respXml = this.requestWithoutCert(url, this.fillRequestData(reqData));
		return this.processResponseXml(respXml);
	}
}

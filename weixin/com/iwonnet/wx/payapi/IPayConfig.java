package com.iwonnet.wx.payapi;

import java.io.InputStream;

public interface IPayConfig {

	/**
	 * 获取 Pay ID
	 */
	public abstract Long getPayID();

	/**
	 * 获取 App ID
	 */
	public abstract String getAppID();

	/**
	 * 获取 Mch ID
	 */
	public abstract String getMchID();

	/**
	 * 获取 API 密钥
	 */
	public abstract String getKey();

	/**
	 * 获取商户证书内容
	 */
	public abstract InputStream getCertStream();

	/**
	 * 获取通知地址
	 */
	public abstract String getNotifyUrl();
}

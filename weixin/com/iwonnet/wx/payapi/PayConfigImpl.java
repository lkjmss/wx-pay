package com.iwonnet.wx.payapi;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Map;

import com.iwonnet.wx.exception.WeixinException;

public class PayConfigImpl implements IPayConfig {

	private final Long payid;

	private final String appid;

	private final String mch_id;

	private final String key;

	private final byte[] certData;

	private final String notify_url;

	public PayConfigImpl(Map<String, Object> key) throws WeixinException {
		try {
			payid = Long.valueOf(key.get("id").toString());
			appid = key.get("appid").toString();
			mch_id = key.get("mch_id").toString();
			this.key = key.get("key").toString();
			InputStream in = new FileInputStream(key.get("certPath").toString());
			certData = new byte[in.available()];
			in.read(certData);
			in.close();
			notify_url = key.get("notify_url").toString();
		} catch (Exception e) {
			throw new WeixinException("秘钥查询失败！", e);
		}
	}

	@Override
	public Long getPayID() {
		return payid;
	}

	@Override
	public String getAppID() {
		return appid;
	}

	@Override
	public String getMchID() {
		return mch_id;
	}

	@Override
	public String getKey() {
		return key;
	}

	@Override
	public InputStream getCertStream() {
		return new ByteArrayInputStream(certData);
	}

	@Override
	public String getNotifyUrl() {
		return notify_url;
	}
}

package com.iwonnet.wx.payapi;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.junit.Test;

import sun.misc.BASE64Decoder;

import com.iwonnet.wx.model.WxAppxx;
import com.jfinal.kit.HttpKit;

public class JunitTest {

	@Test
	public void test() {
		try {
			// System.out.println(HttpKit.get("http://ip.3322.net"));

			String url = "https://img.w3cschool.cn/attachments/image/20161005/1475634939249992.png";
			// url =
			// "http://d9.sina.com.cn/pfpghc2/201710/14/4161d7a3268345408527e9b40cf85419.jpg";
			HttpURLConnection conn = HttpKit.getHttpURLConnection(url, "GET", null);
			System.out.print(HttpKit.printReqHeader(conn));
			conn.connect();
			System.out.print(HttpKit.printResHeader(conn));
			InputStream in = new BufferedInputStream(conn.getInputStream());
			File file = new File("D:" + File.separator + "opt" + File.separator + "temp.jpg");
			if (!file.exists()) {
				file.createNewFile();
			}
			OutputStream out = new FileOutputStream(file);
			int length = 0;
			byte[] buff = new byte[1024];
			while ((length = in.read(buff, 0, buff.length)) != -1) {
				out.write(buff, 0, length);
			}
			out.close();
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test
	public void testModel() {
		WxAppxx appxx = WxAppxx.Dao.findFirstByCol("wxid", 1L);
		System.out.println(appxx);
	}

	@Test
	public void testAES() {
		try {
			byte[] password = "7A87687E9F726C1FF06B0455DA5040EF".toLowerCase().getBytes("UTF-8");
			byte[] encrypt = new BASE64Decoder().decodeBuffer("/FDKh291jHIZJNDOaaHGLLdOvoluXKUNJtcSnNu5InKSMR2DqKg56Cj5UOlknmtG2U/mUVUzecXxUNaRFJ2l7j+V8KG6wXUJTw9pbsEbeSnbzWmBvzKc5SHePzTrUT7jT1WDX8XmmHzrQX9HctAhRjtRrp4T8MZDVk/X6ccezs+ozvIV4rjiIRqgQ8AmIsE3WtHbi88Iqb17sSvLXi13tHzZqZ+nFIGtDO6it4KkTrzSApIBY9rH85NebLcyL5w62Ro+jMPm58hfT3Z3PEVMOb9PFd170YeEma0DcY4O7EEfh6AK2rIwq2oBQgHw0/sTQtT2kM4vl7UOm7bqblhitIwYDKm1Rkb7mMiW1Sehp/bYIkmitItc5eS+xA3UHzHnxonlOOenazCZqSHRpPQnBugrRKprQnBL7CuTndWjo5edK3GvFHQdrgK4tjDzhBWVjfkrwhES7x7F8QgI4oiTZevrE/8mWlsZCHb0inWdJU9B8ntZWp0gPaEfj3kD3WJtzgrkY81NM5tzj6kiAW/FsB+OubsfiFXFMS8r6I0GtbyXC71ZdzoVJuPtt5VO4JRx7ky0H1Qoblvz2PoJWl0ysy+t0OFjWhggcD1NqZbHizfmFLRG/aI+o5lBjrF5lLEg14BGUUdR7OrEYKSodon+Q0WoK3s70C3B4m9NhukMEQfpvgFLY+2yiFwikj74go2N2U4AJaXDz56npWJ+MStoeOa2WoJjQ6oTY1Ihfm20k7t6IncWqEHZ/iy9Cm5E37yvWV8qSw0tW2ZbU7BDb2ojLo67n5UWVEuY9O4s4E9ESlTe4xgKaDTsbKampoavpSgC1rRc1ujyMNuTJ2IvZcjFIqxvTjWMnhJ6Wgl/Wc2IAusa8/TdPJEhSl9UeEBfXHemfI29H9Unxx8vwyLQP6mjIQS2YEKYhzygOjNm4ZB7YmTZcXHlhs1d7Z3G0tW+UEBAIU70jAwrnYkiaBvAP7HWEQ7Q+1Td8OrKjxLY1RtJGR26qX1/6sqA5PwKLeOmJ7bMpJnzBPzP7+5dewdK0nhdCEhi3MeLKd2rjTLhVhufRMPHiD2y+4oURhZYYNfvdjCK");
			Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
			SecretKeySpec secretKey = new SecretKeySpec(password, "AES");
			cipher.init(Cipher.DECRYPT_MODE, secretKey);
			String decrypt = new String(cipher.doFinal(encrypt), "UTF-8");
			System.out.println(decrypt);
			System.out.println(PayUtil.xmlToMap(decrypt));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

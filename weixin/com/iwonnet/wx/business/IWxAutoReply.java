package com.iwonnet.wx.business;

import java.util.List;

import com.jfinal.weixin.sdk.msg.out.News;

/**
 * 微信自动回复接口，配置此接口，可实现微信菜单或者消息的自动恢复功能。
 * 
 * 根据不同的传入参数，可回复不同的结果
 * @author Administrator
 *
 */
public interface IWxAutoReply {
  //是否按钮
  public void setIsMenu(boolean isMenu);
  //传入消息，暂只支持string类型的消息，string类型的消息可以是微信用户输入的文字，可以是用户点击了菜单的KEY
  public void setInMsg(String inMsg);
  //String类型的返回消息
  public String getStringReply();
  //获取返回消息类型
  public String getReplyType();
  //获取图文返回消息
  public List<News>  getArticles();
}

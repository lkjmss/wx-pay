package com.iwonnet.wx.business;

import java.util.List;

import com.iwonnet.wx.exception.WeixinException;
import com.iwonnet.wx.model.BcDmt;
import com.iwonnet.wx.model.WxAppxx;
import com.iwonnet.wx.model.WxCd;
import com.iwonnet.wx.model.WxYhfz;
import com.iwonnet.wx.model.WxZdhf;
import com.jfinal.weixin.sdk.api.User;
import com.jfinal.weixin.sdk.msg.in.InMsg;

public interface IWXBusiness {

  public String test();

  public void saveOrUpdateInTemplateEvent(InMsg msg);

  /**
   * 获取Appxx
   * 
   * @param appid
   * @param secretMD5
   * @return TODO
   */
  public WxAppxx searchAppxx(String appid, String secretMD5);

  public void saveBcDmt(BcDmt bcDmt);

  /**
   * 保存
   * 
   * @param wxCd
   */
  public void saveWxCd(WxCd wxCd);

  /**
   * @param wxYhfz
   * @date 2015-3-16
   * @author zhongjf
   */
  public void saveWxYhfz(WxYhfz wxYhfz);

  /**
   * 更新或保存关注用户信息
   * 
   * @param access_token
   *          token
   * @param user
   *          关注用户信息
   * @param yhbh
   *          用户编号
   * @param wxid
   *          微信ID
   * @throws WeixinException
   */
  public void saveOrUpdateGzyh(String access_token, User user, String yhbh, String wxid)
      throws WeixinException;
  /**
   * 查询自动回复的内容
   * @author xz 2015-3-23
   *
   * @param wxid
   * @return
   *
   * @return WxZdhf
   */
  public WxZdhf searchWxZdhf(Long wxid,String receiveContent);

  /**
   * 判断用户关注与取消关注
   * @param users
   */
public void gzOrQxgz(List<User> users,String wxid);
}

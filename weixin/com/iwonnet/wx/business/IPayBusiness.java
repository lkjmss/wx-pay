package com.iwonnet.wx.business;

import java.util.Map;

import net.sf.json.JSONObject;

import com.iwonnet.wx.model.BaseModel;
import com.iwonnet.wx.payapi.IPayConfig;
import com.iwonnet.wx.payapi.Pay;
import com.iwonnet.wx.payapi.PayConstants.OrderType;

public interface IPayBusiness {

	/**
	 * 通过列名查询数据
	 * 
	 * @param column
	 * @param param
	 * @param clazz
	 * @return
	 */
	public abstract <T extends BaseModel<T>> T queryByCol(String column, Object param, Class<T> clazz);

	/**
	 * 查询商户信息
	 * 
	 * @param wxid
	 * @return
	 */
	public abstract Map<String, Object> queryPayKey(Long wxid);

	/**
	 * 响应支付结果通知
	 * 
	 * @param pay
	 * @param notify
	 * @throws Exception
	 */
	public abstract void responseNotify(Pay pay, Map<String, String> notify) throws Exception;

	/**
	 * 退款结果通知
	 * 
	 * @param config
	 * @param notify
	 * @throws Exception
	 */
	public abstract void refundNotify(IPayConfig config, Map<String, String> notify) throws Exception;

	/**
	 * 统一下单
	 * 
	 * @param config
	 * @param orderJson
	 * @return
	 * @throws Exception
	 */
	public abstract Map<String, String> unifiedOrder(IPayConfig config, JSONObject orderJson) throws Exception;

	/**
	 * 查询订单
	 * 
	 * @param config
	 * @param orderNo
	 * @param orderType
	 * @return
	 * @throws Exception
	 */
	public abstract Map<String, String> orderQuery(IPayConfig config, String orderNo, OrderType orderType) throws Exception;

	/**
	 * 关闭订单
	 * 
	 * @param config
	 * @param orderNo
	 * @return
	 * @throws Exception
	 */
	public abstract Map<String, String> closeOrder(IPayConfig config, String orderNo) throws Exception;

	/**
	 * 申请退款
	 * 
	 * @param config
	 * @param refundJson
	 * @return
	 * @throws Exception
	 */
	public abstract Map<String, String> refund(IPayConfig config, JSONObject refundJson) throws Exception;

	/**
	 * 退款查询
	 * 
	 * @param config
	 * @param refundNo
	 * @param orderType
	 * @return
	 * @throws Exception
	 */
	public abstract Map<String, String> refundQuery(IPayConfig config, String refundNo, OrderType orderType) throws Exception;
}

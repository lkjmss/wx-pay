package com.iwonnet.wx.business;


import com.iwonnet.wx.model.BcJk;

public class AutoReplyFactory {
  private static final AutoReplyFactory INSTANCE = new AutoReplyFactory();

  public static AutoReplyFactory getInstance() {
    return INSTANCE;
  }
  public IWxAutoReply getReply(BcJk jk) throws Exception{
    IWxAutoReply autoReply = null;
    try{
      autoReply = (IWxAutoReply) Class.forName(jk.getStr("HFJK")).newInstance();
    }catch(Exception e){
      throw new Exception(e);
    }
    return autoReply;
  }
}

package com.iwonnet.wx.business.impl;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import net.sf.json.JSONObject;

import com.iwonnet.wx.business.IWXBusiness;
import com.iwonnet.wx.exception.WeixinException;
import com.iwonnet.wx.model.BcDmt;
import com.iwonnet.wx.model.WxAppxx;
import com.iwonnet.wx.model.WxCd;
import com.iwonnet.wx.model.WxGzyh;
import com.iwonnet.wx.model.WxMbFsjl;
import com.iwonnet.wx.model.WxYhfz;
import com.iwonnet.wx.model.WxZdhf;
import com.iwonnet.wx.utils.EnumUtil;
import com.jfinal.kit.StrKit;
import com.jfinal.log.Logger;
import com.jfinal.weixin.sdk.api.ApiResult;
import com.jfinal.weixin.sdk.api.GroupApi;
import com.jfinal.weixin.sdk.api.User;
import com.jfinal.weixin.sdk.msg.in.InMsg;
import com.jfinal.weixin.sdk.msg.in.event.InTemplateEvnet;

public class WXBusinessImpl implements IWXBusiness {

	private static final Logger log = Logger.getLogger(WXBusinessImpl.class);
	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
	private static String default_czr = "WxConsole", default_LocalHost;

	public static void main(String[] args) throws UnknownHostException {
		System.out.println(InetAddress.getLocalHost().getHostAddress());
	}

	static {
		try {
			default_LocalHost = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			default_LocalHost = "127.0.0.1";
		}
	}

	public String test() {
		return "this is spring plug-in test!";
	}

	public void saveOrUpdateInTemplateEvent(InMsg msg) {
		InTemplateEvnet evnet = (InTemplateEvnet) msg;
		WxMbFsjl fsjl = WxMbFsjl.Dao.findFirst("select * from wx_mb_fsjl where msgid = ?",
				evnet.getMsgID());
		String status = evnet.getStatus();
		if (fsjl != null) {
			if ("success".equals(status)) {
				fsjl.set("STATUS", "1");
			} else if ("".equals(status)) {
				fsjl.set("STATUS", "2");
			} else {
				fsjl.set("STATUS", "3");
			}
			fsjl.update();
		} else {
			fsjl = new WxMbFsjl();
			// fsjl.set("ID", "SEQ_ID.nextval");
			fsjl.set("MSGID", evnet.getMsgID());
			fsjl.set("USERID", evnet.getToUserName());
			fsjl.set("TOUSER", evnet.getFromUserName());
			if ("success".equals(status)) {
				fsjl.set("STATUS", "1");
			} else if ("".equals(status)) {
				fsjl.set("STATUS", "2");
			} else {
				fsjl.set("STATUS", "3");
			}
			fsjl.save();
		}
	}

	public WxAppxx searchAppxx(String appid, String secretMD5) {
		WxAppxx appxx = WxAppxx.Dao.findFirst(
				"select * from wx_appxx where appid = ? and MD5(appsecret) = ?", new Object[] { appid,
						secretMD5.toUpperCase() });
		return appxx;
	}

	/**
	 * 查询自动回复的内容
	 * 
	 * @author xz 2015-3-23
	 * 
	 * @param wxid
	 * @return
	 * 
	 * @return WxZdfh
	 */
	public WxZdhf searchWxZdhf(Long wxid, String receiveContent) {
		WxZdhf wxZdfh = WxZdhf.Dao.findFirst("select * from wx_zdhf where ppfs='1' and wxid=?  and ppnr=?", wxid, receiveContent);
		if (wxZdfh != null) return wxZdfh;
		return WxZdhf.Dao.findFirst("select * from wx_zdhf where ppfs='2' and wxid=? and ppnr like ?", wxid, "%" + receiveContent + "%");
	}

	public void saveBcDmt(BcDmt bcDmt) {
		String czsj = sdf.format(new Date());
		bcDmt.set("CZSJ", czsj);
		bcDmt.set("CZRIP", default_LocalHost);
		bcDmt.set("ZXBZ", "0");
		// bcDmt.set("YXBH", "SEQ_ID.nextval");
		bcDmt.save();
	}

	public void saveWxCd(WxCd wxCd) {
		String czsj = sdf.format(new Date());
		// wxCd.set("CDBH", "SEQ_ID.nextval");
		wxCd.set("FBR", default_czr).set("FBSJ", czsj).set("FBRIP", default_LocalHost);
		wxCd.set("CZR", default_czr).set("CZSJ", czsj).set("CZRIP", default_LocalHost);
		wxCd.save();
	}

	public void saveWxYhfz(WxYhfz wxYhfz) {
		String czsj = sdf.format(new Date());
		// wxYhfz.set("FZBH", "SEQ_ID.nextval");
		wxYhfz.set("CZR", default_czr);
		wxYhfz.set("CZSJ", czsj);
		wxYhfz.set("CZRIP", default_LocalHost);
		wxYhfz.save();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.iwonnet.wx.business.IWXBusiness#saveOrUpdateGzyh(java.lang.String,
	 * com.jfinal.weixin.sdk.api.User, java.lang.String, java.lang.String)
	 */
	public void saveOrUpdateGzyh(String access_token, User user, String yhbh, String wxid)
			throws WeixinException {
		String sql = "select * from wx_gzyh where openid = ?";
		JSONObject query = new JSONObject();
		query.put("openid", user.getOpenid());
		// 获取分组
		ApiResult reslut = GroupApi.getid(access_token, query.toString());
		long groupid = 0;
		String fzbh = "";
		if (reslut.isSucceed()) {
			groupid = reslut.getJson().getLong("groupid");
			String groupsql = "select * from wx_yhfz where yhbh = ? and wxid = ? and tbid = ?";
			WxYhfz yhfz = WxYhfz.dao.findFirst(groupsql, new Object[] { yhbh, wxid, groupid });
			if (StrKit.notNull(yhfz)) {
				fzbh = yhfz.getLong("FZBH").toString();
			} else {
				throw new WeixinException("未找到相对应的分组信息，请先同步分组！");
			}
		}
		WxGzyh gzyh = WxGzyh.dao.findFirst(sql, user.getOpenid());
		Date subscribe_time = new Date(user.getSubscribe_time()*1000);
		if (StrKit.notNull(gzyh)) {
			gzyh.set("FZBH", fzbh);
			gzyh.set("WXID", wxid);
			gzyh.set("TBZT", EnumUtil.SFBZ.YES.getValue());
			gzyh.set("OPENID", user.getOpenid());
			gzyh.set("NC", user.getNickname());
			gzyh.set("XB", String.valueOf(user.getSex()));
			gzyh.set("SZGJ", user.getCountry());
			gzyh.set("SZSF", user.getProvince());
			gzyh.set("SZCS", user.getCity());
			gzyh.set("YHTX", user.getHeadimgurl());
			gzyh.set("YHZT", EnumUtil.GZZT.YGZ.getCode());
			gzyh.set("GZSJ", sdf.format(subscribe_time));
			gzyh.set("CZR", default_czr);
			gzyh.set("CZSJ", sdf.format(new Date()));
			gzyh.set("CZRIP", default_LocalHost);
			gzyh.set("BZ", user.getRemark());
			gzyh.update();
		} else {
			gzyh = new WxGzyh();
			// gzyh.set("GZBH", "SEQ_ID.nextval");
			gzyh.set("YHBH", yhbh);
			gzyh.set("WXID", wxid);
			gzyh.set("FZBH", fzbh);
			gzyh.set("TBZT", EnumUtil.SFBZ.YES.getValue());
			gzyh.set("OPENID", user.getOpenid());
			gzyh.set("NC", user.getNickname());
			gzyh.set("XB", String.valueOf(user.getSex()));
			gzyh.set("SZGJ", user.getCountry());
			gzyh.set("SZSF", user.getProvince());
			gzyh.set("SZCS", user.getCity());
			gzyh.set("YHTX", user.getHeadimgurl());
			gzyh.set("YHZT", EnumUtil.GZZT.YGZ.getCode());
			gzyh.set("GZSJ", sdf.format(subscribe_time));
			gzyh.set("CZR", default_czr);
			gzyh.set("CZSJ", sdf.format(new Date()));
			gzyh.set("CZRIP", default_LocalHost);
			gzyh.set("BZ", user.getRemark());
			gzyh.save();
		}
	}
	public void gzOrQxgz(List<User> users,String wxid){
		String sql = "select * from wx_gzyh where wxid = ? ";
		List<WxGzyh> gzyhs = WxGzyh.dao.find(sql,wxid);
		for(WxGzyh gzyh:gzyhs){
			String openid = gzyh.getStr("OPENID");
			boolean flag =false;
			for(User user:users){
				String newOpenid = user.getOpenid();
				if(openid.equals(newOpenid)){
					flag = true;
					break;
				}
			}
			if(!flag){
				gzyh.set("YHZT", EnumUtil.GZZT.QXGZ.getCode());
				gzyh.update();
			}
		}
	}
}

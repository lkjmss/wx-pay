package com.iwonnet.wx.business.impl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;

import net.sf.json.JSONObject;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.iwonnet.wx.beanfactory.PayBeanFactory;
import com.iwonnet.wx.business.IPayBusiness;
import com.iwonnet.wx.exception.WeixinException;
import com.iwonnet.wx.model.BaseModel;
import com.iwonnet.wx.model.WxOrder;
import com.iwonnet.wx.model.WxRefund;
import com.iwonnet.wx.payapi.IPayConfig;
import com.iwonnet.wx.payapi.Pay;
import com.iwonnet.wx.payapi.PayConstants;
import com.iwonnet.wx.payapi.PayConstants.OrderType;
import com.iwonnet.wx.payapi.PayConstants.TradeState;
import com.iwonnet.wx.payapi.PayUtil;
import com.iwonnet.wx.utils.StringUtil;
import com.jfinal.kit.HttpKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

public class PayBusinessImpl implements IPayBusiness {

	private static final Log LOGGER = LogFactory.getLog(PayBusinessImpl.class);

	private Pay getPay(IPayConfig config) {
		return PayBeanFactory.getInstance().getPay(config);
	}

	private Map<String, String> parsePayJson(JSONObject payJson) {
		Map<String, String> data = new HashMap<String, String>();
		Iterator<?> it = payJson.keys();
		while (it.hasNext()) {
			String key = (String) it.next();
			String value = payJson.getString(key);
			data.put(key, value);
		}
		return data;
	}

	@Override
	public <T extends BaseModel<T>> T queryByCol(String column, Object param, Class<T> clazz) {
		try {
			return clazz.newInstance().findFirstByCol(column, param);
		} catch (Exception e) {
			throw new RuntimeException("查询异常！", e);
		}
	}

	@Override
	public Map<String, Object> queryPayKey(Long wxid) {
		Map<String, Object> result = new HashMap<String, Object>();
		Record record = Db.findFirst("select id,appid,mch_id,`key`,certPath,notify_url from wx_pay where wxid=?", wxid);
		if (record != null) {
			result = record.getColumns();
		}
		return result;
	}

	private void notifyOuterSys(String type, Long payid, Map<String, String> notify, String notify_url) throws Exception {
		JSONObject json = new JSONObject();
		json.put("type", type);
		String appsecret = Db.queryStr("select xx.appsecret from wx_appxx xx inner join wx_pay p on xx.id=p.wxid where p.id=?", payid);
		String timestamp = String.valueOf(System.currentTimeMillis());
		json.put("timestamp", timestamp);
		int max = 1000000;
		int min = 100000;
		String nonce = String.valueOf(new Random().nextInt(max) % (max - min) + min);
		Map<String, String> queryParas = new HashMap<String, String>();
		queryParas.put("nonce", nonce);
		String[] array = new String[] { appsecret, timestamp, nonce };
		Arrays.sort(array);
		String signature = new StringBuilder().append(array[0]).append(array[1]).append(array[2]).toString();
		signature = PayUtil.MD5(signature);
		json.put("signature", signature);
		json.put("data", notify);
		LOGGER.debug(new StringBuilder("\nNotifyURL:").append(notify_url).append("?nonce=").append(nonce)
				.append("\nNotifyData: ").append(json.toString()));
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		json = JSONObject.fromObject(HttpKit.post(notify_url, queryParas, json.toString(), headers));
		if (!PayConstants.SUCCESS.equals(json.getString("return_code"))) {
			throw new WeixinException(json.getString("return_msg"));
		}
	}

	@Override
	public void responseNotify(Pay pay, Map<String, String> notify) throws Exception {
		IPayConfig config = pay.getConfig();
		String notify_url = config.getNotifyUrl();
		if (notify_url != null && !notify_url.equals(pay.getNotifyUrl())) {
			notifyOuterSys("order", config.getPayID(), notify, notify_url);
		}
		WxOrder order = queryByCol(OrderType.out_trade_no.name(), notify.get("out_trade_no"), WxOrder.class);
		updateOrder(order, notify, TradeState.SUCCESS.name());
	}

	@Override
	public void refundNotify(IPayConfig config, Map<String, String> notify) throws Exception {
		Pay pay = getPay(config);
		String notify_url = config.getNotifyUrl();
		if (notify_url != null && !notify_url.equals(pay.getNotifyUrl())) {
			notifyOuterSys("refund", config.getPayID(), notify, notify_url);
		}
		WxRefund refund = queryByCol(OrderType.out_refund_no.name(), notify.get("out_refund_no"), WxRefund.class);
		updateRefund(refund, notify.get("refund_status"), notify.get("success_time"));
	}

	private void updateOrder(WxOrder order, Map<String, String> result, String trade_state) {
		if (!order.getStr("trade_state").equals(trade_state)) {
			order.set("trade_state", trade_state);
			if (TradeState.SUCCESS.name().equals(trade_state)) {
				order.set("transaction_id", result.get("transaction_id"));
				order.set("time_end", result.get("time_end"));
				if (result.containsKey("trade_state_desc")) {
					order.set("trade_state_desc", result.get("trade_state_desc"));
				}
			}
		}
		order.update();
	}

	private void updateRefund(WxRefund refund, String refund_status, String refund_success_time) {
		if (!refund.getStr("refund_status").equals(refund_status)) {
			refund.set("refund_status", refund_status);
			if (TradeState.SUCCESS.name().equals(refund_status)) {
				refund.set("refund_success_time", refund_success_time);
			}
		}
		refund.update();
	}

	@Override
	public Map<String, String> unifiedOrder(IPayConfig config, JSONObject orderJson) throws Exception {
		Map<String, String> data = parsePayJson(orderJson);
		Map<String, String> result = getPay(config).unifiedOrder(data);
		if (PayConstants.SUCCESS.equals(result.get("return_code"))) {
			WxOrder order = new WxOrder();
			order.set("out_trade_no", data.get("out_trade_no"));
			order.set("body", data.get("body"));
			order.set("total_fee", StringUtil.objectToLong(data.get("total_fee")));
			if (data.containsKey("openid")) {
				order.set("openid", data.get("openid"));
			}
			if (data.containsKey("product_id")) {
				order.set("product_id", data.get("product_id"));
			}
			order.set("payid", config.getPayID());
			order.set("appid", data.get("appid"));
			order.set("mch_id", data.get("mch_id"));
			order.set("trade_type", data.get("trade_type"));
			order.set("time_start", data.get("time_start"));
			order.set("time_expire", data.get("time_expire"));
			order.set("spbill_create_ip", data.get("spbill_create_ip"));
			if (PayConstants.SUCCESS.equals(result.get("result_code"))) {
				order.set("prepay_id", result.get("prepay_id"));
				if (result.containsKey("code_url")) {
					order.set("code_url", result.get("code_url"));
				}
				order.set("trade_state", TradeState.NOTPAY.name());
			} else {
				order.set("trade_state", result.get("result_code"));
				order.set("trade_state_desc", result.get("err_code") + "|" + result.get("err_code_des"));
			}
			order.save();
		}
		return result;
	}

	@Override
	public Map<String, String> orderQuery(IPayConfig config, String orderNo, OrderType orderType) throws Exception {
		Map<String, String> data = new HashMap<String, String>();
		data.put(orderType.name(), orderNo);
		Map<String, String> result = getPay(config).orderQuery(data);
		if (PayConstants.SUCCESS.equals(result.get("return_code")) && PayConstants.SUCCESS.equals(result.get("result_code"))) {
			WxOrder order = queryByCol(orderType.name(), orderNo, WxOrder.class);
			updateOrder(order, result, result.get("trade_state"));
		}
		return result;
	}

	@Override
	public Map<String, String> closeOrder(IPayConfig config, String orderNo) throws Exception {
		Map<String, String> data = new HashMap<String, String>();
		data.put(OrderType.out_trade_no.name(), orderNo);
		Map<String, String> result = getPay(config).closeOrder(data);
		if (PayConstants.SUCCESS.equals(result.get("return_code")) && PayConstants.SUCCESS.equals(result.get("result_code"))) {
			WxOrder order = queryByCol(OrderType.out_trade_no.name(), orderNo, WxOrder.class);
			order.set("trade_state", TradeState.CLOSED.name());
			order.set("trade_state_desc", result.get("result_msg"));
			order.update();
		}
		return result;
	}

	@Override
	public Map<String, String> refund(IPayConfig config, JSONObject refundJson) throws Exception {
		Map<String, String> data = parsePayJson(refundJson);
		Map<String, String> result = getPay(config).refund(data);
		if (PayConstants.SUCCESS.equals(result.get("return_code")) && PayConstants.SUCCESS.equals(result.get("result_code"))) {
			WxRefund refund = new WxRefund();
			refund.set("out_trade_no", data.get("out_trade_no"));
			refund.set("transaction_id", result.get("transaction_id"));
			refund.set("out_refund_no", data.get("out_refund_no"));
			refund.set("refund_id", result.get("refund_id"));
			refund.set("refund_fee", StringUtil.objectToLong(data.get("refund_fee")));
			refund.set("refund_status", "PROCESSING");
			refund.save();
			WxOrder order = queryByCol(OrderType.out_trade_no.name(), refund.get("out_trade_no"), WxOrder.class);
			if (!TradeState.REFUND.name().equals(order.getStr("trade_state"))) {
				order.set("trade_state", TradeState.REFUND.name());
			}
			order.update();
		}
		return result;
	}

	@Override
	public Map<String, String> refundQuery(IPayConfig config, String refundNo, OrderType orderType) throws Exception {
		Map<String, String> data = new HashMap<String, String>();
		data.put(orderType.name(), refundNo);
		Map<String, String> result = getPay(config).refundQuery(data);
		if (PayConstants.SUCCESS.equals(result.get("return_code")) && PayConstants.SUCCESS.equals(result.get("result_code"))) {
			WxRefund refund = queryByCol(orderType.name(), refundNo, WxRefund.class);
			updateRefund(refund, result.get("refund_status_0"), result.get("refund_success_time_0"));
		}
		return result;
	}
}

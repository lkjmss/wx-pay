package com.iwonnet.wx.business.impl;

import com.iwonnet.wx.business.IWxAutoReply;

public abstract class BaseAutoReply implements IWxAutoReply{
  public boolean isMenu;
  public String inMsg;
  @Override
  public void setInMsg(String inMsg) {
    this.inMsg = inMsg;

  }

  @Override
  public void setIsMenu(boolean isMenu) {
    this.isMenu = isMenu;
  }
}

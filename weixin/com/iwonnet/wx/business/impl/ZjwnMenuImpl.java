package com.iwonnet.wx.business.impl;

import java.util.ArrayList;
import java.util.List;

import com.iwonnet.wx.business.IWxAutoReply;
import com.jfinal.weixin.sdk.msg.out.News;

public class ZjwnMenuImpl  extends BaseAutoReply implements IWxAutoReply{

	private final static String  NewInMsg = "V1001_CPFW_CPJX,V2001_CZSC_GYJS,V2002_CZSC_ZYJS,V2003_CZSC_YBDZ,V2004_CZSC_KFCZ,V2005_CZSC_BBDY";
  private String replyType;
  
  @Override
	public List<News> getArticles() {
    replyType = "news";
		List<News> newList = new ArrayList<News>();
		if ("V1001_CPFW_CPJX".equals(this.inMsg)) {
			News news = new News("产品介绍",
					"EasyHIS系统是医疗卫生应用集成平台（ESIP）的重要组成部分，是针对中小型医院药店的实际业务需求研发的一体化在线HIS系统，采用SAAS（Software as a Service：软件即服务）的运维模式，面向中小型医疗机构提供专业技术服务，为医院、药店提供内部诊疗管理和销售管理，实现与城镇居民医保、新农合等各种医保结算系统的实时交易结算。系统能有效地降低区县级中小型医疗机构、药店的信息化成本，提升信息化水平和管理能力，提高信息化投资回报率。",
					"http://www.iwonnet.com/CPJS.jpg", "http://www.iwonnet.com/wx/cpjs/cpjs.html");
			newList.add(news);
		} else if ("V2001_CZSC_GYJS".equals(this.inMsg)) {
			// 药店指南
			News news = new News("药店指南", "关于药店以及医院门诊部的医保（自费）购药结算的操作说明", "http://www.iwonnet.com/GYJS.jpg",
					"http://www.iwonnet.com/wx/Pharmacy/czsm_yd.html");
			newList.add(news);
		} else if ("V2002_CZSC_ZYJS".equals(this.inMsg)) {
			// 医院指南
			News news = new News("医院指南", "关于医院住院部的医保（自费）住院结算，病房病床管理，每日医嘱设定、复核、执行、每日清单打印等操作说明",
					"http://www.iwonnet.com/ZYJS.jpg", "http://www.iwonnet.com/wx/hospital/czsm_yy.html");
			newList.add(news);
		} else if ("V2003_CZSC_YBDZ".equals(this.inMsg)) {
			// Del
			News news = new News("医保对账", "关于医疗机构医保刷卡的日对账、月对账的操作说明", "http://www.iwonnet.com/YBDZ.jpg",
					"http://www.iwonnet.com/wx/index.html");
			newList.add(news);
		} else if ("V2004_CZSC_KFCZ".equals(this.inMsg)) {
			// 库房指南
			News news = new News("库房指南", "关于库房的日常管理和出入库管理", "http://www.iwonnet.com/KFCZ.jpg",
					"http://www.iwonnet.com/wx/inventory/kufang.html");
			newList.add(news);
		} else if ("V2005_CZSC_BBDY".equals(this.inMsg)) {
			// Del
			News news = new News("报表打印", "EasyHIS中各种报表说明以及出处", "http://www.iwonnet.com/BBDY.jpg",
					"http://www.iwonnet.com/wx/index.html");
			newList.add(news);
		}
		return newList;
	}

  @Override
  public String getReplyType() {
  	if(NewInMsg.indexOf(this.inMsg)>=0){
  		return "news";
  	}
  	return "text";
  	
  }

  @Override
  public String getStringReply() {
    replyType = "txt";
    if ("V2005_KHFW_DH".equals(this.inMsg)){
    	return "24小时客服服务电话:\n\n400-023-1171";
    }else  if ("V2005_JFFW_ZH".equals(this.inMsg)){
    	return "开户行：重庆市农村商业银行北部新区支行\n 户  名：鲍家龙 \n 账  号：6228 6711 5140 9571";
    }
    return "本平台还无法识别该内容："+this.inMsg;
  }
}

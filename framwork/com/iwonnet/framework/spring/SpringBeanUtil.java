package com.iwonnet.framework.spring;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * SpringBean工具类
 * 
 * @author yanglh
 * 
 */
public class SpringBeanUtil implements ApplicationContextAware {

	private static ApplicationContext context;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.springframework.context.ApplicationContextAware#setApplicationContext
	 * (org.springframework.context.ApplicationContext)
	 */
	@SuppressWarnings("static-access")
	public void setApplicationContext(ApplicationContext context) throws BeansException {
		this.context = context;
	}

	/**
	 * 获取Spring容器
	 * 
	 * @return Spring容器
	 */
	public static ApplicationContext getContext() {
		return context;
	}

	/**
	 * 从spring容器中获取Bean
	 * 
	 * @param beanName
	 *            Bean名称
	 * @param clazz
	 *            Bean的类
	 * @param <T>
	 *            泛型
	 * @return 实例
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getBean(String beanName, Class<T> clazz) {
		return (T) getContext().getBean(beanName);
	}
}

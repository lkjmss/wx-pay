package com.jfinal.weixin.sdk.msg.in.event;

import com.jfinal.weixin.sdk.msg.in.InMsg;

/**
 * <xml> <ToUserName><![CDATA[gh_7f083739789a]]></ToUserName>
 * <FromUserName><![CDATA[oia2TjuEGTNoeX76QEjQNrcURxG8]]></FromUserName>
 * <CreateTime>1395658920</CreateTime> <MsgType><![CDATA[event]]></MsgType>
 * <Event><![CDATA[TEMPLATESENDJOBFINISH]]></Event> <MsgID>200163836</MsgID>
 * <Status><![CDATA[success]]></Status> </xml>
 * 
 */
public class InTemplateEvnet extends InMsg {

  private String event;
  private String eventKey;
  private String msgID;
  private String status;

  public InTemplateEvnet(String toUserName, String fromUserName, Integer createTime, String msgType) {
    super(toUserName, fromUserName, createTime, msgType);
  }

  public String getEvent() {
    return event;
  }

  public void setEvent(String event) {
    this.event = event;
  }

  public String getEventKey() {
    return eventKey;
  }

  public void setEventKey(String eventKey) {
    this.eventKey = eventKey;
  }

  public String getMsgID() {
    return msgID;
  }

  public void setMsgID(String msgID) {
    this.msgID = msgID;
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

}

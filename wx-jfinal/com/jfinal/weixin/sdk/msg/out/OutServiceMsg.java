package com.jfinal.weixin.sdk.msg.out;

import com.jfinal.weixin.sdk.msg.in.InMsg;

/**
 * 消息转发到多客服 <xml> <ToUserName><![CDATA[touser]]></ToUserName>
 * <FromUserName><![CDATA[fromuser]]></FromUserName>
 * <CreateTime>1399197672</CreateTime>
 * <MsgType><![CDATA[transfer_customer_service]]></MsgType> <TransInfo>
 * <KfAccount>test1@test</KfAccount> </TransInfo></xml>
 */
public class OutServiceMsg extends OutMsg {

  public static final String TEMPLATE = "<xml>\n"
      + "<ToUserName><![CDATA[${__msg.toUserName}]]></ToUserName>\n"
      + "<FromUserName><![CDATA[${__msg.fromUserName}]]></FromUserName>\n"
      + "<CreateTime>${__msg.createTime}</CreateTime>\n"
      + "<MsgType><![CDATA[${__msg.msgType}]]></MsgType>\n" + "</xml>";

  public OutServiceMsg() {
    this.msgType = "transfer_customer_service";
  }

  public OutServiceMsg(InMsg inMsg) {
    super(inMsg);
    this.msgType = "transfer_customer_service";
  }
}

/**
 * Copyright (c) 2011-2014, James Zhan 詹波 (jfinal@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.jfinal.weixin.sdk.jfinal;

import com.iwonnet.wx.controller.WeixinController;
import com.jfinal.weixin.sdk.msg.in.InImageMsg;
import com.jfinal.weixin.sdk.msg.in.InLinkMsg;
import com.jfinal.weixin.sdk.msg.in.InLocationMsg;
import com.jfinal.weixin.sdk.msg.in.InTextMsg;
import com.jfinal.weixin.sdk.msg.in.InVideoMsg;
import com.jfinal.weixin.sdk.msg.in.InVoiceMsg;
import com.jfinal.weixin.sdk.msg.in.event.InFollowEvent;
import com.jfinal.weixin.sdk.msg.in.event.InLocationEvent;
import com.jfinal.weixin.sdk.msg.in.event.InMenuEvent;
import com.jfinal.weixin.sdk.msg.in.event.InQrCodeEvent;
import com.jfinal.weixin.sdk.msg.in.speech_recognition.InSpeechRecognitionResults;

/**
 * WeixinControllerAdapter
 */
public abstract class WeixinControllerAdapter extends WeixinController {

  protected abstract void processInFollowEvent(InFollowEvent inFollowEvent);

  protected abstract void processInTextMsg(String appid, InTextMsg inTextMsg);

  protected abstract void processInMenuEvent(String appid, InMenuEvent inMenuEvent);

  protected void processInImageMsg(String appid, InImageMsg inImageMsg) {

  }

  protected void processInVoiceMsg(String appid, InVoiceMsg inVoiceMsg) {

  }

  protected void processInVideoMsg(String appid, InVideoMsg inVideoMsg) {

  }

  protected void processInLocationMsg(String appid, InLocationMsg inLocationMsg) {

  }

  protected void processInLinkMsg(String appid, InLinkMsg inLinkMsg) {

  }

  protected void processInQrCodeEvent(InQrCodeEvent inQrCodeEvent) {

  }

  protected void processInLocationEvent(InLocationEvent inLocationEvent, String appid) {

  }

  protected void processInSpeechRecognitionResults(String appid,
      InSpeechRecognitionResults inSpeechRecognitionResults) {

  }
}

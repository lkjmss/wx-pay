package com.jfinal.weixin.sdk.api;

import net.sf.json.JSONObject;

import com.iwonnet.wx.utils.EnumUtil.MpMsgType;
import com.iwonnet.wx.utils.StringUtil;
import com.jfinal.kit.HttpKit;
import com.jfinal.kit.StrKit;

/**
 * 客服消息发送，群发消息api
 * 
 * @author yang
 * 
 */
public class MessageSendApi {

	private static String message_custom_send_api = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=";
	private static String message_mass_sendall_api = "https://api.weixin.qq.com/cgi-bin/message/mass/sendall?access_token=";
	private static String message_mass_send_api = "https://api.weixin.qq.com/cgi-bin/message/mass/send?access_token=";
	private static String message_mass_delete_api = "https://api.weixin.qq.com/cgi-bin/message/mass/delete?access_token=";
	private static String message_mass_preview_api = "https://api.weixin.qq.com/cgi-bin/message/mass/preview?access_token=";
	private static String message_mass_get_api = "https://api.weixin.qq.com/cgi-bin/message/mass/get?access_token=";
	private static String message_template_send_api = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=";

	/**
	 * 推送模板消息
	 * 
	 * @param access_token
	 * @param jsonStr
	 * @return
	 */
	public static ApiResult sendTemplate(String access_token, String jsonStr) {
		String jsonResult = HttpKit.post(message_template_send_api + access_token, jsonStr);
		return new ApiResult(jsonResult);
	}

	public static ApiResult sendCustomMessage(String access_token, String jsonStr) {
		if (StrKit.isBlank(access_token)) {
			access_token = AccessTokenApi.getAccessToken().getAccessToken();
		}
		String jsonResult = HttpKit.post(message_custom_send_api + access_token, jsonStr);
		return new ApiResult(jsonResult);
	}

	/**
	 * 全部用戶羣發或根据分组进行群发
	 * 
	 * @param access_token
	 *            token
	 * @param is_to_all
	 *            用于设定是否向全部用户发送，值为true或false，选择true该消息群发给所有用户，
	 *            选择false可根据group_id发送给指定群组的用户
	 * @param group_id
	 *            群发到的分组的group_id，参加用户管理中用户分组接口，若is_to_all值为true，可不填写group_id
	 * @param media_id
	 *            用于群发的消息的media_id
	 * @param content
	 *            发送文本消息时文本的内容
	 * @param msgtype
	 *            群发的消息类型，图文消息为mpnews，文本消息为text，语音为voice，音乐为music，图片为image，
	 *            视频为mpvideo
	 * @return 返回数据示例（正确时的JSON返回结果）：{ "errcode":0,
	 *         "errmsg":"send job submission success", "msg_id":34182 }
	 */
	public static ApiResult sendAll(String access_token, String jsonStr) {
		String jsonResult = HttpKit.post(message_mass_sendall_api + access_token, jsonStr);
		return new ApiResult(jsonResult);
	}

	/**
	 * 根据OpenID列表群发
	 * 
	 * @param access_token
	 *            token
	 * @param touser
	 *            填写图文消息的接收者，一串OpenID列表，OpenID最少1个，最多10000个
	 * @param media_id
	 *            用于群发消息的media_id
	 * @param content
	 *            发送文本消息时文本的内容
	 * @param msgtype
	 *            群发的消息类型，图文消息为mpnews，文本消息为text，语音为voice，音乐为music，图片为image，
	 *            视频为mpvideo
	 * @return 返回数据示例（正确时的JSON返回结果）：{ "errcode":0,
	 *         "errmsg":"send job submission success", "msg_id":34182 }
	 */
	public static ApiResult send(String access_token, String jsonStr) {
		String jsonResult = HttpKit.post(message_mass_send_api + access_token, jsonStr);
		return new ApiResult(jsonResult);
	}

	public static ApiResult sendPreview(String access_token, String jsonStr) {
		String jsonResult = HttpKit.post(message_mass_preview_api + access_token, jsonStr);
		return new ApiResult(jsonResult);
	}

	/**
	 * 删除群发
	 * 
	 * @param access_token
	 *            token
	 * @param msg_id
	 *            发送出去的消息ID
	 * @return 返回数据示例（正确时的JSON返回结果）： { "errcode":0, "errmsg":"ok" }
	 */
	public static ApiResult delete(String access_token, String msg_id) {
		JSONObject delete = new JSONObject();
		delete.put("msg_id", msg_id);
		if (StrKit.isBlank(access_token)) {
			access_token = AccessTokenApi.getAccessToken().getAccessToken();
		}
		String jsonResult = HttpKit.post(message_mass_delete_api + access_token, delete.toString());
		return new ApiResult(jsonResult);
	}

	/**
	 * 查询群发消息发送状态
	 * 
	 * @param access_token
	 *            token
	 * @param msg_id
	 *            发送出去的消息ID
	 * @return 返回数据示例（正确时的JSON返回结果）： { "msg_id":201053012,
	 *         "msg_status":"SEND_SUCCESS" } <br>
	 *         SEND_SUCCESS 表示发送成功<br>
	 *         SEND_FAIL 表示发送失败<br>
	 */
	public static ApiResult get(String access_token, String msg_id) {
		JSONObject delete = new JSONObject();
		delete.put("msg_id", msg_id);
		if (StrKit.isBlank(access_token)) {
			access_token = AccessTokenApi.getAccessToken().getAccessToken();
		}
		String jsonResult = HttpKit.post(message_mass_get_api + access_token, delete.toString());
		return new ApiResult(jsonResult);
	}
}
package com.jfinal.weixin.sdk.api;

import net.sf.json.JSONObject;

public class User {

  /*
   * { "subscribe":1, "openid":"o9CXhjsR5_aVvMwp90KzZZjoZs3c", "nickname":"诚信",
   * "sex":1, "language":"zh_CN", "city":"温州", "province":"浙江", "country":"中国",
   * "headimgurl":
   * "http:\/\/wx.qlogo.cn\/mmopen\/icMjicpxdRODKL3l9YQffjclCSIyq9gka61PE7m5TewZFB5cwysmM5uDMan39zGX3f7HI064eK2RvmIxIWZmrIGc44HudaGnff\/0",
   * "subscribe_time":1418992933, "remark":"123" }
   */

  private int subscribe;
  private String openid;
  private String nickname;
  private int sex;
  private String language;
  private String city;
  private String province;
  private String country;
  private String headimgurl;
  private long subscribe_time;
  private String remark;
  private Integer[] tagid_list;

  public Integer[] getTagid_list() {
	return tagid_list;
}

public void setTagid_list(Integer[] tagid_list) {
	this.tagid_list = tagid_list;
}

public int getSubscribe() {
    return subscribe;
  }

  public void setSubscribe(int subscribe) {
    this.subscribe = subscribe;
  }

  public String getOpenid() {
    return openid;
  }

  public void setOpenid(String openid) {
    this.openid = openid;
  }

  public String getNickname() {
    return nickname;
  }

  public void setNickname(String nickname) {
	this.nickname = nickname;
  }

  public int getSex() {
    return sex;
  }

  public void setSex(int sex) {
    this.sex = sex;
  }

  public String getLanguage() {
    return language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  public String getProvince() {
    return province;
  }

  public void setProvince(String province) {
    this.province = province;
  }

  public String getCountry() {
    return country;
  }

  public void setCountry(String country) {
    this.country = country;
  }

  public String getHeadimgurl() {
    return headimgurl;
  }

  public void setHeadimgurl(String headimgurl) {
    this.headimgurl = headimgurl;
  }

  public long getSubscribe_time() {
    return subscribe_time;
  }

  public void setSubscribe_time(long subscribe_time) {
    this.subscribe_time = subscribe_time;
  }

  public String getRemark() {
    return remark;
  }

  public void setRemark(String remark) {
    this.remark = remark;
  }

  public String toJSONStr() {
    return JSONObject.fromObject(this).toString();
  }
}

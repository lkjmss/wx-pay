/**
 * Copyright (c) 2011-2014, James Zhan 詹波 (jfinal@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.jfinal.weixin.sdk.api;

import java.util.Iterator;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.jfinal.kit.HttpKit;
import com.jfinal.kit.StrKit;
import com.jfinal.weixin.sdk.kit.ParaMap;

/**
 * 用户管理 API
 * https://api.weixin.qq.com/cgi-bin/user/info?access_token=ACCESS_TOKEN
 * &openid=OPENID&lang=zh_CN
 */
public class UserApi {

  private static String getUserInfo = "https://api.weixin.qq.com/cgi-bin/user/info";
  private static String getFollowers = "https://api.weixin.qq.com/cgi-bin/user/get";
  private static String updateremark = "https://api.weixin.qq.com/cgi-bin/user/info/updateremark?access_token=";

  public static ApiResult getUserInfo(String access_token, String openId) {
    if (StrKit.isBlank(access_token)) {
      access_token = AccessTokenApi.getAccessToken().getAccessToken();
    }
    ParaMap pm = ParaMap.create("access_token", access_token).put("openid", openId)
        .put("lang", "zh_CN");
    return new ApiResult(HttpKit.get(getUserInfo, pm.getData()));
  }

  public static ApiResult getFollowers(String access_token, String nextOpenid) {
    if (StrKit.isBlank(access_token)) {
      access_token = AccessTokenApi.getAccessToken().getAccessToken();
    }
    ParaMap pm = ParaMap.create("access_token", access_token);
    if (nextOpenid != null)
      pm.put("next_openid", nextOpenid);
    return new ApiResult(HttpKit.get(getFollowers, pm.getData()));
  }

  public static ApiResult getFollowers(String access_token) {
    return getFollowers(access_token, null);
  }

  public static void main(String[] args) {
    ApiConfig.setAppId("wx429394bf7b590082");
    ApiConfig.setAppSecret("c2fb2ea03db9f0c33e4701ba0b7af8ce");
    String access_token = AccessTokenApi.getAccessToken().getAccessToken();
    JSONObject object = getFollowers(access_token).getJson();
    System.out.println(object.toString());
    System.out.println(getFollowers(access_token, "o9CXhjt1kU0MC_iBIxLBBSoo-xzI").getJsonStr());
    // {"total":44,"count":0,"next_openid":""}

    JSONArray json = object.getJSONObject("data").getJSONArray("openid");
    for (Iterator<String> it = json.iterator(); it.hasNext();) {
      String openid = it.next();
      System.out.println(getUserInfo(access_token, openid).getJsonStr());
    }
  }

  /**
   * 设置备注名
   * 
   * @param access_token
   * @param jsonStr
   *          JSON串
   * @return
   * @date 2015-3-17
   * @author zhongjf
   */
  public static ApiResult updateremark(String access_token, String jsonStr) {
    if (StrKit.isBlank(access_token)) {
      access_token = AccessTokenApi.getAccessToken().getAccessToken();
    }
    return new ApiResult(HttpKit.post(updateremark + access_token, jsonStr));
  }
}

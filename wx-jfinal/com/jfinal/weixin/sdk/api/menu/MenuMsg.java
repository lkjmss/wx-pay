package com.jfinal.weixin.sdk.api.menu;

import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

public class MenuMsg {

  /*
   * { "button":[ { "type":"click", "name":"今日歌曲", "key":"V1001_TODAY_MUSIC" },
   * { "name":"菜单", "sub_button":[ { "type":"view", "name":"搜索",
   * "url":"http://www.soso.com/" }, { "type":"view", "name":"视频",
   * "url":"http://v.qq.com/" }, { "type":"click", "name":"赞一下我们",
   * "key":"V1001_GOOD" }] }] }
   */
  private static JSONObject _menu_msg;
  private static Map<Integer, JSONObject> _menu_buttons;
  static {
    _menu_msg = new JSONObject();
    _menu_buttons = new HashMap<Integer, JSONObject>();
  }

  public void putButton(int order, JSONObject json) {
    _menu_buttons.put(order, json);
  }

  public void putAllButtons(Map<Integer, JSONObject> menu_buttons) {
    _menu_buttons = menu_buttons;
  }

  private void _Bulid() {
    // 清除 JSON
    _menu_msg.clear();
    JSONArray __button = new JSONArray();
    for (int i = 1; i <= 3; i++) {
      JSONObject _button = _menu_buttons.get(i);
      if (_button != null && !_button.isEmpty())
        __button.add(_button);
    }
    _menu_msg.put("button", __button);
  }

  public String getJson() {
    _Bulid();
    return _menu_msg.toString();
  }

  public static void main(String[] args) {
    MenuMsg msg = new MenuMsg();
    msg.putButton(1, JSONObject.fromObject("{\"name\":\"身份认证\", \"sub_button\":["
        + "{\"type\":\"click\", \"name\":\"用户绑定\", \"key\":\"V0001_SFRZ_YHBD\"},"
        + "{\"type\":\"click\", \"name\":\"到期查询\", \"key\":\"V0002_SFRZ_DQCX\"},"
        + "{\"type\":\"click\", \"name\":\"缴费确认\", \"key\":\"V0003_SFRZ_JFQR\"}" + "]}"));
    msg.putButton(2, JSONObject.fromObject("{\"name\":\"产品服务\", \"sub_button\":["
        + "{\"type\":\"click\", \"name\":\"产品介绍\", \"key\":\"V1001_CPFW_CPJX\"},"
        + "{\"type\":\"click\", \"name\":\"在线客服\", \"key\":\"V1002_CUSTOMER_SERVICE\"}" + "]}"));
    msg.putButton(3, JSONObject.fromObject("{\"name\":\"操作指南\", \"sub_button\":["
        + "{\"type\":\"click\", \"name\":\"药店指南\", \"key\":\"V2001_CZSC_GYJS\"},"
        + "{\"type\":\"click\", \"name\":\"医院指南\", \"key\":\"V2002_CZSC_ZYJS\"},"
        // +
        // "{\"type\":\"click\", \"name\":\"医保对账\", \"key\":\"V2003_CZSC_YBDZ\"},"
        + "{\"type\":\"click\", \"name\":\"库房指南\", \"key\":\"V2004_CZSC_KFCZ\"}"
        // +
        // "{\"type\":\"click\", \"name\":\"报表打印\", \"key\":\"V2005_CZSC_BBDY\"}"
        + "]}"));
    System.out.println(msg.getJson());
  }
}

package com.jfinal.weixin.sdk.api;

import net.sf.json.JSONObject;

/**
 * 用于上传的图文消息
 * 
 * @author yanglh
 * 
 */
public class Article {

  private String thumb_media_id;// 图文消息缩略图的media_id，可以在基础支持-上传多媒体文件接口中获得
  private String author;// 图文消息的作者
  private String title;// 图文消息的标题
  private String content_source_url;// 在图文消息页面点击“阅读原文”后的页面
  private String content;// 图文消息页面的内容，支持HTML标签
  private String digest;// 图文消息的描述
  private String show_cover_pic;// 是否显示封面，1为显示，0为不显示

  /**
   * 创建用于上传的图文消息
   * 
   * @param thumb_media_id
   *          图文消息缩略图的media_id，可以在基础支持-上传多媒体文件接口中获得
   * @param author
   *          图文消息的作者
   * @param title
   *          图文消息的标题
   * @param content_source_url
   *          在图文消息页面点击“阅读原文”后的页面
   * @param content
   *          图文消息页面的内容，支持HTML标签
   * @param digest
   *          图文消息的描述
   * @param show_cover_pic
   *          是否显示封面，1为显示，0为不显示
   */
  public Article(String thumb_media_id, String author, String title, String content_source_url,
      String content, String digest, String show_cover_pic) {
    this.thumb_media_id = thumb_media_id;
    this.author = author;
    this.title = title;
    this.content_source_url = content_source_url;
    this.content = content;
    this.digest = digest;
    this.show_cover_pic = show_cover_pic;
  }

  public String getThumb_media_id() {
    return thumb_media_id;
  }

  public void setThumb_media_id(String thumb_media_id) {
    this.thumb_media_id = thumb_media_id;
  }

  public String getAuthor() {
    return author;
  }

  public void setAuthor(String author) {
    this.author = author;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getContent_source_url() {
    return content_source_url;
  }

  public void setContent_source_url(String content_source_url) {
    this.content_source_url = content_source_url;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }

  public String getDigest() {
    return digest;
  }

  public void setDigest(String digest) {
    this.digest = digest;
  }

  public String getShow_cover_pic() {
    return show_cover_pic;
  }

  public void setShow_cover_pic(String show_cover_pic) {
    this.show_cover_pic = show_cover_pic;
  }

  public String toString() {
    return JSONObject.fromObject(this).toString();
  }

  public static void main(String[] args) {
    Article article = new Article(
        "lynDZrCEFaU0A2w-zjhn3uDdhkxyxMeNaCZSHm4yURt3GQMldNE7lnQdKWQF3cg6",
        "",
        "欢迎关注浙江网能微信公众号",
        "https://www.baidu.com/s?wd=%E6%B5%99%E6%B1%9F%E7%BD%91%E8%83%BD%E4%BF%A1%E6%81%AF%E6%8A%80%E6%9C%AF%E6%9C%89%E9%99%90%E5%85%AC%E5%8F%B8&rsv_spt=1&issp=1&f=8&rsv_bp=0&rsv_idx=2&ie=utf-8&tn=baiduhome_pg&rsv_enter=0&rsv_pq=e8c08c0e00012e51&rsv_t=3ae78%2FzEpHGmmE5DVH7MHq1c%2Fj1CLNUxmVYYAIkyZFyeh1TGnoBhf6avs%2BZz%2BTUQQYn1&oq=bean%20to%20jsono&inputT=2354&rsv_sug3=70&rsv_sug1=52&rsv_sug6=7&bs=%E9%92%9F%E4%BF%8A%E9%A3%9E",
        "浙江网能信息技术有限公司成立于天堂硅谷-杭州，是专业从事政务行业应用软件开发、系统集成、信息服务的高新技术企业。公司崇尚服务至上的经营理念，是国内为数不多的专业应用及应用集成服务提供商。公司凭借着全新的服务理念，丰富的项目实施经验和行业知识，发展迅速。公司研发的系列SaaS模式应用服务产品，自推出以来，迅速得到了市场的认可。应用规模和用户呈现爆发式的增长，短时间内即拥有了数千家用户。全新的应用服务理念，改变了用户的思维定式，也为公司创造了稳定的经营收入。公司取得的业绩和技术能力获得了客户的高度认可，为今后承接和实施国内重大信息化建设项目奠定了基础。公司在稳健经营和积极发展的同时，强化服务意识、提高服务质量，为客户提供多种形式、多种渠道的服务，公司着重组建了技术支持中心和客户服务中心，确保服务持续、稳定。“网聚众志，能鼎大成”---是我们的经营理念！“积极、诚实、守信、团结”---是我们的企业文化！通过努力，成为应用服务的先锋，是我们追求的目标。",
        "浙江网能信息技术有限公司成立于天堂硅谷-杭州，是我们追求的目标。",
        "1");
    ApiConfig.setAppId("wx429394bf7b590082");
    ApiConfig.setAppSecret("c2fb2ea03db9f0c33e4701ba0b7af8ce");
    ApiResult result = MediaUploadApi.newsUpload(null, new Article[] { article, article });
    System.out.println(result.getJsonStr());
    // {"type":"news","media_id":"RYmqChUrmXykDNZ8sSpqSRQg84Th7Rx682qqWVUwdxu0dKJ55tHRApwRaR1Dw_4_","created_at":1426666799}

    // ZR7TSxbT7TYqFfih2SYp3W8t2wMagvPwI3JhDJCtsd4fNqjNH99zLIAXlP18MgIN
    // TFsq-HSb3WzCW-N-XXqSDvFUzAbCsraKCU2sQA_Qq9N3UMOEtUtUa-DXnlf5_SVh
    // -ozt-E8RJcwrtDXNfh2geVxE5ldBo1C0FUO7u_K9aljUgaIS1zlW9x5CbANpBg9O
  }
}

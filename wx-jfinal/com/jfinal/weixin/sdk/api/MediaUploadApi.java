package com.jfinal.weixin.sdk.api;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.jfinal.kit.HttpKit;
import com.jfinal.kit.StrKit;

public class MediaUploadApi {

//  private static String media_upload_url = "http://file.api.weixin.qq.com/cgi-bin/media/upload?access_token=";
  private static String media_upload_url = "https://api.weixin.qq.com/cgi-bin/media/upload?access_token=";
  private static String media_upload_news_url = "https://api.weixin.qq.com/cgi-bin/media/uploadnews?access_token=";

  public static ApiResult mediaUpload(String access_token, String file_type, File file)
      throws IOException {
    return mediaUpload(access_token, file_type, file.getName(), new FileInputStream(file));
  }

  /**
   * 上传图文消息素材
   * 
   * @param access_token
   *          token
   * @param jsonStr
   *          图文消息，一个图文消息支持1到10条图文
   * @return 返回数据示例（正确时的JSON返回结果）： { "type":"news", "media_id":
   *         "CsEf3ldqkAYJAU6EJeIkStVDSvffUJ54vqbThMgplD-VJXXof6ctX5fI6-aYyUiQ",
   *         "created_at":1391857799 }<br>
   *         type
   *         媒体文件类型，分别有图片（image）、语音（voice）、视频（video）和缩略图（thumb），次数为news，即图文消息<br>
   *         media_id 媒体文件/图文消息上传后获取的唯一标识<br>
   *         created_at 媒体文件上传时间
   */
  public static ApiResult newsUpload(String access_token, String jsonStr) {
    if (StrKit.isBlank(access_token)) {
      access_token = AccessTokenApi.getAccessToken().getAccessToken();
    }
    return new ApiResult(HttpKit.post(media_upload_news_url + access_token, jsonStr));
  }

  /**
   * 上传图文消息素材
   * 
   * @param access_token
   *          token
   * @param articles
   *          图文消息，一个图文消息支持1到10条图文
   * @return 返回数据示例（正确时的JSON返回结果）： { "type":"news", "media_id":
   *         "CsEf3ldqkAYJAU6EJeIkStVDSvffUJ54vqbThMgplD-VJXXof6ctX5fI6-aYyUiQ",
   *         "created_at":1391857799 }<br>
   *         type
   *         媒体文件类型，分别有图片（image）、语音（voice）、视频（video）和缩略图（thumb），次数为news，即图文消息<br>
   *         media_id 媒体文件/图文消息上传后获取的唯一标识<br>
   *         created_at 媒体文件上传时间
   */
  public static ApiResult newsUpload(String access_token, Article... articles) {
    JSONArray articles_arr = JSONArray.fromObject(articles);
    JSONObject json = new JSONObject();
    json.put("articles", articles_arr);
    return newsUpload(access_token, json.toString());
  }

  public static ApiResult mediaUpload(String access_token, String file_type, String fileName,
      InputStream inputFile) throws IOException {
    String result = null;
    if (StrKit.isBlank(access_token)) {
      access_token = AccessTokenApi.getAccessToken().getAccessToken();
    }
    URL url = new URL(media_upload_url + access_token + "&type=" + file_type);
    HttpURLConnection con = (HttpURLConnection) url.openConnection();
    con.setRequestMethod("POST"); // 以Post方式提交表单，默认get方式
    con.setDoInput(true);
    con.setDoOutput(true);
    con.setUseCaches(false); // post方式不能使用缓存
    // 设置请求头信息
    con.setRequestProperty("Connection", "Keep-Alive");
    con.setRequestProperty("Charset", "UTF-8");
    // 设置边界
    String BOUNDARY = "----------" + System.currentTimeMillis();
    con.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + BOUNDARY);
    // 请求正文信息
    // 第一部分：
    StringBuilder sb = new StringBuilder();
    sb.append("--"); // 必须多两道线
    sb.append(BOUNDARY);
    sb.append("\r\n");
    sb.append("Content-Disposition: form-data;name=\"file\";filename=\"" + fileName + "\"\r\n");
    sb.append("Content-Type:application/octet-stream\r\n\r\n");
    byte[] head = sb.toString().getBytes("utf-8");
    // 获得输出流
    OutputStream out = new DataOutputStream(con.getOutputStream());
    // 输出表头
    out.write(head);
    // 文件正文部分
    // 把文件已流文件的方式 推入到url中
    DataInputStream in = new DataInputStream(inputFile);
    int bytes = 0;
    byte[] bufferOut = new byte[1024];
    while ((bytes = in.read(bufferOut)) != -1) {
      out.write(bufferOut, 0, bytes);
    }
    in.close();
    // 结尾部分
    byte[] foot = ("\r\n--" + BOUNDARY + "--\r\n").getBytes("utf-8");// 定义最后数据分隔线
    out.write(foot);
    out.flush();
    out.close();
    StringBuffer buffer = new StringBuffer();
    BufferedReader reader = null;
    try {
      // 定义BufferedReader输入流来读取URL的响应
      reader = new BufferedReader(new InputStreamReader(con.getInputStream()));
      String line = null;
      while ((line = reader.readLine()) != null) {
        // System.out.println(line);
        buffer.append(line);
      }
      if (result == null) {
        result = buffer.toString();
      }
    } catch (IOException e) {
      System.out.println("发送POST请求出现异常！" + e);
      e.printStackTrace();
      throw new IOException("数据读取异常");
    } finally {
      if (reader != null) {
        reader.close();
      }
      inputFile.close();
    }
    return new ApiResult(result);
  }

  /**
   * @param access_token
   * @param file_type
   *          媒体文件类型，分别有图片（image）、语音（voice）、视频（video）和缩略图（thumb）
   * @param fileName
   *          文件名
   * @param media_url
   * @return
   * @throws IOException
   */
  public static ApiResult mediaUpload(String access_token, String file_type, String fileName,
      URL media_url) throws IOException {
    // 打开链接
    HttpURLConnection conn = (HttpURLConnection) media_url.openConnection();
    // 设置请求方式为"GET"
    conn.setRequestMethod("GET");
    // 超时响应时间为5秒
    conn.setConnectTimeout(5 * 1000);
    // 通过输入流获取图片数据
    InputStream inStream = conn.getInputStream();
    return mediaUpload(access_token, file_type, fileName, inStream);
  }

  public static void main(String[] args) throws IOException {
    ApiConfig.setAppId("wx429394bf7b590082");
    ApiConfig.setAppSecret("c2fb2ea03db9f0c33e4701ba0b7af8ce");
    // http://wechatmedia.qiniudn.com/20150306151334718.jpg
    URL url = new URL("http://7j1yxx.com1.z0.glb.clouddn.com/20150615161053411.jpg?");
    ApiResult result = mediaUpload(null, "image", "20150615161053411.jpg", url);
    System.out.println(result.getJsonStr());
    // {"type":"image","media_id":"Ni88Tv3E4o-N4BLiZjW8-BoIGMr40jXay6XkZAfvHOUAHm7p-6wTjM4rJQCrkHmt","created_at":1426666466}
    // lynDZrCEFaU0A2w-zjhn3uDdhkxyxMeNaCZSHm4yURt3GQMldNE7lnQdKWQF3cg6
  }
}

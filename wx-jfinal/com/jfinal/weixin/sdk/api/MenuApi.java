/**
 * Copyright (c) 2011-2014, James Zhan 詹波 (jfinal@126.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 */

package com.jfinal.weixin.sdk.api;

import net.sf.json.JSONObject;

import com.jfinal.kit.HttpKit;
import com.jfinal.kit.StrKit;
import com.jfinal.weixin.sdk.api.menu.MenuMsg;

/**
 * menu api
 */
public class MenuApi {

  private static String getMenu = "https://api.weixin.qq.com/cgi-bin/menu/get?access_token=";
  private static String createMenu = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=";

  /**
   * 查询菜单
   * 
   * @param access_token
   *          TODO
   */
  public static ApiResult getMenu(String access_token) {
    String jsonResult;
    if (StrKit.isBlank(access_token)) {
      jsonResult = HttpKit.get(getMenu + AccessTokenApi.getAccessToken().getAccessToken());
    } else {
      jsonResult = HttpKit.get(getMenu + access_token);
    }
    return new ApiResult(jsonResult);
  }

  /**
   * 创建菜单
   * 
   * @param access_token
   *          TODO
   */
  public static ApiResult createMenu(String access_token, String jsonStr) {
    if (StrKit.isBlank(access_token)) {
      access_token = AccessTokenApi.getAccessToken().getAccessToken();
    }
    String jsonResult = HttpKit.post(createMenu + access_token, jsonStr);
    return new ApiResult(jsonResult);
  }

  public static void main(String[] args) {
    ApiConfig.setAppId("wx429394bf7b590082");
    ApiConfig.setAppSecret("c2fb2ea03db9f0c33e4701ba0b7af8ce");
    System.out.println(MenuApi.getMenu(null).getJsonStr());
    MenuMsg msg = new MenuMsg();
    msg.putButton(1, JSONObject.fromObject("{\"name\":\"身份认证\", \"sub_button\":["
        + "{\"type\":\"click\", \"name\":\"用户绑定\", \"key\":\"V0001_SFRZ_YHBD\"},"
        + "{\"type\":\"click\", \"name\":\"到期查询\", \"key\":\"V0002_SFRZ_DQCX\"},"
        + "{\"type\":\"click\", \"name\":\"缴费确认\", \"key\":\"V0003_SFRZ_JFQR\"}" + "]}"));
    msg.putButton(2, JSONObject.fromObject("{\"name\":\"产品服务\", \"sub_button\":["
        + "{\"type\":\"click\", \"name\":\"产品介绍\", \"key\":\"V1001_CPFW_CPJX\"},"
        + "{\"type\":\"click\", \"name\":\"在线客服\", \"key\":\"V1002_CUSTOMER_SERVICE\"}" + "]}"));
    msg.putButton(3, JSONObject.fromObject("{\"name\":\"操作指南\", \"sub_button\":["
        + "{\"type\":\"click\", \"name\":\"药店指南\", \"key\":\"V2001_CZSC_GYJS\"},"
        + "{\"type\":\"click\", \"name\":\"医院指南\", \"key\":\"V2002_CZSC_ZYJS\"},"
        // +
        // "{\"type\":\"click\", \"name\":\"医保对账\", \"key\":\"V2003_CZSC_YBDZ\"},"
        + "{\"type\":\"click\", \"name\":\"库房指南\", \"key\":\"V2004_CZSC_KFCZ\"}"
        // +
        // "{\"type\":\"click\", \"name\":\"报表打印\", \"key\":\"V2005_CZSC_BBDY\"}"
        + "]}"));
    System.out.println(msg.getJson());
    // System.out.println(new MenuApi().createMenu().getJsonStr());
  }
}

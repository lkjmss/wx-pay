package com.jfinal.weixin.sdk.api;

import com.jfinal.kit.HttpKit;
import com.jfinal.weixin.sdk.kit.ParaMap;

/**
 * 多客服系统API
 * 
 * @author yanglh
 * 
 */
public class CustomServiceAPI {

  private static final String getKflist = "https://api.weixin.qq.com/cgi-bin/customservice/getkflist";
  private static final String getOnlinekflist = "https://api.weixin.qq.com/cgi-bin/customservice/getonlinekflist";

  // private static final String getRecord =
  // "https://api.weixin.qq.com/cgi-bin/customservice/getrecord";

  public static ApiResult getOnlinekflist() {
    ParaMap pm = ParaMap.create("access_token", AccessTokenApi.getAccessToken().getAccessToken());
    return new ApiResult(HttpKit.get(getOnlinekflist, pm.getData()));
  }

  public static ApiResult getFlist() {
    ParaMap pm = ParaMap.create("access_token", AccessTokenApi.getAccessToken().getAccessToken());
    return new ApiResult(HttpKit.get(getKflist, pm.getData()));
  }
}

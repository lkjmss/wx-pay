package com.jfinal.weixin.sdk.api;

import net.sf.json.JSONObject;

import com.jfinal.kit.HttpKit;
import com.jfinal.kit.StrKit;

/**
 * 1 创建分组<br/>2 查询所有分组<br/>3 查询用户所在分组 <br/>4 修改分组名 <br/>5 移动用户分组 <br/>6
 * 批量移动用户分组
 * 
 * @author Administrator
 * 
 */
/**
 * @author Administrator
 * 
 */
public class GroupApi {

  private static final String create_url = "https://api.weixin.qq.com/cgi-bin/groups/create?access_token=";
  private static final String get_url = "https://api.weixin.qq.com/cgi-bin/groups/get?access_token=";
  private static final String getid_url = "https://api.weixin.qq.com/cgi-bin/groups/getid?access_token=";
  private static final String rename_url = "https://api.weixin.qq.com/cgi-bin/groups/update?access_token=";
  private static final String move_url = "https://api.weixin.qq.com/cgi-bin/groups/members/update?access_token=";
  private static final String batch_move_url = "https://api.weixin.qq.com/cgi-bin/groups/members/batchupdate?access_token=";
  private static final String delete_url = "https://api.weixin.qq.com/cgi-bin/groups/delete?access_token=";
  /**
   * 删除分组
   * @param access_token
   * @param jsonStr
   * @return
   */
  public static ApiResult delete(String access_token,String jsonStr){
	  if (StrKit.isBlank(access_token)) {
	      access_token = AccessTokenApi.getAccessToken().getAccessToken();
	    }
	  return new ApiResult(HttpKit.post(delete_url + access_token, jsonStr));
  }
  /**
   * 创建分组
   * 
   * @param access_token
   * @param jsonStr
   *          {"group":{"name":"test"}}
   * @return
   * @date 2015-3-16
   * @author zhongjf
   */
  public static ApiResult create(String access_token, String jsonStr) {
    if (StrKit.isBlank(access_token)) {
      access_token = AccessTokenApi.getAccessToken().getAccessToken();
    }
    return new ApiResult(HttpKit.post(create_url + access_token, jsonStr));
  }

  /**
   * 得到分组
   * 
   * @param access_token
   * @return
   * @date 2015-3-16
   * @author zhongjf
   */
  public static ApiResult get(String access_token) {
    if (StrKit.isBlank(access_token)) {
      access_token = AccessTokenApi.getAccessToken().getAccessToken();
    }
    return new ApiResult(HttpKit.get(get_url + access_token));
  }

  /**
   * 查询用户所在分组
   * 
   * @param access_token
   * @param jsonStr
   *          {"openid":"od8XIjsmk6QdVTETa9jLtGWA6KBc"}
   * @return
   * @date 2015-3-16
   * @author zhongjf
   */
  public static ApiResult getid(String access_token, String jsonStr) {
    if (StrKit.isBlank(access_token)) {
      access_token = AccessTokenApi.getAccessToken().getAccessToken();
    }
    System.out.println(access_token);
    return new ApiResult(HttpKit.post(getid_url + access_token, jsonStr));
  }

  /**
   * 修改分组名称
   * 
   * @param access_token
   * @param jsonStr
   *          {"group":{"id":108,"name":"test2_modify2"}}
   * @return
   * @date 2015-3-16
   * @author zhongjf
   */
  public static ApiResult rename(String access_token, String jsonStr) {
    if (StrKit.isBlank(access_token)) {
      access_token = AccessTokenApi.getAccessToken().getAccessToken();
    }
    return new ApiResult(HttpKit.post(rename_url + access_token, jsonStr));
  }

  /**
   * 移动用户分组
   * 
   * @param access_token
   * @param jsonStr
   *          {"openid":"oDF3iYx0ro3_7jD4HFRDfrjdCM58","to_groupid":108}
   * @return
   * @date 2015-3-16
   * @author zhongjf
   */
  public static ApiResult move(String access_token, String jsonStr) {
    if (StrKit.isBlank(access_token)) {
      access_token = AccessTokenApi.getAccessToken().getAccessToken();
    }
    return new ApiResult(HttpKit.post(move_url + access_token, jsonStr));
  }

  /**
   * 批量移动用户分组
   * 
   * @param access_token
   * @param jsonStr
   *          {"openid_list":["oDF3iYx0ro3_7jD4HFRDfrjdCM58",
   *          "oDF3iY9FGSSRHom3B-0w5j4jlEyY"],"to_groupid":108}
   * @return
   * @date 2015-3-16
   * @author zhongjf
   */
  public static ApiResult batchupdate(String access_token, String jsonStr) {
    if (StrKit.isBlank(access_token)) {
      access_token = AccessTokenApi.getAccessToken().getAccessToken();
    }
    String _json = HttpKit.post(batch_move_url + access_token, jsonStr);
    return new ApiResult(_json);
  }

  public static void main(String[] args) {
    ApiConfig.setAppId("wx429394bf7b590082");
    ApiConfig.setAppSecret("c2fb2ea03db9f0c33e4701ba0b7af8ce");
    System.out.println(GroupApi.getid(null, "{\"openid\":\"o9CXhjphPRClqMNCtNpU9jYoEBz4\"}")
        .getJsonStr());
    //
    // JSONObject createJson = new JSONObject();
    // createJson.put("name", "vip组");
    //
    // JSONObject Json = new JSONObject();
    // Json.put("group", createJson);
    // System.out.println(GroupApi.create(null, Json.toString()).getJsonStr());
    // System.out.println(GroupApi.get(null).getJsonStr());

  }
}

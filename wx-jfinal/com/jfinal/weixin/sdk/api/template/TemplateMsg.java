package com.jfinal.weixin.sdk.api.template;

import java.util.Map;

import net.sf.json.JSONObject;

public abstract class TemplateMsg {

  protected String touser;
  protected String template_id;
  protected String url;
  protected String topcolor;
  protected JSONObject data;

  public String getTouser() {
    return touser;
  }

  public void setTouser(String touser) {
    this.touser = touser;
  }

  public String getTemplate_id() {
    return template_id;
  }

  public void setTemplate_id(String template_id) {
    this.template_id = template_id;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getTopcolor() {
    return topcolor;
  }

  public void setTopcolor(String topcolor) {
    this.topcolor = topcolor;
  }

  public JSONObject getData() {
    return data;
  }

  public TemplateMsg setData(JSONObject data) {
    this.data = data;
    return this;
  }

  @SuppressWarnings("rawtypes")
  public TemplateMsg appendData(JSONObject jsonObject) {
    if (data == null) {
      data = new JSONObject();
    }
    data.putAll((Map) jsonObject);
    return this;
  }

  public TemplateMsg putData(Object key, Object value) {
    if (data == null) {
      data = new JSONObject();
    }
    data.put(key, value);
    return this;
  }

  public String build() {
    JSONObject jsonObject = new JSONObject();
    jsonObject.put("touser", touser);
    jsonObject.put("template_id", template_id);
    jsonObject.put("url", url);
    jsonObject.put("topcolor", topcolor);
    jsonObject.put("data", data);
    return jsonObject.toString();
  }
}

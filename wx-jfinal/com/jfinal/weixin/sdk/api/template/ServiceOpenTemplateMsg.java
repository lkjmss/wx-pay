package com.jfinal.weixin.sdk.api.template;

import net.sf.json.JSONObject;

import com.jfinal.kit.HttpKit;
import com.jfinal.weixin.sdk.api.AccessToken;
import com.jfinal.weixin.sdk.api.AccessTokenApi;
import com.jfinal.weixin.sdk.api.ApiConfig;

public class ServiceOpenTemplateMsg extends TemplateMsg {

  public ServiceOpenTemplateMsg() {
    this.template_id = "QtyI_3fBkF7txjnX1ai07cscrzXfQcxw5ZYatWhJXC4";
    this.topcolor = "#009966";
  }

  public static void main(String[] args) {
    ApiConfig.setAppId("wx429394bf7b590082");
    ApiConfig.setAppSecret("c2fb2ea03db9f0c33e4701ba0b7af8ce");
    // String getFollowers = "https://api.weixin.qq.com/cgi-bin/user/get";
    String sendTemplateMsg = "https://api.weixin.qq.com/cgi-bin/message/template/send?access_token=";
    String token = null;
    AccessToken at = AccessTokenApi.getAccessToken();
    if (at.isAvailable()) {
      token = at.getAccessToken();
      /*
       * ParaMap pm = ParaMap.create("access_token", token); JSONArray array =
       * JSONObject.fromObject(HttpKit.get(getFollowers, pm.getData()))
       * .getJSONObject("data").getJSONArray("openid"); for (Object object :
       * array) {
       */
      String openid = "o9CXhjmGj8t3Yx01qNRhkVZ4bvBs";
      ServiceOpenTemplateMsg msg = new ServiceOpenTemplateMsg();
      msg.setTouser(openid);
      JSONObject data = new JSONObject();
      data.put("first",
          JSONObject.fromObject("{value:\"您好，感谢关注浙江网能EzHIS微信服务平台\",color:\"#0A0A0A\"}"));
      data.put("keyword1", JSONObject.fromObject("{value:\"浙江网能EzHIS微信服务平台\",color:\"#0A0A0A\"}"));
      data.put("keyword2", JSONObject.fromObject("{value:\"2014-12-09\",color:\"#0A0A0A\"}"));
      data.put("remark", JSONObject.fromObject("{value:\"这是一条测试模板消息\",color:\"#0A0A0A\"}"));
      msg.setData(data);
      HttpKit.post(sendTemplateMsg + token, msg.build());
      // }
    } else
      System.out.println(at.getErrorCode() + " : " + at.getErrorMsg());
  }
}
